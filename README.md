# MN-AL-Repository_Lithaud_Ferrari_Guyot

A repository to store and our TD and TP of Mn an Al subject


## Authors
All the authors of this reposotories are : **Alexandre Lithaud**, **Julien Ferrari** and **Romain Guyot**

# Méthode Numérique (MN)

- **TP 1 - Polynome** ([Sujet](/Methode_Numerique/TP1-Polynome/TP1MN.pdf)) &#8594; [Voir ici](/Methode_Numerique/TP1-Polynome/) &#8594; [Compte Rendu](/Methode_Numerique/TP1-Polynome/poly/compteRenduTP1MN-Lithaud-Ferrari-Guyot.pdf)

- **TP 2 - BLAS** ([Sujet](/Methode_Numerique/TP2-BLAS/TP2MN.pdf)) &#8594; [Voir ici](/Methode_Numerique/TP2-BLAS/) &#8594; [Compte Rendu](/Methode_Numerique/TP2-BLAS/TPBLAS/doc/compte_rendu-TP2-BLAS-FERRARI_GUYOT_LITHAUD.pdf)


# Algorithmique Avancée (AL)

- **TP 1 - ABR AVL** ([Sujet](/Algo_Avancee/TP1-ABR_AVL/TP1ALG.pdf))&#8594; [Voir ici](/Algo_Avancee/TP1-ABR_AVL/ABR/) &#8594; [Compte Rendu](/Algo_Avancee/TP1-ABR_AVL/ABR/compteRenduTP1AL-Lithaud-Ferrari-Guyot.pdf)

- **TP 2 - A234** ([Sujet](/Algo_Avancee/TP2-ABR-234/TP2ALG.pdf))&#8594; [Voir ici](/Algo_Avancee/TP2-ABR-234/A234/) &#8594; [Compte Rendu](/Algo_Avancee/TP2-ABR-234/compte_rendu_TP2_A234-Lithaud-Guyot-Ferrari.pdf)

- **TP 3 - Graphe** ([Sujet](/Algo_Avancee/TP3-Graphe/TP3Algo.pdf))&#8594; [Voir ici](/Algo_Avancee/TP3-Graphe/) &#8594; [Compte Rendu](/Algo_Avancee/TP3-Graphe/compte_rendu_TP3_Graphe-Lithaud-Guyot-Ferrari.pdf)


## Content

this repository is for academic purpose only.