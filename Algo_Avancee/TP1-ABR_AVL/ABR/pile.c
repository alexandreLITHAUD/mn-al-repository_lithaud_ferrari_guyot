#include <stdlib.h>
#include "pile.h"
#include "abr.h"

ppile_t creer_pile()
{
  ppile_t pile = malloc(sizeof(pile_t));
  pile->sommet = 0;
  return pile;
}

int detruire_pile(ppile_t p)
{

  while (!pile_vide(p))
  {
    pnoeud_t t = depiler(p);
    free(t);
  }

  free(p);
  return 0;
}

int pile_vide(ppile_t p)
{
  return p->sommet == 0;
}

int pile_pleine(ppile_t p)
{
  return p->sommet == MAX_PILE_SIZE;
}

pnoeud_t depiler(ppile_t p)
{

  if (!pile_vide(p))
  {
    pnoeud_t res = res = p->Tab[p->sommet - 1];
    p->Tab[p->sommet] = NULL;
    p->sommet--;

    return res;
  }

  return NULL;
}

int empiler(ppile_t p, pnoeud_t pn)
{

  if (!pile_pleine(p))
  {

    p->Tab[p->sommet] = pn;
    p->sommet++;

    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;
}
