#ifndef AVL_H
#define AVL_H

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) > (b) ? (b) : (a))

typedef struct n2{
    int cle;
    int bal;
    struct n2 *fdroite, *fgauche;
} noeud;

typedef noeud *Arbre;

int feuille(Arbre a);

Arbre lire_arbre_avl (char *nom_fichier);

void afficher_arbre_avl (Arbre a, int niveau);

Arbre rajouter_cle(Arbre a, int cle);

Arbre enlever_cle(Arbre a, int cle);

Arbre equilibrer(Arbre a);

#endif