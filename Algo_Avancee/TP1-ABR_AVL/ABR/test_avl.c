#include <stdio.h>
#include <stdlib.h>

#include "avl.h"

int main(int argc, char **argv)
{

    Arbre a;
    Arbre a2;

    Arbre a3;
    Arbre a4;

    if (argc != 3)
    {
        fprintf(stderr, "il manque le parametre nom de fichier\n");
        exit(-1);
    }

    a = lire_arbre_avl(argv[1]);
    a2 = lire_arbre_avl(argv[2]);

    afficher_arbre_avl(a, 0);
    afficher_arbre_avl(a2, 0);

    /*
       appeler les fonctions que vous
       avez implementees
    */

    /* TEST AJOUT */
    printf("Rajout de la cle 10 à a\n");
    a3 = rajouter_cle(a, 20);
    afficher_arbre_avl(a3, 0);

    a3 = rajouter_cle(a, 30);
    afficher_arbre_avl(a3, 0);

//     /* TEST DESTRUCTION */
    printf("Destruction du noeud 5 à a\n");
    a4 = enlever_cle(a,5);
    afficher_arbre_avl(a4, 0);

    return EXIT_SUCCESS;
}