#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

/**
 * @brief Fonction d'aide de l'utilisation du programme
 * 
 */
void helper(){
    printf("Programme de test pile : \n");
    printf("\tc : creer_pile\n");
    printf("\tt : detruire_pile\n");
    printf("\tv : pile_vide\n");
    printf("\tp : pile_pleine\n");
    printf("\td : depiler\n");
    printf("\te : empiler\n");
    printf("\ts : visualiser la pile\n");
    printf("\th : afficher l\'aide\n");
    printf("\tq : quit\n");
}

void voirPile(ppile_t p){

    if(pile_vide(p)){
        printf(YELLOW"PILE VIDE\n"NOCOLOR);
        return;
    }

    for(int i=0;i<MAX_PILE_SIZE;i++){

        printf("|");

        if(i < p->sommet){
            printf(BLUE" %i "NOCOLOR,p->Tab[i]->cle);
        }
        else{
            printf(RED" NILL "NOCOLOR);
        }

    }

    printf("\n");

}

int main(){

    helper();

    char c;
    int exit = 1;

    ppile_t p = NULL;

    while(exit){

        printf(BLUE"Que voulez faire ?\n"NOCOLOR);
        scanf("%c",&c);


        switch (c)
        {
        case 'h':
            helper();
            break;

        case 'c':
            if(p != NULL){
                printf(YELLOW"La Pile est deja créé\n"NOCOLOR);
            }
            else{
                p = creer_pile();
                printf(GREEN"La Pile a été créé avec succès\n"NOCOLOR);
            }
            break;

        case 't':
            if(p == NULL){
                printf(YELLOW"Il n'y a pas de Pile à détruire\n"NOCOLOR);
            }
            else{
                if(detruire_pile(p)){
                    printf(RED"Echec de la détruction de la pile\n"NOCOLOR);
                    return EXIT_FAILURE;
                }
                else{
                    p = NULL;
                    printf(GREEN"Destruction réussite\n"NOCOLOR);
                }
            }
            break;

        case 'v':
            if(p == NULL){
                printf(YELLOW"Il n'y a pas de Pile à regarder\n"NOCOLOR);
            }
            else{
                if(pile_vide(p)){
                    printf(GREEN"La Pile est vide\n"NOCOLOR);
                }else{
                    printf(GREEN"La Pile n'est pas vide\n"NOCOLOR);
                }
            }
            break;
        
        case 'p':
            if(p == NULL){
                printf(YELLOW"Il n'y a pas de Pile à regarder\n"NOCOLOR);
            }
            else{
                if(pile_pleine(p)){
                    printf(GREEN"La Pile est pleine\n"NOCOLOR);
                }else{
                    printf(GREEN"La Pile n'est pas pleine\n"NOCOLOR);
                }
            }
            break;

        case 'e':
            if(p == NULL){
                printf(YELLOW"Il n'y a pas de Pile à empiler\n"NOCOLOR);
                break;
            }
            if(pile_pleine(p)){
                printf(YELLOW"On ne peux pas épiler une Pile pleine\n"NOCOLOR);
                break;
            }
            else{
                printf(BLUE"Quelle est la valeur à ajouter ?\n"NOCOLOR);
                int val=0;
                scanf("%i",&val);
                pnoeud_t pn = malloc(sizeof(noeud_t));
                pn->cle = val;
                if(!empiler(p,pn)){
                    printf(GREEN"Empilation réussite\n"NOCOLOR);
                }
                else{
                    printf(RED"Echec d\'empilation\n"NOCOLOR);
                    detruire_pile(p);
                    return EXIT_FAILURE;
                }
            }
            break;

        case 'd':
            if(p == NULL){
                printf(YELLOW"Il n'y a pas de Pile à depiler\n"NOCOLOR);
                break;
            }
            if(pile_vide(p)){
                printf(YELLOW"On ne peux pas dépiler une Pile vide\n"NOCOLOR);
                break;
            }
            pnoeud_t test = depiler(p);
            if(test !=NULL ){
                printf(GREEN"Depilation réussite\n"NOCOLOR);
                free(test);
            }
            else{
                printf(RED"Echec de dépilation\n"NOCOLOR);
                detruire_pile(p);
                return EXIT_FAILURE;
            }
            break;

        case 's':
            voirPile(p);
            break;

        case 'q':
            exit = 0;
            if(p != NULL){
                detruire_pile(p);
            }
            break;
        }
        fflush(0);
    }

    return EXIT_SUCCESS;

}