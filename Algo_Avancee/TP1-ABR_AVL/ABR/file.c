#include <stdlib.h>

#include "abr.h"
#include "file.h"

int nbVal = 0;

pfile_t creer_file()
{
  pfile_t f = malloc(sizeof(file_t));
  f->tete = 0;
  f->queue = 0;
  return f;
}

int detruire_file(pfile_t f)
{
  while (!file_vide(f))
  {
    pnoeud_t test = defiler(f);
    free(test);
  }
  free(f);
  return EXIT_SUCCESS;
}

int file_vide(pfile_t f)
{
  return (f->queue == f->tete && nbVal == 0);
}

int file_pleine(pfile_t f)
{
  return (f->queue == f->tete && nbVal == MAX_FILE_SIZE);
}

pnoeud_t defiler(pfile_t f)
{
  if(!file_vide(f)){

    pnoeud_t res = f->Tab[f->tete];
    f->Tab[f->tete] = NULL;
    f->tete = (f->tete+1) % MAX_FILE_SIZE;
    nbVal--;
    return res;
  }
  return NULL;
}

int enfiler(pfile_t f, pnoeud_t p)
{

  if(!file_pleine(f)){
    f->Tab[(f->queue) % MAX_FILE_SIZE] = p;
    f->queue = (f->queue+1) % MAX_FILE_SIZE;
    nbVal++;
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}
