#include "pile.h"
#include <stdio.h>
#include <stdlib.h>


int main(){

    ppile_t p = creer_pile();

    pnoeud_t pn = malloc(sizeof(noeud_t));
    pn->cle = 5;
    
    pnoeud_t pn2 = malloc(sizeof(noeud_t));
    pn2->cle = 7;

    pn->fdroite = pn2;

    if(!empiler(p, pn)){
        return EXIT_FAILURE;
    }

    if(!empiler(p, pn2)){
        return EXIT_FAILURE;
    }

    while(!pile_vide(p)){
        pnoeud_t res = depiler(p);
        printf("cle = %i\n",res->cle);
    }

    return EXIT_SUCCESS;
}