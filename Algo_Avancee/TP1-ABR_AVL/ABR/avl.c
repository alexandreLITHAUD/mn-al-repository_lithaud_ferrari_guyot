#include <stdio.h>
#include <stdlib.h>

#include "avl.h"

Arbre ROTD(Arbre a);
Arbre ROTG(Arbre a);

Arbre recherche_cle_arbre(Arbre a, int valeur);
Arbre ajouter_noeud(Arbre a, Arbre n);
Arbre enlever_noeud(Arbre a, int n);
int actualisationBalance(Arbre a);
Arbre otermin(Arbre a);
int hauteur_arbre_r(Arbre a);

/* ============================================== */
/**
 * @brief Fonction qui dit si un arbre est une feuille ou non
 * 
 * @param a l'arbre a analyser
 * @return int 1 -> si feuille
 *             0 -> sinon
 */
int feuille(Arbre a)
{
  if (a == NULL)
    return 0;
  else
  {
    if ((a->fgauche == NULL) && (a->fdroite == NULL))
      return 1;
    else
      return 0;
  }
}

/* ============================================== */
/**
 * @brief Fonction qui recherche la cle d'un arbre
 * 
 * @param a l'arbre dans lequel on cherche la valeur
 * @param valeur la valeur que l'on cherche
 * @return le noeud déténant la clé 
 *         NULL -> sinon
 */
Arbre recherche_cle_arbre(Arbre a, int valeur)
{
  if (a == NULL)
    return NULL;
  else
  {
    if (a->cle == valeur)
      return a;
    else
    {
      if (a->cle < valeur)
        return recherche_cle_arbre(a->fdroite, valeur);
      else
        return recherche_cle_arbre(a->fgauche, valeur);
    }
  }
}

/* ============================================== */
/**
 * @brief Fonction qui lit un arbre contenu dans un fichier
 * 
 * @param nom_fichier un pointeur sur le fichier
 * @return un arbre
 * 
 */
Arbre lire_arbre_avl(char *nom_fichier)
{
  FILE *f;
  int cle;
  Arbre a = NULL;

  f = fopen(nom_fichier, "r");

  while (fscanf(f, "%d", &cle) != EOF)
  {
    a = rajouter_cle(a, cle);
  }

  fclose(f);

  return a;
}

/* ============================================== */
/**
 * @brief Fonction qui affiche un arbre ainsi que les niveaux de ses noeuds
 * 
 * @param a l'arbre a afficher
 * @param niveau le niveau de chaque noeud
 *
 */
void afficher_arbre_avl(Arbre a, int niveau)
{
  int i;

  if (a != NULL)
  {
    afficher_arbre_avl(a->fdroite, niveau + 1);

    for (i = 0; i < niveau; i++)
      printf("\t");
    printf(" %d (%d)\n\n", a->cle, niveau);

    afficher_arbre_avl(a->fgauche, niveau + 1);
  }
  return;
}

/* ============================================== */
/**
 * @brief Fonction qui ajoute un noeud dans un arbre
 * 
 * @param a l'arbre dans lequel on ajoute
 * @param n le noeud a ajouter
 * @return l'arbre avec le noeud ajouté
 * 
 */
Arbre ajouter_noeud(Arbre a, Arbre n)
{
  if (a == NULL)
    return n;
  else if (n->cle < a->cle){
    a->fgauche = ajouter_noeud(a->fgauche, n);
    a->bal--;
  }
  else{
    a->fdroite = ajouter_noeud(a->fdroite, n);
    a->bal++;
  }
  return a;
}

/* ============================================== */
/**
 * @brief Fonction qui rajoute une clé dans un arbre
 * 
 * @param a l'arbre 
 * @param cle la clé a ajouter
 * @return l'arbre avec la clé ajoutée
 */
Arbre rajouter_cle(Arbre a, int cle)
{
  Arbre n;
  Arbre ptrouve;

  ptrouve = recherche_cle_arbre(a, cle);

  if (ptrouve == NULL)
  {
    n = (Arbre)malloc(sizeof(noeud));
    n->cle = cle;
    n->fgauche = NULL;
    n->fdroite = NULL;
    n->bal = 0;

    a = ajouter_noeud(a, n);

    // On met a jour les balancier
    // a = actualisationBalance(a);

    // On rééquilibre l'arbre
    a = equilibrer(a);

    return a;
  }
  else
    return a;
}

/* ============================================== */
/**
 * @brief Fonction qui enleve un noeud dans un arbre
 * 
 * @param a l'arbre 
 * @param n la clé du noeud à enlever
 * @return le nouvel arbre 
 *          
 */
Arbre enlever_noeud(Arbre a, int n)
{
  if (a == NULL){
    return a;
  }
  else if (n < a->cle){   //recherche du noeud dans l'arbre
    a->fgauche = enlever_noeud(a->fgauche,n);
  }
  else if (n > a->cle){
    a->fdroite = enlever_noeud(a->fdroite,n);
  }
  else if (a->fgauche == NULL) {    //libération de l'espace mémoire et suppression du noeud
    free(a);
    return a->fdroite;
  }else if (a->fdroite == NULL) {
    free(a);
    return a->fgauche;
  }
  else {
    a->cle = a->fgauche->cle;
    a->fgauche = otermin(a->fgauche);
  }
  if (hauteur_arbre_r(a) == 0){   //mise à jour des balances
    a->bal = 0;
    return a;
  }   
  else {  
    a->bal = a->bal + hauteur_arbre_r(a);
    if (a->bal == 0){
      a->bal = -1;
      return a;
    }
    else{
      a->bal = 0;
      return a;
    }
  }
}

/* ============================================== */
/**
 * @brief Fonction qui enleve une clé dans un arbre
 * 
 * @param a l'arbre 
 * @param clé la clé à enlever
 * @return le nouvel arbre 
 *          
 */
Arbre enlever_cle(Arbre a, int cle)
{
  if(a == NULL){
    return a;
  }

  Arbre ptrouve = recherche_cle_arbre(a,cle);

  if(ptrouve == NULL){
    return a;
  }
  else{
    a = enlever_noeud(a,cle);

    a = equilibrer(a);
    return a;
  }
  
}

/* ============================================== */
/**
 * @brief Fonction qui enleve la valeur de cle minimale dans un arbre
 * 
 * @param a l'arbre 
 * @return le nouvel arbre 
 *          
 */
Arbre otermin(Arbre a)
{
  int h = hauteur_arbre_r(a);
  
  if (a->fgauche == NULL) {
    a->fdroite->bal = -1;
    return a->fdroite;
  }
  else {
    a->fgauche->bal = h;
    a->fgauche = otermin(a->fgauche); 
    h = -h;
  }
  if (h == 0){
    a->bal = 0;
    return a;
  }
  else {
    a->bal = a->bal + h;
    a = equilibrer(a);
    if (a->bal == 0){
      a->bal = -1;
      return a;
    }
    else{
      a->bal = 0;
      return a;
    }
  }
}

/* ============================================== */
/**
 * @brief Fonction qui réequilibre un arbre afin de le conserver avl
 * 
 * @param a l'arbre 
 * @return le nouvel arbre 
 *          
 */
Arbre equilibrer(Arbre a)
{
    if (a->bal == 2)
        if (a->fdroite->bal >= 0)
            return ROTG(a);
        else
        {
            a->fdroite = ROTD(a->fdroite);
            return ROTG(a);
        }
    else if (a->bal == -2)
        if (a->fgauche->bal <= 0)
            return ROTD(a);
        else
        {
            a->fgauche = ROTG(a->fgauche);
            return ROTD(a);
        }
    else
        return a;
}

/* ============================================== */
/**
 * @brief Fonction qui effectue une rotation droite sur l'arbre
 * @param a l'arbre 
 * 
 * @return la nouvelle racine de l'arbre
 *          
 */
// GESTION DES BALANCES
Arbre ROTD(Arbre a)
{
  Arbre a2 = NULL;
  int balA, balA2;

  a2 = a->fgauche;

  balA = a->bal;
  balA2 = a2->bal;

  a->fgauche = a2->fdroite;
  a2->fdroite = a;

  a->bal = balA-min(balA2,0)+1;
  a2->bal = max(balA+2,balA+balA2+2);
  a2->bal = max(balA2+1,a2->bal);

  return a2;
    
}

/* ============================================== */
/**
 * @brief Fonction qui effectue une rotation gauche sur l'arbre
 * @param a l'arbre 
 * 
 * @return la nouvelle racine de l'arbre
 *          
 */
// GESTION DES BALANCES
Arbre ROTG(Arbre a)
{
  Arbre a2 = NULL;
  int balA, balA2;

  a2 = a->fdroite;

  balA = a->bal;
  balA2 = a2->bal;

  a->fdroite = a2->fgauche;
  a2->fgauche = a;

  a->bal = balA-max(balA2,0)-1;
  a2->bal = min(balA-2,balA+balA2-2);
  a2->bal = min(a2->bal,balA2-1);

  return a2;
}

/* ============================================== */
/**
 * @brief Fonction qui calcule la hauteur d'un arbre
 * @param a l'arbre 
 * 
 * @return la hauteur de l'arbre
 *          
 */
int hauteur_arbre_r(Arbre a)
{
  if (a == NULL)
    return -1;
  else if (feuille(a))
    return 0;
  else
    return 1 + max(hauteur_arbre_r(a->fgauche), hauteur_arbre_r(a->fdroite));
}

/* ============================================== */
/**
 * @brief Fonction qui retourne ula balance d'un noeud
 * @param a l'arbre 
 * 
 * @return la balance du noeud
 *          
 */
int actualisationBalance(Arbre a)
{
  return a->bal;
}
