#include <stdio.h>
#include <stdlib.h>

#include "abr.h"

int main(int argc, char **argv)
{
  Arbre_t a;
  Arbre_t a2;

  if (argc != 3)
  {
    fprintf(stderr, "il manque le parametre nom de fichier\n");
    exit(-1);
  }

  a = lire_arbre(argv[1]);
  a2 = lire_arbre(argv[2]);

  afficher_arbre(a, 0);

  /*
     appeler les fonctions que vous
     avez implementees
  */

  printf("parcour en largeur = ");
  parcourir_arbre_largeur(a);

  printf("hauteur = %i | ", hauteur_arbre_r(a));

  printf("hauteurNR = %i\n", hauteur_arbre_nr(a));

  printf("nbCle = %i | ", nombre_cles_arbre_r(a));

  printf("nbCleNR = %i\n", nombre_cles_arbre_nr(a));

  printf("cleMin = %i\n", trouver_cle_min(a));

  printf("Liste de clés croissante (R) = ");
  imprimer_liste_cle_triee_r(a);
  printf("\n");

  printf("Liste de clés croissante (NR) = ");
  imprimer_liste_cle_triee_nr(a);
  printf("\n");

  printf("Abre plein = %s\n", (arbre_plein(a) == 1) ? "true" : "false");

  printf("Abre parfait = %s\n", (arbre_parfait(a) == 1) ? "true" : "false");

  Arbre_t temp = rechercher_cle_inf_arbre(a,5);
  printf("Clé directement inférieur à 5 = %i\n", (temp == NULL) ? -1 : temp->cle);

  Arbre_t temp2 = rechercher_cle_sup_arbre(a,5);
  printf("Clé directement supérieur à 5 = %i\n", (temp2 == NULL) ? -1 : temp2->cle);

  Arbre_t test = detruire_cle_arbre(a,10);
  printf("Arbre 1 avec la cle 10 détruite\n");
  test == NULL ? fprintf(stderr,"Empty tree\n") : afficher_arbre(test,0);

  printf("arbre 1\n");
  afficher_arbre(a,0);

  printf("arbre 2");
  afficher_arbre(a2,0);

  Arbre_t uni = union_deux_arbres(a,a2);
  printf("Arbre 1 unie avec l'arbre 2\n");
  afficher_arbre(uni,0);

  Arbre_t uni2 = intersection_deux_arbres(a,a2);
  printf("Arbre 1 intersecté avec l'arbre 2\n");
  afficher_arbre(uni2,0);

  printf("TEST PASSED\n");
  
}
