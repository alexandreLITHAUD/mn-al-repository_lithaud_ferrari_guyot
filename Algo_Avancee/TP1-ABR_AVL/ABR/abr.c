#include <stdio.h>
#include <stdlib.h>

#include "abr.h"
#include "pile.h"
#include "file.h"

#define max(a, b) ((a) > (b) ? (a) : (b))

/* ============================================== */
/**
 * @brief Fonction qui dit si un arbre est une feuille ou non
 * 
 * @param a l'arbre a analyser
 * @return int 1 -> si feuille
 *             0 -> sinon
 */
int feuille(Arbre_t a)
{
  if (a == NULL)
    return 0;
  else
  {
    if ((a->fgauche == NULL) && (a->fdroite == NULL))
      return 1;
    else
      return 0;
  }
}

/* ============================================== */
/**
 * @brief Fonction interne a ajouter_cle
 * @see ajouter_cle
 * @param a l'arbre dans lequel ajouter
 * @param n l'arbre a ajouter
 * @return Arbre_t l'arbre mis a jour
 */
Arbre_t ajouter_noeud(Arbre_t a, Arbre_t n)
{
  /* ajouter le noeud n dans l'arbre a */

  if (a == NULL)
    return n;
  else if (n->cle < a->cle)
    a->fgauche = ajouter_noeud(a->fgauche, n);
  else
    a->fdroite = ajouter_noeud(a->fdroite, n);
  return a;
}

/* ============================================== */
/**
 * @brief Fonction qui recherhce si une cle est dans un arbre
 * 
 * @param a l'arbre a analyser
 * @param valeur la valeur a chercher dans l'arbre
 * @return Arbre_t NULL -> si il n'y est pas
 *              l'arbre -> sinon
 */
Arbre_t rechercher_cle_arbre(Arbre_t a, int valeur)
{
  if (a == NULL)
    return NULL;
  else
  {
    if (a->cle == valeur)
      return a;
    else
    {
      if (a->cle < valeur)
        return rechercher_cle_arbre(a->fdroite, valeur);
      else
        return rechercher_cle_arbre(a->fgauche, valeur);
    }
  }
}

/* ============================================== */
/**
 * @brief Fonction qui ajouter une cle a un arbre
 * 
 * @param a l'arbre
 * @param cle la cle a rajouter
 * @return Arbre_t l'arbre mis a jour
 */
Arbre_t ajouter_cle(Arbre_t a, int cle)
{
  Arbre_t n;
  Arbre_t ptrouve;

  /*
     ajout de la clé. Creation du noeud n qu'on insere
    dans l'arbre a
  */

  ptrouve = rechercher_cle_arbre(a, cle);

  if (ptrouve == NULL)
  {
    n = (Arbre_t)malloc(sizeof(noeud_t));
    n->cle = cle;
    n->fgauche = NULL;
    n->fdroite = NULL;

    a = ajouter_noeud(a, n);
    return a;
  }
  else
    return a;
}

/* ============================================== */
/**
 * @brief Fonction créant un arbre a partie d'un fichier
 * 
 * @param nom_fichier le fichier
 * @return Arbre_t l'arbre
 */
Arbre_t lire_arbre(char *nom_fichier)
{
  FILE *f;
  int cle;
  Arbre_t a = NULL;

  f = fopen(nom_fichier, "r");

  while (fscanf(f, "%d", &cle) != EOF)
  {
    a = ajouter_cle(a, cle);
  }

  fclose(f);

  return a;
}

/* ============================================== */
/**
 * @brief Fonction qui affiche l'arbre sur la console
 * 
 * @param a l'arbre a afficher
 * @param niveau le niveau de départ
 */
void afficher_arbre(Arbre_t a, int niveau)
{
  /*
    affichage de l'arbre a
    on l'affiche en le penchant sur sa gauche
    la partie droite (haute) se retrouve en l'air
  */

  int i;

  if (a != NULL)
  {
    afficher_arbre(a->fdroite, niveau + 1);

    for (i = 0; i < niveau; i++)
      printf("\t");
    printf(" %d (%d)\n\n", a->cle, niveau);

    afficher_arbre(a->fgauche, niveau + 1);
  }
  return;
}

/* ============================================== */
/**
 * @brief Fonction qui calcul la hauteur d'un arbre
 *        RECURSIVEMENT
 * @param a l'arbre
 * @return int la hauteur de l'arbre
 */
int hauteur_arbre_r(Arbre_t a)
{
  if (a == NULL)
    return -1;
  else if (feuille(a))
    return 0;
  else
    return 1 + max(hauteur_arbre_r(a->fgauche), hauteur_arbre_r(a->fdroite));
}

/* ============================================== */
/**
 * @brief Fonction qui calcul la hauteur d'un arbre
 *        ITERATIVEMENT
 * @param a l'arbre 
 * @return int la hauteur de l'arbre
 */
int hauteur_arbre_nr(Arbre_t a)
{
  if (a == NULL)
    return -1;

  int nbElement = 0;
  int compteur = 0;
  int hauteurMax = 0;

  pfile_t f = creer_file();

  if (a->fdroite != NULL)
  {
    enfiler(f, a->fdroite);
    nbElement++;
  }
  if (a->fgauche != NULL)
  {
    enfiler(f, a->fgauche);
    nbElement++;
  }

  compteur = nbElement;

  while (!file_vide(f))
  {

    if (compteur > 0)
    {
      compteur--;
    }

    Arbre_t temp = defiler(f);
    nbElement--;

    if (temp->fgauche != NULL)
    {
      enfiler(f, temp->fgauche);
      nbElement++;
    }
    if (temp->fdroite != NULL)
    {
      enfiler(f, temp->fdroite);
      nbElement++;
    }

    if (compteur == 0)
    {
      hauteurMax++;
      compteur = nbElement;
    }
  }

  return hauteurMax;
}

/* ============================================== */
/**
 * @brief Parcour d'un arbre en largeur
 * 
 * @param a l'arbre a parcourir
 */
void parcourir_arbre_largeur(Arbre_t a)
{

  pfile_t f = creer_file();

  if (a == NULL)
  {
    // RIEN
  }
  else
  {

    if (a->fgauche != NULL)
    {
      enfiler(f, a->fgauche);
    }
    if (a->fdroite != NULL)
    {
      enfiler(f, a->fdroite);
    }

    printf("| %i |", a->cle);

    while (!file_vide(f))
    {

      Arbre_t temp = defiler(f);
      printf("| %i |", temp->cle);

      if (temp->fgauche != NULL)
      {
        enfiler(f, temp->fgauche);
      }
      if (temp->fdroite != NULL)
      {
        enfiler(f, temp->fdroite);
      }
    }

    printf("\n");
  }

  return;
}

/* ============================================== */
/**
 * @brief Fonction qui compte le nombre de cle d'un arbre
 *        RECURSIVEMENT
 * @param a l'arbre
 * @return int le nombre de cle
 */
int nombre_cles_arbre_r(Arbre_t a)
{
  if (a == NULL)
  {
    return 0;
  }
  if (feuille(a))
  {
    return 1;
  }
  else
  {
    return 1 + (nombre_cles_arbre_r(a->fgauche)) + (nombre_cles_arbre_r(a->fdroite));
  }
}

/* ============================================== */
/**
 * @brief Fonction qui compte le nombre de cle d'un arbre
 *        ITERATIVEMENT
 * @param a l'arbre
 * @return int le nombre de cle
 */
int nombre_cles_arbre_nr(Arbre_t a)
{

  int nbCle = 0;
  if (a == NULL)
  {
    return nbCle;
  }

  pfile_t f = creer_file();

  nbCle++;

  if (a->fdroite != NULL)
  {
    enfiler(f, a->fdroite);
  }
  if (a->fgauche != NULL)
  {
    enfiler(f, a->fgauche);
  }

  while (!file_vide(f))
  {
    nbCle++;

    Arbre_t temp = defiler(f);

    if (temp->fgauche != NULL)
    {
      enfiler(f, temp->fgauche);
    }
    if (temp->fdroite != NULL)
    {
      enfiler(f, temp->fdroite);
    }
  }

  return nbCle;
}

/* ============================================== */
/**
 * @brief Fonction qui trouve la plus petite cle de l'arbre
 * 
 * @param a l'arbre
 * @return int la plus petite cle
 */
int trouver_cle_min(Arbre_t a)
{
  if (a == NULL)
  {
    return -1;
  }

  if (a->fgauche == NULL)
  {
    return a->cle;
  }

  else
  {
    return trouver_cle_min(a->fgauche);
  }
}

/* ============================================== */
/**
 * @brief Fonction qui affiche la liste des clé triées
 *        RECURSIVEMENT
 * @param a l'arbre
 */
void imprimer_liste_cle_triee_r(Arbre_t a)
{

  if (a == NULL)
  {
    fprintf(stderr, "Arbre Vide ! ");
    return;
  }

  if (a->fgauche != NULL)
    imprimer_liste_cle_triee_r(a->fgauche);

  printf("| %i |", a->cle);

  if (a->fdroite != NULL)
    imprimer_liste_cle_triee_r(a->fdroite);

  return;
}

/* ============================================== */
/**
 * @brief Fonction qui affiche la liste des clé triées
 *        ITERATIVEMENT
 * @param a l'arbre
 */
void imprimer_liste_cle_triee_nr(Arbre_t a)
{
  if (a == NULL)
  {
    fprintf(stderr, "Arbre Vide ! ");
    return;
  }

  ppile_t p = creer_pile();
  Arbre_t temp = a;
  empiler(p, a);

  while (1)
  { // WHILE 1 temp != NULL
    if (temp->fgauche != NULL)
    {
      empiler(p, temp->fgauche);
      temp = temp->fgauche;
    }
    else
    {
      break;
    }
  }

  pnoeud_t res;

  while (!pile_vide(p))
  {
    res = depiler(p);
    printf("| %i |", res->cle);

    if (res->fdroite != NULL)
    {
      empiler(p, res->fdroite);
      Arbre_t test = res->fdroite;
      // Empiler tout les noeud a gauche
      while (1)
      {
        if (test->fgauche != NULL)
        {
          empiler(p, test->fgauche);
          test = test->fgauche;
        }
        else
        {
          break;
        }
      }
    }
  }
}

/* ============================================== */
/**
 * @brief Fonction qui indique si l'arbres est plein ou non
 * 
 * @param a l'arbre
 * @return int 1 -> si plein
 *             0 -> sinon
 */
int arbre_plein(Arbre_t a)
{
  if (a == NULL)
    return 0;

  if (!feuille(a) && (a->fdroite == NULL || a->fgauche == NULL))
    return 0;

  if (feuille(a))
    return 1;

  else
  {
    int res1 = arbre_plein(a->fdroite);
    int res2 = arbre_plein(a->fgauche);

    if (!res1 || !res2)
      return 0;
  }

  return 1;
}

/* ============================================== */
/**
 * @brief Fonction qui indique si l'arbres est parfait ou non
 * 
 * @param a l'arbre
 * @return int 1 -> si parfait
 *             0 -> sinon
 */
int arbre_parfait(Arbre_t a)
{

  if (a == NULL)
    return 0;

  int hauteurMax = hauteur_arbre_r(a);

  pfile_t f = creer_file();
  enfiler(f, a);

  int profArbre = 0;
  int nbElement = 0;
  int compteur = 0;

  if (a->fgauche != NULL)
  {
    enfiler(f, a->fgauche);
    nbElement++;
  }

  if (a->fdroite != NULL)
  {
    enfiler(f, a->fdroite);
    nbElement++;
  }

  if (a->fgauche == NULL && a->fdroite != NULL)
    return 0;

  compteur = nbElement;

  while (!file_vide(f))
  {

    if (compteur > 0)
    {
      compteur--;
    }

    pnoeud_t temp = defiler(f);
    nbElement--;

    if (temp->fgauche == NULL && temp->fdroite != NULL)
      return 0;

    if (temp->fdroite == NULL)
    {
      if (profArbre + 2 < hauteurMax)
      {
        return 0;
      }
    }

    if (temp->fgauche != NULL)
    {
      enfiler(f, temp->fgauche);
      nbElement++;
    }

    if (temp->fdroite != NULL)
    {
      enfiler(f, temp->fdroite);
      nbElement++;
    }

    if (compteur == 0)
    {
      profArbre++;
      compteur = nbElement;
    }
  }

  return 1;
}

/* ============================================== */
/**
 * @brief Fonction qui cherche la premiere cle la plus grande
 * d'une valeur dans un arbre
 * 
 * @param a l'arbre
 * @param valeur la valeur
 * @return Arbre_t la cle directement plus grande
 *          NULL -> si aucune
 */
Arbre_t rechercher_cle_sup_arbre(Arbre_t a, int valeur)
{
  if(a == NULL){
    return NULL;
  }
  Arbre_t a1 = rechercher_cle_sup_arbre(a->fgauche,valeur);
  if(a1 != NULL){
    return a1;
  }
  Arbre_t a2 = rechercher_cle_sup_arbre(a->fdroite,valeur);
  if(a->cle > valeur){return a;}
  return a2;
}

/* ============================================== */
/**
 * @brief Fonction qui cherche la premiere cle la plus petite
 * d'une valeur dans un arbre
 * 
 * @param a l'arbre
 * @param valeur la valeur
 * @return Arbre_t la cle directement plus petite
 *          NULL -> si aucune
 */
Arbre_t rechercher_cle_inf_arbre(Arbre_t a, int valeur)
{
  if(a == NULL){
    return NULL;
  }
  Arbre_t a1 = rechercher_cle_inf_arbre(a->fdroite,valeur);
  if(a1 != NULL){ // != NULL
    return a1;
  }
  Arbre_t a2 = rechercher_cle_inf_arbre(a->fgauche,valeur);
  if(a->cle < valeur){return a;}
  return a2;
}

/* ============================================== */
// //  Trois cas principaux
// //  - Cas 1 supprimer une feuille
// //  - Cas 2 supprimer un noeud interne avec 2 fils
// //  - Cas 3 supprimer un noeud interne avec 1 fils

/**
 * @brief Fonction qui supprime une cle d'un arbre
 * 
 * @param a l'arbre
 * @param cle la cle a supprimer
 * @return Arbre_t l'arbre mis a jour
 */
Arbre_t detruire_cle_arbre(Arbre_t a, int cle)
{

  // Si le noeud n'existe pas
  if (rechercher_cle_arbre(a, cle) == NULL)
  {
    fprintf(stderr,"Nothing to change\n");
    return a;
  }

  if(a->cle == cle){
    //LES 3 CAS

    //CAS 1
    if(feuille(a)){
      free(a);
      return NULL;
    }

    //CAS 2
    else if(a->fdroite != NULL && a->fgauche != NULL){

      Arbre_t test = a->fgauche;
      Arbre_t prec = test;

      while (test->fdroite != NULL)
      {
        prec = test;
        test = test->fdroite;
      }

      if (prec != test)
      {
        prec->fdroite = NULL;
        a = test;
      }
      else{
        a = test;
      }
      return a;
    }

    //CAS 3
    else{
      if(a->fdroite != NULL){
        a = a->fdroite;
      }
      else{
        a = a->fgauche;
      }
      return a;
    }
  }

  Arbre_t temp = a;      // Le noeud a supprimer
  Arbre_t parent = temp; // Le noeud parent du noeud a supprimer

  while (temp->cle != cle)
  {
    parent = temp;

    if (temp->cle < cle)
    {
      temp = temp->fdroite;
    }
    else if (temp->cle > cle)
    {
      temp = temp->fgauche;
    }
  }

  // LES TROIS CAS
  //  Cas 1
  if (feuille(temp))
  {

    if (parent->fgauche == temp)
    {
      parent->fgauche = NULL;
      free(temp);
    }
    else
    {
      parent->fdroite = NULL;
      free(temp);
    }

    return a;
  }

  // Cas 2
  else if (temp->fgauche != NULL && temp->fdroite != NULL)
  {

    Arbre_t res = temp->fgauche;

    Arbre_t prec = res;
    while (res->fdroite != NULL)
    {
      prec = res;
      res = res->fdroite;
    }
    if (prec != res)
    {
      prec->fdroite = NULL;

      if (parent->fgauche == temp)
      {
        parent->fgauche = res;
      }
      else
      {
        parent->fdroite = res;
      }
      res->fgauche = temp->fgauche;
      res->fdroite = temp->fdroite;
    }
    else
    {
      if (parent->fgauche == temp)
      {
        parent->fgauche = res;
      }
      else
      {
        parent->fdroite = res;
      }
      res->fdroite = temp->fdroite;
    }

    free(temp);

    return a;
  }

  // Cas 3
  else
  {
    Arbre_t res = NULL;

    if (temp->fdroite != NULL)
    {
      res = temp->fdroite;
    }
    else
    {
      res = temp->fgauche;
    }

    if (parent->fgauche == temp)
    {
      parent->fgauche = res;
      free(temp);
    }
    else
    {
      parent->fdroite = res;
      free(temp);
    }
    

    return a;
  }
}

/* ============================================== */
/**
 * @brief Fonction qui creer un arbre qui est l'intersection
 * de deux arbre
 * 
 * @param a1 l'arbre 1
 * @param a2 l'arbre 2
 * @return Arbre_t l'intersection de l'arbre 1 et l'arbre 2
 */
Arbre_t intersection_deux_arbres(Arbre_t a1, Arbre_t a2)
{
  if(a1 == NULL || a2 == NULL){
    return NULL;
  }

  Arbre_t res = NULL;
  pfile_t f = creer_file();

  if(a1->fdroite != NULL){
    enfiler(f, a1->fdroite);
  }
  if(a1->fgauche != NULL){
    enfiler(f, a1->fgauche);
  }

  if(rechercher_cle_arbre(a2,a1->cle) != NULL){
    res = ajouter_cle(res,a1->cle);
  }

  while(!file_vide(f)){

    Arbre_t temp = defiler(f);

    if(temp == NULL){
      break;
    }

    if(rechercher_cle_arbre(a2,temp->cle) != NULL){
      res = ajouter_cle(res,temp->cle);
    }

    if(temp->fdroite != NULL){
      enfiler(f, temp->fdroite);
    }
    if(temp->fgauche != NULL){
      enfiler(f, temp->fgauche);
    }

  }

  return res;
}

/* ============================================== */
/**
 * @brief Fonction qui creer un arbre qui est l'union
 * de deux arbre
 * 
 * @param a1 l'arbre 1
 * @param a2 l'arbre 2
 * @return Arbre_t l'union de l'arbre 1 et l'arbre 2
 */
Arbre_t union_deux_arbres(Arbre_t a1, Arbre_t a2)
{
  if(a1 == NULL || a2 == NULL){
    
    if(a1 == NULL && a2 == NULL){
      return NULL;
    }
    else if(a1 == NULL){
      return a2;
    }
    else{
      return a1;
    }
  }

  Arbre_t res = NULL;
  pfile_t file1 = creer_file();

  if(a1->fdroite != NULL){
    enfiler(file1, a1->fdroite);
  }
  if(a1->fgauche != NULL){
    enfiler(file1, a1->fgauche);
  }

  res = ajouter_cle(res,a1->cle);

  while(!file_vide(file1)){

    Arbre_t temp = defiler(file1);

    if(temp == NULL){
      break;
    }

    res = ajouter_cle(res,temp->cle);

    if(temp->fdroite != NULL){
      enfiler(file1, temp->fdroite);
    }
    if(temp->fgauche != NULL){
      enfiler(file1, temp->fgauche);
    }
  }


  pfile_t file2 = creer_file();

  if(a2->fdroite != NULL){
    enfiler(file2, a2->fdroite);
  }
  if(a2->fgauche != NULL){
    enfiler(file2, a2->fgauche);
  }

  res = ajouter_cle(res,a2->cle);

  while(!file_vide(file2)){

    Arbre_t temp = defiler(file2);

    if(temp == NULL){
      break;
    }

    Arbre_t isExisting = rechercher_cle_arbre(a1,temp->cle);

    if(isExisting == NULL){
      res = ajouter_cle(res,temp->cle);
    }

    if(temp->fdroite != NULL){
      enfiler(file2, temp->fdroite);
    }
    if(temp->fgauche != NULL){
      enfiler(file2, temp->fgauche);
    }
  }

  return res;
}