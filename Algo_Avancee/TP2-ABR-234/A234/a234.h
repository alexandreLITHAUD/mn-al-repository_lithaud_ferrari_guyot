#ifndef A234
#define A234

/* structure noeud presentee en cours */


typedef struct n {
  
  int      t         ; /* t type de noeud 0, 2, 3 ou 4 */ // POUR T = 3 
  int      cles  [3] ;                                    // CLES = 2
  struct n *fils [4] ;                                    // FILS = 3
  
} noeud234, *pnoeud234 ;


/* type Arbre, pointeur vers un noeud */

typedef pnoeud234 Arbre234 ;

typedef struct m {
  int num;
  Arbre234  mem;
} cellule;

Arbre234 lire_arbre (char *nom_fichier) ;

void afficher_arbre (Arbre234 a, int niveau) ;

void ajouter_cle (Arbre234 *a, int cle, int niveau, Arbre234 pere) ;

  
#endif