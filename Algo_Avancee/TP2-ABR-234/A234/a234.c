#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "file.h"
#include "pile.h"

#include "a234.h"

#define max(a, b) ((a) > (b) ? (a) : (b))

int somme_cles(Arbre234 a);
/* =======FONCTION DESTRUIRE CLES======= */
Arbre234 updateGauche(Arbre234 parent, int i);
Arbre234 updateDroit(Arbre234 parent, int i);
Arbre234 updateFusion(Arbre234 parent, int i);
Arbre234 remonter(Arbre234 a, ppile_t pile);
Arbre234 rotationGauche(Arbre234 parent, Arbre234 a, int i);
Arbre234 rotationDroite(Arbre234 parent, Arbre234 a, int i);
Arbre234 fusion(Arbre234 parent, int i);
Arbre234 feuille(Arbre234 a, int cle, ppile_t pile);
Arbre234 minSearch(Arbre234 a);
Arbre234 swap(Arbre234 a, Arbre234 min, int i);
Arbre234 found(Arbre234 a, int cle, ppile_t pile);
Arbre234 merge_1_1_1(Arbre234 a);
Arbre234 search(Arbre234 a, int cle, ppile_t pile);
/* ===================================== */


int hauteur(Arbre234 a)
{
  int h0, h1, h2, h3;

  if (a == NULL)
    return 0;

  if (a->t == 0)
    return 0;

  h0 = hauteur(a->fils[0]);
  h1 = hauteur(a->fils[1]);
  h2 = hauteur(a->fils[2]);
  h3 = hauteur(a->fils[3]);

  return 1 + max(max(h0, h1), max(h2, h3));
}

int NombreCles(Arbre234 a)
{

  int h0 = 0;
  int h1 = 0;
  int h2 = 0;
  int h3 = 0;

  if (a == NULL)
    return 0;

  if (a->t == 0)
    return 0;

  if (a->t == 2)
  {
    h1 = NombreCles(a->fils[1]);
    h2 = NombreCles(a->fils[2]);
  }

  if (a->t == 3)
  {
    h0 = NombreCles(a->fils[0]);
    h1 = NombreCles(a->fils[1]);
    h2 = NombreCles(a->fils[2]);
  }

  if (a->t == 4)
  {
    h0 = NombreCles(a->fils[0]);
    h1 = NombreCles(a->fils[1]);
    h2 = NombreCles(a->fils[2]);
    h3 = NombreCles(a->fils[3]);
  }

  return h0 + h1 + h2 + h3 + (a->t - 1);
}

int CleMax(Arbre234 a)
{
  int max = 0;
  Arbre234 temp = a;

  while (temp->t != 0)
  {
    switch (temp->t)
    {
    case 2:
      max = temp->cles[0];
      temp = temp->fils[2];
      break;

    case 3:
      max = temp->cles[1];
      temp = temp->fils[2];
      break;

    case 4:
      max = temp->cles[2];
      temp = temp->fils[3];
      break;
    }
  }
  return max;
}

int CleMin(Arbre234 a)
{

  int res = 0;
  Arbre234 temp = a;
  while (temp->t != 0)
  {

    switch (temp->t)
    {
    case 2:
      res = temp->cles[1];
      temp = temp->fils[1];
      break;

    case 3:
      res = temp->cles[0];
      temp = temp->fils[0];
      break;

    case 4:
      res = temp->cles[0];
      temp = temp->fils[0];
      break;
    }
  }
  return res;
}

Arbre234 RechercherCle(Arbre234 a, int cle)
{
  /*
     rechercher si la cle a est presente dans
     l'arbre a. Si oui, retourne le noeud ou se trouve la cle.
  */

  Arbre234 b0 = NULL;
  Arbre234 b1 = NULL;
  Arbre234 b2 = NULL;
  Arbre234 b3 = NULL;

  if (a == NULL)
  {
    return NULL;
  }

  if (a->t == 0)
  {
    return NULL;
  }

  if (a->t == 2)
  {
    if (a->cles[1] == cle)
    {
      return a;
    }
    b1 = RechercherCle(a->fils[1], cle);
    b2 = RechercherCle(a->fils[2], cle);
  }

  if (a->t == 3)
  {
    for (int i = 0; i < 2; i++)
    {
      if (a->cles[i] == cle)
      {
        return a;
      }
      b0 = RechercherCle(a->fils[0], cle);
      b1 = RechercherCle(a->fils[1], cle);
      b2 = RechercherCle(a->fils[2], cle);
    }
  }

  if (a->t == 4)
  {
    for (int i = 0; i < 3; i++)
    {
      if (a->cles[i] == cle)
      {
        return a;
      }
      b0 = RechercherCle(a->fils[0], cle);
      b1 = RechercherCle(a->fils[1], cle);
      b2 = RechercherCle(a->fils[2], cle);
      b3 = RechercherCle(a->fils[3], cle);
    }
  }

  if (b0 != NULL)
  {
    return b0;
  }
  if (b1 != NULL)
  {
    return b1;
  }
  if (b2 != NULL)
  {
    return b2;
  }
  return b3;
}

void AnalyseStructureArbre(Arbre234 a, int *feuilles, int *noeud2, int *noeud3, int *noeud4)
{
  if (a == NULL)
    return;

  if (a->t == 0)
  {
    *feuilles += 1;
    return;
  }

  if (a->t == 2)
  {
    *noeud2 += 1;
    AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
  }

  if (a->t == 3)
  {
    *noeud3 += 1;
    AnalyseStructureArbre(a->fils[0], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
  }

  if (a->t == 4)
  {
    *noeud4 += 1;
    AnalyseStructureArbre(a->fils[0], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[1], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[2], feuilles, noeud2, noeud3, noeud4);
    AnalyseStructureArbre(a->fils[3], feuilles, noeud2, noeud3, noeud4);
  }

  return;
}

Arbre234 noeud_max(Arbre234 a)
{
  Arbre234 Noeud_Max = a;
  Arbre234 FilsG, FilsM, FilsD, FilsExtremeD;
  if (a->t == 0)
  {
    return Noeud_Max;
  }

  if (a->t == 2)
  {
    FilsM = noeud_max(a->fils[1]);
    if (somme_cles(FilsM) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsM;
    }
    FilsD = noeud_max(a->fils[2]);
    if (somme_cles(FilsD) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsD;
    }
  }

  if (a->t == 3)
  {
    FilsG = noeud_max(a->fils[0]);
    if (somme_cles(FilsG) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsG;
    }
    FilsM = noeud_max(a->fils[1]);
    if (somme_cles(FilsM) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsM;
    }
    FilsD = noeud_max(a->fils[2]);
    if (somme_cles(FilsD) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsD;
    }
  }

  if (a->t == 4)
  {
    FilsG = noeud_max(a->fils[0]);
    if (somme_cles(FilsG) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsG;
    }
    FilsM = noeud_max(a->fils[1]);
    if (somme_cles(FilsM) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsM;
    }
    FilsD = noeud_max(a->fils[2]);
    if (somme_cles(FilsD) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsD;
    }
    FilsExtremeD = noeud_max(a->fils[3]);
    if (somme_cles(FilsExtremeD) > somme_cles(Noeud_Max))
    {
      Noeud_Max = FilsExtremeD;
    }
  }
  return Noeud_Max;
}

void Afficher_Cles_Largeur(Arbre234 a)
{

  pfile_t f = creer_file();

  switch (a->t)
  {
  case 2:

    printf("%d / ", a->cles[1]);

    enfiler(f, a->fils[1]);
    enfiler(f, a->fils[2]);
    break;

  case 3:

    printf("%d / %d / ", a->cles[0], a->cles[1]);

    enfiler(f, a->fils[0]);
    enfiler(f, a->fils[1]);
    enfiler(f, a->fils[2]);
    break;

  case 4:

    printf("%d / %d / %d / ", a->cles[0], a->cles[1], a->cles[2]);

    enfiler(f, a->fils[0]);
    enfiler(f, a->fils[1]);
    enfiler(f, a->fils[2]);
    enfiler(f, a->fils[3]);
    break;

  default:
    return;
  }

  while (!file_vide(f))
  {

    Arbre234 a2 = defiler(f);

    switch (a2->t)
    {
    case 2:

      printf("%d / ", a2->cles[1]);

      enfiler(f, a2->fils[1]);
      enfiler(f, a2->fils[2]);
      break;

    case 3:

      printf("%d / %d / ", a2->cles[0], a2->cles[1]);

      enfiler(f, a2->fils[0]);
      enfiler(f, a2->fils[1]);
      enfiler(f, a2->fils[2]);
      break;

    case 4:

      printf("%d / %d / %d / ", a2->cles[0], a2->cles[1], a2->cles[2]);

      enfiler(f, a2->fils[0]);
      enfiler(f, a2->fils[1]);
      enfiler(f, a2->fils[2]);
      enfiler(f, a2->fils[3]);
      break;

    default:
      break;
    }
  }

  printf("\n");

  return;
}

void Affichage_Cles_Triees_Recursive(Arbre234 a)
{

  if (a->t == 2)
  {
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d / ", a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
  }
  if (a->t == 3)
  {
    Affichage_Cles_Triees_Recursive(a->fils[0]);
    printf("%d / ", a->cles[0]);
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d / ", a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
  }
  if (a->t == 4)
  {
    Affichage_Cles_Triees_Recursive(a->fils[0]);
    printf("%d / ", a->cles[0]);
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d / ", a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
    printf("%d / ", a->cles[2]);
    Affichage_Cles_Triees_Recursive(a->fils[3]);
  }
}

void Affichage_Cles_Triees_NonRecursive(Arbre234 a)
{
  ppile_t p = creer_pile();
  cellule temp;
  // permet d'enregistrer le nombre de passage dans le noeud lors de l'affichage
  temp.num = 0;

  temp.mem = a;

  while (temp.mem->t != 0)
  {
    empiler(p, temp);
    if (temp.mem->t != 0)
    {
      if (temp.mem->t == 2)
      {
        temp.mem = temp.mem->fils[1];
      }
      else
      {
        temp.mem = temp.mem->fils[0];
      }
    }
  }

  while (!pile_vide(p))
  {
    cellule res = depiler(p);
    if (res.mem->t != 0)
    {
      if (res.mem->t == 2)
      {
        printf("%d / ", res.mem->cles[1]);
        if (res.mem->fils[2] != NULL)
        {
          temp.mem = res.mem->fils[2];
          while (temp.mem->t != 0)
          {
            empiler(p, temp);
            if (temp.mem->t != 0)
            {
              if (temp.mem->t == 2)
              {
                temp.mem = temp.mem->fils[1];
              }
              else
              {
                temp.mem = temp.mem->fils[0];
              }
            }
          }
        }
      }
      else
      {
        if (res.num < res.mem->t - 1)
        {
          printf("%d / ", res.mem->cles[res.num]);
          res.num += 1;
          if (res.num < res.mem->t)
          {
            if (res.mem->fils[res.num] != NULL)
            {
              empiler(p, res);
              temp.mem = res.mem->fils[res.num];
              while (temp.mem->t != 0)
              {
                empiler(p, temp);
                if (temp.mem->t != 0)
                {
                  if (temp.mem->t == 2)
                  {
                    temp.mem = temp.mem->fils[1];
                  }
                  else
                  {
                    temp.mem = temp.mem->fils[0];
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* ========================================================================== */

void Detruire_Cle(Arbre234 *a, int cle)
{

  ppile_t pile = creer_pile();
  pnoeud234 c = *a;
  if (c->fils[1]->t != 0)
  {
    search(c, cle, pile);
    merge_1_1_1(c);
    return;
  }
  else
  {
    if (c->t == 2 && c->cles[1] == cle)
    {
      c->t = 0;
    }
    else
    {
      if (c->t == 3)
      {
        if (c->cles[0] == cle)
        {
          c->t = 2;
        }
        if (c->cles[1] == cle)
        {
          c->t = 2;
          c->cles[1] = c->cles[0];
        }
      }
      if (c->t == 4)
      {
        if (c->cles[0] == cle)
        {
          c->t = 3;
          c->cles[0] = c->cles[1];
          c->cles[1] = c->cles[2];
        }
        if (c->cles[1] == cle)
        {
          c->t = 3;
          c->cles[1] = c->cles[2];
        }
        if (c->cles[2] == cle)
        {
          c->t = 3;
        }
      }
    }
  }
}

Arbre234 search(Arbre234 a, int cle, ppile_t pile)
{
  cellule cellule = {0, a};
  if (a->t == 0)
  {
    return a;
  }
  if (a->t == 2)
  {
    if (a->cles[1] == cle)
    {
      return found(a, cle, pile);
    }
    else
    {
      if (a->cles[1] > cle)
      {
        cellule.num = 2;
        empiler(pile, cellule);
        return search(a->fils[1], cle, pile);
      }
      if (a->cles[1] < cle)
      {
        cellule.num = 1;
        empiler(pile, cellule);
        return search(a->fils[2], cle, pile);
      }
    }
  }
  if (a->cles[0] == cle || a->cles[1] == cle || (a->t == 4 && a->cles[2] == cle))
  {
    return found(a, cle, pile);
  }
  if (cle < a->cles[0])
  {
    cellule.num = 0;
    empiler(pile, cellule);
    return search(a->fils[0], cle, pile);
  }
  if (cle > a->cles[0] && cle < a->cles[1])
  {
    cellule.num = 1;
    empiler(pile, cellule);
    return search(a->fils[1], cle, pile);
  }
  if (cle > a->cles[1] && a->t == 3)
  {
    cellule.num = 2;
    empiler(pile, cellule);
    return search(a->fils[2], cle, pile);
  }
  if (cle > a->cles[1] && cle < a->cles[2])
  {
    cellule.num = 3;
    empiler(pile, cellule);
    return search(a->fils[2], cle, pile);
  }
  if (cle > a->cles[2])
  {
    cellule.num = 4;
    empiler(pile, cellule);
    return search(a->fils[3], cle, pile);
  }
  return a;
}

Arbre234 merge_1_1_1(Arbre234 a)
{
  Arbre234 fg = a->fils[1];
  Arbre234 fd = a->fils[2];
  if (a->t == 2 && fg->t == 2 && fd->t == 2)
  {
    a->t = 4;
    a->cles[0] = fg->cles[1];
    a->cles[2] = fd->cles[1];
    a->fils[0] = fg->fils[1];
    a->fils[1] = fg->fils[2];
    a->fils[2] = fd->fils[1];
    a->fils[3] = fd->fils[2];
  }
  return a;
}

Arbre234 found(Arbre234 a, int cle, ppile_t pile)
{
  if (a->fils[1]->t == 0)
  {
    return feuille(a, cle, pile);
  }
  else
  {
    if (a->cles[0] == cle && !(a->t == 2))
    {
      Arbre234 min = minSearch(a->fils[1]);
      swap(a, min, 0);
      empiler(pile, (cellule){1, a});
      search(a->fils[1], cle, pile);
      return a;
    }
    if (a->cles[1] == cle)
    {
      Arbre234 min = minSearch(a->fils[2]);
      swap(a, min, 1);
      empiler(pile, (cellule){2, a});
      search(a->fils[2], cle, pile);
      return a;
    }
    if (a->cles[2] == cle)
    {
      Arbre234 min = minSearch(a->fils[3]);
      swap(a, min, 2);
      empiler(pile, (cellule){3, a});
      search(a->fils[3], cle, pile);
      return a;
    }
  }
  return NULL;
}

Arbre234 swap(Arbre234 a, Arbre234 min, int i)
{
  if (min->t == 2)
  {
    int temp = min->cles[1];
    min->cles[1] = a->cles[i];
    a->cles[i] = temp;
  }
  else
  {
    int temp = min->cles[0];
    min->cles[0] = a->cles[i];
    a->cles[i] = temp;
  }
  return a;
}

Arbre234 minSearch(Arbre234 a)
{
  if (a->fils[1]->t == 0)
  {
    return a;
  }
  else
  {
    if (a->t == 2)
    {
      return minSearch(a->fils[1]);
    }
    return minSearch(a->fils[0]);
  }
}

Arbre234 feuille(Arbre234 a, int cle, ppile_t pile)
{
  if (a->t != 2)
  {
    if (a->t == 3)
    {
      if (a->cles[0] == cle)
      {
        a->t = 2;
      }
      else
      {
        a->t = 2;
        a->cles[1] = a->cles[0];
      }
    }
    if (a->t == 4)
    {
      if (a->cles[0] == cle)
      {
        a->t = 3;
        a->cles[0] = a->cles[1];
        a->cles[1] = a->cles[2];
      }
      else
      {
        if (a->cles[1] == cle)
        {
          a->t = 3;
          a->cles[1] = a->cles[2];
        }
        else
        {
          a->t = 3;
        }
      }
    }
    return a;
  }
  else
  {
    cellule cellule = depiler(pile);
    Arbre234 parent = cellule.mem;
    int i = cellule.num;
    if (parent->t != 2)
    {
      if (i == 0)
      {
        if (parent->fils[1]->t != 2)
        {
          return rotationGauche(parent, a, i);
        }
        else
        {
          return fusion(parent, i);
        }
      }
      if ((i == 2 && parent->t == 3) || (i == 3 && parent->t == 4))
      {
        if (parent->fils[i - 1]->t != 2)
        {
          return rotationDroite(parent, a, i);
        }
        else
        {
          return fusion(parent, i);
        }
      }
      if (parent->fils[i - 1]->t != 2)
      {
        return rotationDroite(parent, a, i);
      }
      else
      {
        if (parent->fils[i + 1]->t != 2)
        {
          return rotationGauche(parent, a, i);
        }
        else
        {
          return fusion(parent, i);
        }
      }
    }
    else
    {
      if (parent->fils[1]->t == 2 && parent->fils[2]->t == 2)
      {
        if (parent->fils[1] == a)
        {
          parent->t = 3;
          parent->cles[0] = parent->cles[1];
          parent->cles[1] = parent->fils[2]->cles[1];
        }
        if (parent->fils[2] == a)
        {
          parent->t = 3;
          parent->cles[0] = parent->fils[1]->cles[1];
        }
        for (int k = 0; k < 4; k++)
        {
          parent->fils[k]->t = 0;
        }
        return remonter(parent, pile);
      }
      if (parent->fils[1] == a)
      {
        i = 1;
        if (parent->fils[2]->t != 2)
        {
          return rotationGauche(parent, a, i);
        }
      }
      if (parent->fils[2] == a)
      {
        i = 2;
        if (parent->fils[1]->t != 2)
        {
          return rotationDroite(parent, a, i);
        }
      }
    }
  }
  return NULL;
}

Arbre234 fusion(Arbre234 parent, int i)
{
  free(parent->fils[i]);
  if (i == 0)
  {
    parent->t = parent->t - 1;
    parent->fils[1]->t = 3;
    parent->fils[1]->cles[0] = parent->cles[0];
  }
  if (i == 1)
  {
    parent->t = parent->t - 1;
    parent->fils[0]->t = 3;
    parent->fils[0]->cles[0] = parent->cles[0];
    parent->fils[1] = parent->fils[0];
  }
  if (i == 2 && parent->t == 3)
  {
    parent->t = 2;
    parent->fils[2] = parent->fils[1];
    parent->fils[1] = parent->fils[0];
    parent->fils[2]->t = 3;
    parent->fils[2]->cles[0] = parent->cles[1];
    parent->cles[1] = parent->cles[0];
  }
  if (i == 2 && parent->t == 4)
  {
    parent->t = 3;
    parent->fils[2] = parent->fils[3];
    parent->fils[2]->t = 3;
    parent->fils[2]->cles[0] = parent->cles[2];
  }
  if (i == 3 && parent->t == 4)
  {
    parent->t = 3;
    parent->fils[2]->t = 3;
    parent->fils[2]->cles[0] = parent->fils[2]->cles[1];
    parent->fils[2]->cles[1] = parent->cles[2];
  }
  return parent;
}

Arbre234 rotationDroite(Arbre234 parent, Arbre234 a, int i)
{
  if (parent->t != 2)
  {
    a->cles[1] = parent->cles[i - 1];
    if (parent->fils[i - 1]->t == 3)
    {
      parent->cles[i - 1] = parent->fils[i - 1]->cles[1];
      parent->fils[i - 1]->t = 2;
      parent->fils[i - 1]->cles[1] = parent->fils[i - 1]->cles[0];
    }
    else
    {
      parent->cles[i - 1] = parent->fils[i - 1]->cles[2];
      parent->fils[i - 1]->t = 3;
    }
  }
  else
  {
    a->cles[1] = parent->cles[1];
    if (parent->fils[i - 1]->t == 3)
    {
      parent->fils[i - 1]->t = 2;
      parent->cles[1] = parent->fils[i - 1]->cles[1];
      parent->fils[i - 1]->cles[1] = parent->fils[i - 1]->cles[0];
    }
    if (parent->fils[i - 1]->t == 4)
    {
      parent->fils[i - 1]->t = 3;
      parent->cles[1] = parent->fils[i - 1]->cles[2];
    }
  }
  return parent;
}

Arbre234 rotationGauche(Arbre234 parent, Arbre234 a, int i)
{
  if (parent->t != 2)
  {
    a->cles[1] = parent->cles[i];
    parent->cles[i] = parent->fils[i + 1]->cles[0];
    if (parent->fils[i + 1]->t == 3)
    {
      parent->fils[i + 1]->t = 2;
    }
    else
    {
      parent->fils[i + 1]->t = 3;
      parent->fils[i + 1]->cles[0] = parent->fils[i + 1]->cles[1];
      parent->fils[i + 1]->cles[1] = parent->fils[i + 1]->cles[2];
    }
  }
  else
  {
    a->cles[1] = parent->cles[1];
    if (parent->fils[i + 1]->t == 3)
    {
      parent->fils[i + 1]->t = 2;
      parent->cles[1] = parent->fils[i - 1]->cles[0];
    }
    if (parent->fils[i + 1]->t == 4)
    {
      parent->fils[i + 1]->t = 3;
      parent->cles[1] = parent->fils[i + 1]->cles[0];
      parent->fils[i + 1]->cles[0] = parent->fils[i + 1]->cles[1];
      parent->fils[i + 1]->cles[1] = parent->fils[i + 1]->cles[2];
    }
  }
  return parent;
}

Arbre234 remonter(Arbre234 a, ppile_t pile)
{
  if (pile_vide(pile))
  {
    return NULL;
  }
  cellule cellule = depiler(pile);
  Arbre234 parent = cellule.mem;
  int i = cellule.num;
  if (parent->t == 2)
  {
    if (parent->fils[1]->t == 2 && parent->fils[2]->t == 2)
    {
      Arbre234 temp;
      if (i == 2)
      {
        temp = parent->fils[1];
        parent->t = 3;
        parent->cles[0] = temp->cles[1];
        parent->fils[0] = temp->fils[1];
        parent->fils[1] = temp->fils[2];
        free(temp);
      }
      else
      {
        temp = parent->fils[2];
        parent->t = 3;
        parent->cles[0] = parent->cles[1];
        parent->cles[1] = temp->cles[1];
        parent->fils[0] = parent->fils[1];
        parent->fils[1] = temp->fils[1];
        parent->fils[2] = temp->fils[2];
      }
      return remonter(parent, pile);
    }
    if (parent->fils[1] == a)
    {
      return updateDroit(parent, 1);
    }
    if (parent->fils[2] == a)
    {
      return updateGauche(parent, 2);
    }
  }
  if (i == 0)
  {
    if (parent->fils[1]->t != 2)
    {
      return updateDroit(parent, 0);
    }
    return updateFusion(parent, i);
  }
  if ((i == 2 && parent->t == 3) || (i == 3 && parent->t == 4))
  {
    if (parent->fils[i - 1]->t != 2)
    {
      return updateGauche(parent, i);
    }
    return updateFusion(parent, i);
  }
  if (parent->fils[i - 1]->t != 2)
  {
    return updateGauche(parent, i);
  }
  if (parent->fils[i + 1]->t != 2)
  {
    return updateDroit(parent, i);
  }
  return updateFusion(parent, i);
}

Arbre234 updateFusion(Arbre234 parent, int i)
{
  if (i < parent->t - 1)
  {
    parent->fils[i + 1]->t = 3;
    parent->fils[i + 1]->cles[0] = parent->cles[i];
    parent->fils[i + 1]->fils[0] = parent->fils[i];
    if (parent->t == 3)
    {
      parent->t = 2;
      if (i == 1)
      {
        parent->cles[1] = parent->cles[0];
        parent->fils[1] = parent->fils[0];
      }
    }
    if (parent->t == 4)
    {
      parent->t = 3;
      for (int j = i; j < 2; j++)
      {
        parent->cles[j] = parent->cles[j + 1];
        parent->fils[j] = parent->fils[j + 1];
      }
      parent->fils[2] = parent->fils[3];
    }
    return parent;
  }
  else
  {
    parent->fils[i - 1]->t = 3;
    parent->fils[i - 1]->cles[0] = parent->fils[i - 1]->cles[1];
    parent->fils[i - 1]->fils[0] = parent->fils[i - 1]->fils[1];
    parent->fils[i - 1]->fils[1] = parent->fils[i - 1]->fils[2];
    parent->fils[i - 1]->cles[1] = parent->cles[i - 1];
    parent->fils[i - 1]->fils[2] = parent->fils[i];
    parent->t -= 1;
    if (parent->t == 2)
    {
      parent->cles[1] = parent->cles[0];
      parent->fils[1] = parent->fils[0];
    }
    return parent;
  }
}

Arbre234 updateDroit(Arbre234 parent, int i)
{
  int retenue = parent->cles[i];
  if (parent->fils[i + 1]->t == 3)
  {
    parent->fils[i + 1]->t = 2;
    parent->cles[i] = parent->fils[i + 1]->cles[0];
    Arbre234 NewNoeud = malloc(sizeof(Arbre234));
    NewNoeud->t = 2;
    NewNoeud->cles[1] = retenue;
    NewNoeud->fils[1] = parent->fils[1];
    NewNoeud->fils[2] = parent->fils[2]->fils[0];
    parent->fils[i] = NewNoeud;
    return parent;
  }
  else
  {
    parent->fils[i + 1]->t = 3;
    parent->cles[i] = parent->fils[i + 1]->cles[0];
    Arbre234 NewNoeud = malloc(sizeof(Arbre234));
    NewNoeud->t = 2;
    NewNoeud->cles[1] = retenue;
    NewNoeud->fils[1] = parent->fils[1];
    NewNoeud->fils[2] = parent->fils[2]->fils[0];
    parent->fils[i] = NewNoeud;
    parent->fils[i + 1]->fils[0] = parent->fils[i + 1]->fils[1];
    parent->fils[i + 1]->fils[1] = parent->fils[i + 1]->fils[2];
    parent->fils[i + 1]->fils[2] = parent->fils[i + 1]->fils[3];
    parent->fils[i + 1]->cles[0] = parent->fils[i + 1]->cles[1];
    parent->fils[i + 1]->cles[1] = parent->fils[i + 1]->cles[2];
    return parent;
  }
}

Arbre234 updateGauche(Arbre234 parent, int i)
{
  int retenue = parent->cles[i - 1];
  if (parent->fils[i - 1]->t == 3)
  {
    parent->fils[i - 1]->t = 2;
    parent->cles[i - 1] = parent->fils[i - 1]->cles[1];
    parent->fils[i - 1]->cles[1] = parent->fils[i - 1]->cles[0];
    Arbre234 NewNoeud = malloc(sizeof(Arbre234));
    NewNoeud->t = 2;
    NewNoeud->cles[1] = retenue;
    NewNoeud->fils[2] = parent->fils[i];
    NewNoeud->fils[1] = parent->fils[i - 1]->fils[2];
    parent->fils[i - 1] = NewNoeud;
    parent->fils[i - 1]->fils[1] = parent->fils[i - 1]->fils[0];
    parent->fils[i - 1]->fils[2] = parent->fils[i - 1]->fils[1];
    return parent;
  }
  else
  {
    parent->fils[i - 1]->t = 3;
    parent->cles[i - 1] = parent->fils[i - 1]->cles[2];
    Arbre234 NewNoeud = malloc(sizeof(Arbre234));
    NewNoeud->t = 2;
    NewNoeud->cles[1] = retenue;
    NewNoeud->fils[2] = parent->fils[i];
    NewNoeud->fils[1] = parent->fils[i - 1]->fils[4];
    parent->fils[i - 1] = NewNoeud;
    return parent;
  }
}

/* ========================================================================== */

int somme_cles(Arbre234 a)
{
  Arbre234 temp = a;

  if (temp->t == 0)
  {
    return 0;
  }

  if (temp->t == 2)
  {
    return temp->cles[0];
  }

  if (temp->t == 3)
  {
    return temp->cles[0] + temp->cles[1];
  }

  if (temp->t == 4)
  {
    return temp->cles[0] + temp->cles[1] + temp->cles[2];
  }

  return 0;
}

/* ======================================================================= */

int main(int argc, char **argv)
{
  Arbre234 a;

  if (argc != 2)
  {
    fprintf(stderr, "il manque le parametre nom de fichier\n");
    exit(-1);
  }

  a = lire_arbre(argv[1]);

  /* =====================AFFICHER ARBRE========================== */
  printf("==== Afficher arbre ====\n");
  afficher_arbre(a, 0);
  printf("\n");
  /* ============================================================= */

  /* =======================NOMBRE CLES======================== */
  printf("\nle nombre de clé est : %i \n", NombreCles(a));
  /* ========================================================== */

  /* ======================CLE MAX========================= */
  printf("\nla cle maximum est : %i\n", CleMax(a));
  /* ====================================================== */

  /* ======================CLE MIN========================= */
  printf("\nla cle minimun est : %i\n", CleMin(a));
  printf("\n");
  /* ====================================================== */

  /* ====================RECHERCHE CLE=========================== */
  Arbre234 test = RechercherCle(a, 20);
  if (test != NULL)
  {
    printf("La clé 20 existe dans l'arbre sur un noeud T%d. \n", test->t);
  }
  else
  {
    printf("La clé 20 n'est pas dans l'arbre \n");
  }

  test = RechercherCle(a, 100);
  if (test == NULL)
  {
    printf("La clé 100 n'existe pas dans l'arbre. \n");
  }
  else
  {
    printf("La clé 100 existe dans l'arbre sur un noeud T%d. \n", test->t);
  }
  /* ============================================================ */

  /* ===================ANALYSE STRUCTURE ARBRE============================ */
  int feuilles = 0;
  int noeud2 = 0;
  int noeud3 = 0;
  int noeud4 = 0;

  printf("\n");
  printf("Analyse Structurelle de l'arbre :\n");
  AnalyseStructureArbre(a, &feuilles, &noeud2, &noeud3, &noeud4);
  printf("NB feuille : %i\nNB 2-noeud : %i\nNB 3-noeud : %i\nNB 4-noeud : %i\n", feuilles, noeud2, noeud3, noeud4);
  /* ======================================================================= */

  /* ====================NOEUD MAX=========================== */
  printf("\n");
  printf("Affichage du noeud max :\n");
  Arbre234 Noeud_Max = noeud_max(a);
  afficher_arbre(Noeud_Max,0);
  printf("\n");
  /* ======================================================== */

  /* ====================CLE PARCOURS LARGEUR=========================== */
  printf("\n");
  printf("Affichage des clés parcour en largeur: \n");
  Afficher_Cles_Largeur(a);
  printf("\n");
  /* =================================================================== */

  /* ======================CLE TRIER (R)========================= */
  printf("\n");
  printf("Affichage des clés triées récursivement: \n");
  Affichage_Cles_Triees_Recursive(a);
  printf("\n");
  /* ============================================================ */

  /* ======================CLE TRIER (NR)========================= */
  printf("\n");
  printf("Affichage des clés triées non récursivement: \n");
  Affichage_Cles_Triees_NonRecursive(a);
  printf("\n");
  /* ============================================================= */

  /* =======================DETRUIRE CLE========================== */
  printf("\n");
  printf("Destruction de la clé 10 : \n");
  Detruire_Cle(&a, 10);
  afficher_arbre(a, 0);
  printf("\n");
  /* ============================================================= */

  printf("TEST PASSED\n");
  return EXIT_SUCCESS;
}

/* ======================================================================= */
