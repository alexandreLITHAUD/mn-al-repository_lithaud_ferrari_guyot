#include <stdio.h>
#include <stdlib.h>
#include "file.h"

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

/**
 * @brief Fonction d'aide de l'utilisation du programme
 * 
 */
void helper(){
    printf("Programme de test file : \n");
    printf("\tc : creer_file\n");
    printf("\tt : detruire_file\n");
    printf("\tv : file_vide\n");
    printf("\tp : file_pleine\n");
    printf("\td : defiler\n");
    printf("\te : emfiler\n");
    printf("\ts : visualiser la file\n");
    printf("\th : afficher l\'aide\n");
    printf("\tq : quit\n");
}

void voirFile(pfile_t f){

    if(file_vide(f)){
        printf(YELLOW"FILE VIDE\n"NOCOLOR);
        return;
    }

    for(int i=0;i<MAX_FILE_SIZE;i++){

        printf("|");

        if(f->Tab[i] != NULL){
            printf(BLUE" %i "NOCOLOR,1);
        }
        else{
            printf(RED" NILL "NOCOLOR);
        }

        if(i == f->tete){
            printf(GREEN" tête "NOCOLOR);
        }

        if(i == f->queue){
            printf(GREEN" queue "NOCOLOR);
        }
    }

    printf("\n");
}

int main(){

    helper();

    char c;
    int exit = 1;

    pfile_t f = NULL;

    while(exit){

        printf(BLUE"Que voulez faire ?\n"NOCOLOR);
        scanf("%c",&c);


        switch (c)
        {
        case 'h':
            helper();
            break;

        case 'c':
            if(f != NULL){
                printf(YELLOW"La File est deja créé\n"NOCOLOR);
            }
            else{
                f = creer_file();
                printf(GREEN"La File a été créé avec succès\n"NOCOLOR);
            }
            break;

        case 't':
            if(f == NULL){
                printf(YELLOW"Il n'y a pas de File à détruire\n"NOCOLOR);
            }
            else{
                if(detruire_file(f)){
                    printf(RED"Echec de la détruction de la file\n"NOCOLOR);
                    return EXIT_FAILURE;
                }
                else{
                    f = NULL;
                    printf(GREEN"Destruction réussite\n"NOCOLOR);
                }
            }
            break;

        case 'v':
            if(f == NULL){
                printf(YELLOW"Il n'y a pas de File à regarder\n"NOCOLOR);
            }
            else{
                if(file_vide(f)){
                    printf(GREEN"La File est vide\n"NOCOLOR);
                }else{
                    printf(GREEN"La File n'est pas vide\n"NOCOLOR);
                }
            }
            break;
        
        case 'p':
            if(f == NULL){
                printf(YELLOW"Il n'y a pas de File à regarder\n"NOCOLOR);
            }
            else{
                if(file_pleine(f)){
                    printf(GREEN"La File est pleine\n"NOCOLOR);
                }else{
                    printf(GREEN"La File n'est pas pleine\n"NOCOLOR);
                }
            }
            break;

        case 'e':
            if(f == NULL){
                printf(YELLOW"Il n'y a pas de File à empiler\n"NOCOLOR);
                break;
            }
            if(file_pleine(f)){
                printf(YELLOW"On ne peux pas enfiler une File pleine\n"NOCOLOR);
                break;
            }
            else{
                printf(BLUE"Quelle est la valeur à ajouter ?\n"NOCOLOR);
                //int val=0;
                //scanf("%i",&val);
                pnoeud234 pn = malloc(sizeof(noeud234));
                //pn->cle = val;
                if(!enfiler(f,pn)){
                    printf(GREEN"Enfilation réussite\n"NOCOLOR);
                }
                else{
                    printf(RED"Echec d\'enfilation\n"NOCOLOR);
                    detruire_file(f);
                    return EXIT_FAILURE;
                }
            }
            break;

        case 'd':
            if(f == NULL){
                printf(YELLOW"Il n'y a pas de File à defiler\n"NOCOLOR);
                break;
            }
            if(file_vide(f)){
                printf(YELLOW"On ne peux pas défiler une File vide\n"NOCOLOR);
                break;
            }
            pnoeud234 test = defiler(f);
            if(test !=NULL ){
                printf(GREEN"Defilation réussite\n"NOCOLOR);
                free(test);
            }
            else{
                printf(RED"Echec de défilation\n"NOCOLOR);
                detruire_file(f);
                return EXIT_FAILURE;
            }
            break;

        case 's':
            voirFile(f);
            break;

        case 'q':
            exit = 0;
            if(f != NULL){
                detruire_file(f);
            }
            break;
        }
        fflush(0);
    }

    return EXIT_SUCCESS;

}