# Compte Rendu TP2 A234 : Lithaud Guyot Ferrari

- [Compte Rendu TP2 A234 : Lithaud Guyot Ferrari](#compte-rendu-tp2-a234--lithaud-guyot-ferrari)
- [Introduction](#introduction)
- [Fonctions](#fonctions)
- [Conclusion](#conclusion)

# Introduction

Lors de ce TP, nous avons pu coder une dizaine de fonctions principales à appliquer sur les arbres 234.
Les arbres 234 sont des ABR contenant des nœuds particuliers. Comme l’indique leur nom, ces derniers peuvent avoir 0, 2, 3 ou 4 fils. On remarque que, à part pour les noeuds ayant 0 fils, le nombre de clés d’un noeud correspond à son nombre de fils moins 1.
Dans ce compte-rendu, nous passerons brièvement en revue les différentes fonctions codées en précisant leur but et l’idée générale derrière leur implémentation.
Nous nous attarderons sur les éventuelles difficultés rencontrées.


# Fonctions


- ***int NombreCles(Arbre234 a)*** &#8594;
        Renvoie le nombre de clés de l’arbre passé en paramètres. Le comptage est réalisé récursivement sur la base de l’attribut t des noeuds234.
- ***int CleMax(Arbre234 a)*** &#8594;
    Renvoie la clé maximale de l’arbre passé en paramètre. Cette fonction à été réalisée itérativement et cherche le nœud le plus à droite car un arbre 234 est forcément trié.
- ***int CleMin(Arbre234 a)*** &#8594;
    Idem: Renvoie la clé minimale de l’arbre passé en paramètre. Cette fonction à été réalisée itérativement et cherche le nœud le plus à gauche car un arbre 234 est forcément trié.
        
- ***Arbre234 RechercherCle(Arbre234 a, int cle)*** &#8594;
        Renvoie le nœud portant la clé passée en paramètre s’il est présent dans l’arbre. Le recherche est réalisée récursivement.
    
- ***void AnalyseStructureArbre(Arbre234 a, int \*feuilles, int \*noeud2, int \*noeud3, int \*noeud4)*** &#8594;
Met dans des variables passées en paramètres le nombre de nœuds 0, 2, 3 et 4. Pour cela on parcourt tout l’arbre et on incrémente à chaque présence de nœud.

- ***Arbre234 noeud_max(Arbre234 a)*** &#8594;
Fonction qui détruit le nœud contenant la somme maximum de clé dans l’arbre passé en paramètre. Cette fonction à été écrite récursivement. Nous avons eu beaucoup de problèmes sur cette fonction. Nous avons dû créer une variable globale contenant un nœud afin de pouvoir restituer la réponse mais nous avons eu un certain nombre de problèmes d’initialisation de cette variable.

- ***void Afficher_Cles_Largeur(Arbre234 a)*** &#8594;
    Affiche les clés de l’arbre passé en paramètre avec un parcours en largeur. Le parcours est réalisé itérativement à l’aide d’une file.
- ***void Affichage_Cles_Triees_Recursive(Arbre234 a)*** &#8594;
    Affiche les clés de l’arbre passé en paramètre triées par ordre croissant. Le parcours est réalisé récursivement en priorisant les fils les plus à gauche puisqu’on utilise des ABR.
- ***void Affichage_Cles_Triees_NonRecursive(Arbre234 a)*** &#8594;
    Affiche les clés de l’arbre passé en paramètre triées par ordre croissant. Le parcours est réalisé itérativement, à l’aide d’une pile, en priorisant les fils les plus à gauche puisqu’on utilise des ABR.
**Difficulté:**    Les nœuds peuvent avoir plus de 2 fils ce qui complexifie grandement le parcours de l’arbre. En effet, lorsque l’on dépile un nœud parent, il est difficile de savoir sur quels fils a déjà été lancée la recherche.
Solution:    Utiliser une structure pour stocker le nombre de fois où un nœud parent a été consulté, on peut ainsi en déduire quels fils n’ont pas été visités.
- ***void Detruire_Cle(Arbre234 \*a, int cle)*** &#8594;
Cette fonction va détruire une clé passée en paramètre. Elle est extrêmement complexe et demande de gérer de nombreux cas. Nous nous sommes servies de nombreuses documentation ainsi que du pdf fournis listant les différents cas afin de la créer. Malgré cela, elle à demandé énormément de lignes de code. Il paraît évident que des optimisations existe pour une fonction semblant si élémentaire. 

# Conclusion

Le travail réalisé lors de ce TP nous a permis d’acquérir une connaissance approfondie en algorithmique des structures arborescentes, notamment sur les arbres non binaires.
En effet, manipuler des arbres n-aires soulève des contraintes diverses et peuvent complexifier l’implémentation de fonctions pourtant simples sur les arbres binaires.
À l’inverse, malgré son apparente complexité, la récursivité permet parfois de les simplifier amplement.




$$\int_{0}^{1} |ln(x)|dx = 1$$