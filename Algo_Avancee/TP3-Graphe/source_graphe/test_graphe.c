#include <stdio.h>
#include <stdlib.h>

#include "graphe.h"

int main (int argc, char **argv)
{
  pgraphe_t g ;
  int nc ;
  
  if (argc != 2)
    {
      fprintf (stderr, "erreur parametre \n") ;
      exit (-1) ;
    }

  /*
    la fonction lire_graphe alloue le graphe (matrice,...) 
    et lit les donnees du fichier passe en parametre
  */
  
  
  lire_graphe (argv [1], &g) ;

  /*
    la fonction ecrire_graphe affiche le graphe a l'ecran
  */  
  
  printf ("nombre de sommets du graphe %d nombre arcs %d \n", nombre_sommets (g), nombre_arcs (g)) ;
  fflush (stdout) ;
  
  ecrire_graphe (g) ;      

  nc = colorier_graphe (g) ;
  
  printf ("nombre chromatique graphe = %d\n", nc) ;

  ecrire_graphe_colorie (g) ;

  afficher_graphe_largeur(g,g->label);

  afficher_graphe_profondeur(g,g->label);

  printf("DIJKSTRA :\n");
  algo_dijkstra(g,g->label);
  ecrire_graphe_dijkstra(g);

  psommet_t p1 = g;
  printf("========================\n");
  while(p1 != NULL){
    int degsortant = degre_sortant_sommet(g, chercher_sommet(g, p1->label));
    int degentrant = degre_entrant_sommet(g, chercher_sommet(g, p1->label));
    printf("Degré sortant du sommet %d : %d\n", p1->label,degsortant);
    printf("Degré entrant du sommet %d : %d\n", p1->label,degentrant);
    p1 = p1->sommet_suivant;
  }

  int degmax = degre_maximal_graphe(g);
  printf("========================\n");
  printf("Degré maximal du graphe : %d\n", degmax);

  printf("========================\n");
  int min = degre_minimal_graphe(g);
  printf("Le degré minimal du graphe est: %d\n", min);

  printf("========================\n");
  independant(g) ? printf("Le graphe est indepandant\n") : printf("Le graphe n'est pas indepandant\n");

  printf("========================\n");
  complet(g) ? printf("Le graphe est complet\n") : printf("Le graphe n'est pas complet\n");

  printf("========================\n");
  regulier(g) ? printf("Le graphe est régulier\n") : printf("Le graphe n'est pas régulier\n");

  psommet_t s = chercher_sommet(g,g->label);
  pchemin_t c = (pchemin_t) malloc(sizeof(chemin_t));
  c->depart = s; 
  c->parcours = s->liste_arcs;

  pchemin_t c2 = (pchemin_t) malloc(sizeof(chemin_t));
  c2->depart = s;
  c2->parcours = s->liste_arcs->arc_suivant;

  printf("========================\n");
  elementaire(g,c) ? printf("Le chemin est élémentaire\n") : printf("Le chemin n'est pas élémentaire\n");

  printf("========================\n");
  simple(g,c) ? printf("Le chemin est simple\n") : printf("Le chemin n'est pas simple\n");

  printf("========================\n");
  elementaire(g,c2) ? printf("Le chemin est élémentaire\n") : printf("Le chemin n'est pas élémentaire\n");

  printf("========================\n");
  simple(g,c2) ? printf("Le chemin est simple\n") : printf("Le chemin n'est pas simple\n");

  printf("========================\n");
  eulerien(g,c) ? printf("Le chemin est eulerien\n") : printf("Le chemin n'est pas eulerien\n");
  
  printf("========================\n");
  hamiltonien(g,c) ? printf("Le chemin est hamiltonien\n") : printf("Le chemin n'est pas hamiltonien\n");

  printf("========================\n");
  eulerien(g,c2) ? printf("Le chemin est eulerien\n") : printf("Le chemin n'est pas eulerien\n");
  
  printf("========================\n");
  hamiltonien(g,c2) ? printf("Le chemin est hamiltonien\n") : printf("Le chemin n'est pas hamiltonien\n");

  printf("========================\n");
  psommet_t g1 = g;
  psommet_t g2 = g;

  while(g1 != NULL){

    while(g2 != NULL){
      printf("distance entre %d et %d est : %d \n",g1->label,g2->label,distance(g,g1->label,g2->label));
      g2 = g2->sommet_suivant;
    }

    g1 = g1->sommet_suivant;
  }

   printf("le diametre de g est : %d\n",diametre(g));

  printf("========================\n");
  graphe_eulerien(g) ? printf("Le graphe est eulerien\n") : printf("Le graphe n'est pas eulerien\n");

  printf("========================\n");
  graphe_hamiltonien(g) ? printf("Le graphe est hamiltonien\n") : printf("Le graphe n'est pas hamiltonien\n");

}
