#include <stdlib.h>
#include <limits.h>

#include "graphe.h"
#include "file.h"

int nbVal = 0;

pfile_t creer_file()
{
  pfile_t f = malloc(sizeof(file_t));
  f->tete = 0;
  f->queue = 0;
  return f;
}

int detruire_file(pfile_t f)
{
  while (!file_vide(f))
  {
    psommet_t test = defiler(f);
    free(test);
  }
  free(f);
  return EXIT_SUCCESS;
}

int file_vide(pfile_t f)
{
  return (f->queue == f->tete && nbVal == 0);
}

int file_pleine(pfile_t f)
{
  return (f->queue == f->tete && nbVal == MAX_FILE_SIZE);
}

psommet_t defiler(pfile_t f)
{
  if(!file_vide(f)){

    psommet_t res = f->Tab[f->tete];
    f->Tab[f->tete] = NULL;
    f->tete = (f->tete+1) % MAX_FILE_SIZE;
    nbVal--;
    return res;
  }
  return NULL;
}

int enfiler(pfile_t f, psommet_t p)
{

  if(!file_pleine(f)){
    f->Tab[(f->queue) % MAX_FILE_SIZE] = p;
    f->queue = (f->queue+1) % MAX_FILE_SIZE;
    nbVal++;
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}

psommet_t enlever_min(pfile_t f)
{

  if(file_vide(f)){
    return NULL;
  }

  int min = INT_MAX;
  int index = 0;
  parc_t minA;

  for(int i =0;i<nbVal;i++){

    
    psommet_t p = f->Tab[f->tete + i % MAX_FILE_SIZE];

    parc_t l = p->liste_arcs;

    while(l != NULL){

      if(l->poids < min && l->dest->isVisited==0){
        min = l->poids;
        index = i;
        minA = l;
      }

      l = l->arc_suivant;
    }

  }

  // IL FAUT L'NELEVER SANS CASSE LA FILE

  f->Tab[f->queue + index % MAX_FILE_SIZE] = NULL;

  if(index == 0){
    f->tete = (f->tete+1) % MAX_FILE_SIZE;
  }

  else if(index == nbVal){
    f->queue = (f->queue-1) % MAX_FILE_SIZE;
  }

  else{

    //int temp = index;
    for(int i =0;i<index - f->queue % MAX_FILE_SIZE;i++){
      f->Tab[index + i %MAX_FILE_SIZE] = f->Tab[index + i + 1 %MAX_FILE_SIZE];
    }

    f->queue = (f->queue-1) % MAX_FILE_SIZE;
  }

  nbVal--;
  
  // SI index = 0 alors il faut juste bouger la queud ++
  // SI index = nbVal il faut juste bouger la tête --
  
  // SINON TOUT DECALLER;

  // NBVAL --:
  //minA->isVisited = 1;
  return minA->dest;

}
