/*
  Structures de type graphe
  Structures de donnees de type liste
  (Pas de contrainte sur le nombre de noeuds des  graphes)
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "graphe.h"
#include "file.h"
#include "pile.h"
#include "fap.h"

psommet_t chercher_sommet(pgraphe_t g, int label)
{
  psommet_t s;

  s = g;

  while ((s != NULL) && (s->label != label))
  {
    s = s->sommet_suivant;
  }
  return s;
}

parc_t existence_arc(parc_t l, psommet_t s)
{
  parc_t p = l;

  while (p != NULL)
  {
    if (p->dest == s)
      return p;
    p = p->arc_suivant;
  }
  return p;
}

void ajouter_arc(psommet_t o, psommet_t d, int distance)
{
  parc_t parc;

  parc = (parc_t)malloc(sizeof(arc_t));

  if (existence_arc(o->liste_arcs, d) != NULL)
  {
    fprintf(stderr, "ajout d'un arc deja existant\n");
    exit(-1);
  }

  parc->poids = distance;
  parc->dest = d;
  parc->arc_suivant = o->liste_arcs;
  o->liste_arcs = parc;
  return;
}

// ===================================================================

int nombre_sommets(pgraphe_t g)
{
  psommet_t p = g;
  int nb = 0;

  while (p != NULL)
  {
    nb = nb + 1;
    p = p->sommet_suivant;
  }

  return nb;
}

int nombre_arcs(pgraphe_t g)
{

  psommet_t p = g;
  int nb_arcs = 0;

  while (p != NULL)
  {
    parc_t l = p->liste_arcs;

    while (l != NULL)
    {
      nb_arcs = nb_arcs + 1;
      l = l->arc_suivant;
    }

    p = p->sommet_suivant;
  }
  return nb_arcs;
}

void init_couleur_sommet(pgraphe_t g)
{
  psommet_t p = g;

  while (p != NULL)
  {
    p->couleur = 0;        // couleur indefinie
    p = p->sommet_suivant; // passer au sommet suivant dans le graphe
  }

  return;
}

void init_isVisited_sommet(pgraphe_t g)
{
  psommet_t p = g;

  while (p != NULL)
  {
    p->isVisited = 0;        // non visited
    p = p->sommet_suivant; // passer au sommet suivant dans le graphe
  }

  return;
}

void init_isVisited_arc(pgraphe_t g)
{
  psommet_t s = g;
  parc_t a = s->liste_arcs;
  while (s->sommet_suivant != NULL){
    while (a != NULL){
      a->isVisited = 0;        // non visited
      a = a->arc_suivant; // passer au sommet suivant dans le graphe
    }
    s = s->sommet_suivant;
  }
  return;
}

int colorier_graphe(pgraphe_t g)
{
  /*
    coloriage du graphe g

    datasets
    graphe data/gr_planning
    graphe data/gr_sched1
    graphe data/gr_sched2
  */

  psommet_t p = g;
  parc_t a;
  int couleur;
  int max_couleur = INT_MIN; // -INFINI

  int change;

  init_couleur_sommet(g);

  while (p != NULL)
  {
    couleur = 1; // 1 est la premiere couleur

    // Pour chaque sommet, on essaie de lui affecter la plus petite couleur

    // Choix de la couleur pour le sommet p

    do
    {
      a = p->liste_arcs;
      change = 0;

      while (a != NULL)
      {
        if (a->dest->couleur == couleur)
        {
          couleur = couleur + 1;
          change = 1;
        }
        a = a->arc_suivant;
      }

    } while (change == 1);

    // couleur du sommet est differente des couleurs de tous les voisins

    p->couleur = couleur;
    if (couleur > max_couleur)
      max_couleur = couleur;

    p = p->sommet_suivant;
  }

  return max_couleur;
}

/**
 * It takes a graph and a root node, and prints out the nodes in the graph in a breadth-first traversal
 * 
 * @param g the graph
 * @param r the root node
 * 
 * @return the number of vertices in the graph.
 */
void afficher_graphe_largeur(pgraphe_t g, int r)
{
  /*
    afficher les sommets du graphe avec un parcours en largeur
  */
  printf("PARCOUR EN LARGEUR :\n");

  init_isVisited_sommet(g);

  psommet_t root = chercher_sommet(g,r);
  if(root == NULL) { return; }

  pfile_t f = creer_file();
  enfiler(f, root);

  while (!file_vide(f))
  {

    pgraphe_t temp = defiler(f);
    temp->isVisited = 1;

    printf("| %i | ", temp->label);

    parc_t placeHolder = temp->liste_arcs;
    while (placeHolder != NULL)
    {

      if (placeHolder->dest->isVisited == 0)
      {
        placeHolder->dest->isVisited = 1;
        enfiler(f, placeHolder->dest);
      }
      placeHolder = placeHolder->arc_suivant;
    }
  }

  printf("\n");
  return;
}

/**
 * It prints the vertices of the graph in a depth-first order
 * 
 * @param g the graph
 * @param r the root node
 * 
 * @return the vertices in the graph.
 */
void afficher_graphe_profondeur(pgraphe_t g, int r)
{
  /*
    afficher les sommets du graphe avec un parcours en profondeur
  */
  printf("PARCOUR EN PROFONDEUR :\n");

  init_isVisited_sommet(g);

  psommet_t root = chercher_sommet(g,r);

  if(root == NULL) { return; }

  ppile_t p = creer_pile();
  empiler(p, root);

  while (!pile_vide(p))
  {

    pgraphe_t temp = depiler(p);
    temp->isVisited = 1;

    printf("| %i | ", temp->label);

    parc_t placeHolder = temp->liste_arcs;
    while (placeHolder != NULL)
    {

      if (placeHolder->dest->isVisited == 0)
      {
        placeHolder->dest->isVisited = 1;
        empiler(p, placeHolder->dest);
      }
      placeHolder = placeHolder->arc_suivant;
    }
  }

  printf("\n");
  return;
}

/**
 * It initializes the arcs of the graph
 * 
 * @param g the graph
 */
void init_arc(pgraphe_t g)
{

  psommet_t p = g;

  while (p != NULL)
  {
    parc_t l = p->liste_arcs;

    while (l != NULL)
    {
      l->isVisited = 0;
      l = l->arc_suivant;
    }

    p = p->sommet_suivant;
  }
}

/**
 * It initializes the distance of each vertex to the maximum integer value
 * 
 * @param g the graph
 * 
 * @return Nothing.
 */
void init_dist_dijkstra(pgraphe_t g)
{
   psommet_t p = g;

  while (p != NULL)
  {
    p->dist = INT_MAX;
    p = p->sommet_suivant; // passer au sommet suivant dans le graphe
  }

  return; 
}


/**
 * It's a Dijkstra's algorithm implementation
 * 
 * @param g the graph
 * @param r the root node
 */
void algo_dijkstra(pgraphe_t g, int r)
{
  init_isVisited_sommet(g);
  init_dist_dijkstra(g);

  // PREDECESSEUR POTENTIELLEMENT
  psommet_t root = chercher_sommet(g,r);
  if(root == NULL){ return; }
  
  root->dist = 0;

  fap f = creer_fap_vide();
  
  root->isVisited = 1;
  f = inserer(f, root,0);
  
  while(!est_fap_vide(f)){ 

    psommet_t temp;
    int prio;
    f = extraire(f,&temp,&prio);

    temp->isVisited = 1;

    parc_t placeHolder = temp->liste_arcs;

    while (placeHolder != NULL)
    {

      if(temp->dist + placeHolder->poids < placeHolder->dest->dist && placeHolder->dest->isVisited == 0){
        placeHolder->dest->dist = temp->dist + placeHolder->poids;
        f = inserer(f,placeHolder->dest,placeHolder->dest->dist);
      }

      placeHolder = placeHolder->arc_suivant;
    }

  }

  detruire_fap(f);

  /*
    algorithme de dijkstra
    des variables ou des chanmps doivent etre ajoutees dans les structures.
  */

  return;
}

// ======================================================================

/**
 * It returns the number of arcs leaving a given vertex
 * 
 * @param g the graph
 * @param s the vertex we want to know the degree of
 * 
 * @return The number of outgoing arcs from the vertex n in the graph g.
 */
int degre_sortant_sommet(pgraphe_t g, psommet_t s)
{
  /*
    Cette fonction retourne le nombre d'arcs sortants
    du sommet n dans le graphe g
  */ 
  parc_t a = s->liste_arcs;
  int compteur = 0;
  
  while (a!= NULL)    
  {
    a = a->arc_suivant;   // Parcours la liste d'arcs sortant du sommet
    compteur++;
  }

  return compteur;
}

/**
 * It returns the number of arcs entering the node n in the graph g
 * 
 * @param g the graph
 * @param s the node we want to know the number of incoming arcs
 * 
 * @return The number of arcs entering the node n in the graph g
 */
int degre_entrant_sommet(pgraphe_t g, psommet_t s)
{
  /*
    Cette fonction retourne le nombre d'arcs entrants
    dans le noeud n dans le graphe g
  */
  int count = 0;

  psommet_t temp = g;

  while (temp!= NULL){
    parc_t test = existence_arc(temp->liste_arcs, s);   // Test si un arc arrive au sommet s
    if (test != NULL){
      count++;
    }
    temp = temp->sommet_suivant;    
  }

  return count;
}

/**
 * It computes the maximum degree of a graph
 * 
 * @param g the graph
 * 
 * @return The maximum degree of the graph.
 */
int degre_maximal_graphe(pgraphe_t g)
{
  /*
    Max des degres des sommets du graphe g
  */

  psommet_t s = g;

  int degmax = degre_sortant_sommet(g, s) + degre_entrant_sommet(g, s);
  int temp;

  while (s->sommet_suivant != NULL)   // On parcourt tous les sommets du graphe
  {
    s = s->sommet_suivant;
    temp = degre_sortant_sommet(g, s) + degre_entrant_sommet(g, s);   // On somme les arcs sortants et entrants de chaque sommet
    if (temp > degmax)
    {
      degmax = temp;      // On conserve la max
    }
  }

  return degmax;
}

/**
 * It computes the minimal degree of a graph
 * 
 * @param g the graph
 * 
 * @return The minimal degree of the graph.
 */
int degre_minimal_graphe(pgraphe_t g)
{
  /*
    Min des degres des sommets du graphe g
  */
  psommet_t temp = g;

  int min = degre_entrant_sommet(g, temp) + degre_sortant_sommet(g, temp);
  temp = temp->sommet_suivant;


  while (temp != NULL){
    int deg = degre_entrant_sommet(g, temp) + degre_sortant_sommet(g, temp);   
    
    if (deg < min){
      min = deg;
    }

    temp = temp->sommet_suivant;      // On conserve le min
  }

  return min;
}

/**
 * It returns 1 if the graph is independant, 0 otherwise
 * 
 * @param g the graph
 * 
 * @return 0 or 1
 */
int independant(pgraphe_t g)
{
  /* Les aretes du graphe n'ont pas de sommet en commun */

  psommet_t temp = g;

  while (temp != NULL) {
    int test = degre_entrant_sommet(g, temp) + degre_sortant_sommet(g, temp);   
    if (test > 1){    // On regarde si le degré max de chaque sommet est inférieur ou égal à 1 
      return 0;
    }
  }

  return 1;
}

/**
 * It returns 1 if the graph is complete, 0 otherwise
 * 
 * @param g the graph
 * 
 * @return 0 or 1
 */
int complet(pgraphe_t g)
{
  /* Toutes les paires de sommet du graphe sont jointes par un arc */

  psommet_t ps = g;

  while (ps != NULL){

    if(degre_sortant_sommet(g,ps) != nombre_sommets(g)-1){      // On regarde si le degré sortant de chaque sommet est égal au nombre total de sommets du graphe excepté celui que l'on traite
      return 0;
    }
  }

  return 1;
  
}

/**
 * It returns 1 if the graph is regular, 0 otherwise
 * 
 * @param g the pointer to the first vertex of the graph
 * 
 * @return 1 if the graph is regular, 0 otherwise.
 */
int regulier(pgraphe_t g)
{
  /*
     graphe regulier: tous les sommets ont le meme degre
     g est le pointeur vers le premier sommet du graphe
     renvoie 1 si le graphe est régulier, 0 sinon
  */
  
  psommet_t s = g;
  int temp1 = degre_entrant_sommet(g,s) + degre_sortant_sommet(g,s);
  int temp2 = temp1;
  while(s->sommet_suivant != NULL){                   
    s = s->sommet_suivant;
    temp2 = degre_entrant_sommet(g,s) + degre_sortant_sommet(g,s);
    if(temp2 != temp1){       // On voit si un des sommets à un degré différent à n'importe quel autre sommet
      return 0;
    }
  }
  return 1;
} 

/*
  placer les fonctions de l'examen 2017 juste apres
*/

/**
 * It checks if a path is elementary by checking if it passes twice through the same vertex
 * 
 * @param g the graph
 * @param c the path to check
 */
int elementaire (pgraphe_t g, pchemin_t c)
{
  /* 
      chemin élémentaire : un chemin ne passant pas deux fois 
      par un même sommet
      renvoie 1 si le chemin est élémentaire, 0 sinon
  */
  init_isVisited_sommet(g);       
  psommet_t s = c->depart;
  s->isVisited = 1;
  parc_t a;

  a = c->parcours;
  while(a != NULL){   

    s = a->dest;              // On parcourt les sommets du chemin
    if(s->isVisited == 1){    // On regarde si un sommet a déjà été visité
      return 0;   
    }
    s->isVisited = 1;         // On marque un sommet comme visité
    a = a->arc_suivant;       // On continue le chemin
  }
  
  return 1;
  
}

/**
 * It checks if a path is simple
 * 
 * @param g the graph
 * @param c the path we're checking
 */
int simple (pgraphe_t g, pchemin_t c)
{
  /* 
      chemin simple : un chemin ne passant pas deux fois 
      par un même arc
      renvoie 1 si le chemin est simple, 0 sinon
  */
  init_isVisited_arc(g);

  psommet_t s = c->depart;
  parc_t a;

  a = c->parcours;
  while(a != NULL){   

    if(a->isVisited == 1){    // On regarde si un arc a déjà été visité
      return 0;
    }
    a->isVisited = 1;         // On marque un arc comme visité
      a = a->arc_suivant;     // On continue le chemin
    }
  
  return 1;
}

/**
 * It checks if the path is eulerian by checking if all the arcs have been visited
 * 
 * @param g the graph
 * @param c the path
 */
int eulerien (pgraphe_t g , pchemin_t c)
{
  /*
      chemin eulerien : un chemin utilisant 
      tous les arcs du graphe
      renvoie 1 si le chemin est eulerien, 0 sinon
  */
  init_isVisited_arc(g);

  psommet_t s = c->depart;
  parc_t a;
  int compteur = 0;
  a = c->parcours;    
  while(a != NULL){

    if(a->isVisited == 0){      // On compte les arcs nouveaux sur le chemin
      compteur ++;
      a->isVisited = 1;
    }
    a = a ->arc_suivant;
  }
  if(compteur >= nombre_arcs(g)){   // On compare si on a parcouru au moins autant d'arc différents que le graphe n'en possède au total
    return 1;
  }
  return 0;
}

/**
 * It checks if a path is hamiltonian by checking if it visits all the vertices of the graph
 * 
 * @param g the graph
 * @param c the path we're checking
 */
int hamiltonien (pgraphe_t g , pchemin_t c)
{
  /*
      chemin hamiltonien : un chemin passant par 
      tous les sommets du graphe
      renvoie 1 si le chemin est hamiltonien, 0 sinon
  */
  init_isVisited_sommet(g);
  psommet_t s = c->depart;
  s->isVisited = 1;
  parc_t a;
  int compteur = 0;
  a = c->parcours;
  while(a != NULL){
    
    s = a->dest;
    if(s->isVisited == 0){        // On compte les sommets nouveaux sur le chemin      
      compteur ++;
      s->isVisited = 1;
    }
    a = a->arc_suivant;
  }
  if(compteur >= nombre_sommets(g)){    // On compare si on a parcouru au moins autant d'arc différents que le graphe n'en possède au total
    return 1;
  }
  return 0;
}

/**
 * It checks if the graph is eulerian by checking if all nodes have been visited, if there are more
 * than 2 nodes with odd degrees, and if there are more than 2 nodes with odd degrees
 * 
 * @param g the graph
 * 
 * @return 1 if the graph is eulerian, 0 if it is not.
 */
int graphe_eulerien (pgraphe_t g)
{

  // SEARCH IF A GRAPH IS CONNEXE
  psommet_t p = g;
  afficher_graphe_largeur(g,1);

  while(p != NULL){
    if(p->isVisited == 0){
      return 0;
    }
    p = p->sommet_suivant;
  }

  psommet_t p2 = g;
  int oddDeg = 0;

  while(p2 != NULL){
    if(degre_entrant_sommet(g,p2) + degre_sortant_sommet(g,p2) % 2 == 1){   // On regarde si le graphe a au plus deux sommets impairs
      oddDeg++;
    }
    if(oddDeg > 2){
      return 0;
    }
    p2 = p2->sommet_suivant;
  }

  if(oddDeg > 2){
    return 0;
  }

  return 1;
}

/**
 * If the graph has less than 3 vertices, it's not Hamiltonian, otherwise, if any vertex has a degree
 * less than the number of vertices, it's not Hamiltonian, otherwise it is
 * 
 * @param g the graph
 */
int graphe_hamiltonien (pgraphe_t g)
{

  // Theorème de DIRAC

  if(nombre_sommets(g) < 3 ){
    return 0;
  }

  psommet_t p = g;
  int nbSommet = nombre_sommets(g);

  while (p != NULL)
  {
    if(degre_entrant_sommet(g,p) + degre_sortant_sommet(g,p) < nbSommet%2){
      return 0;
    }

    p = p->sommet_suivant;
  }
  
  // Il manque surement de nombreux autre cas a gérer

  return 1;
}

/**
 * It computes the distance between two vertices in a graph
 * 
 * @param g the graph
 * @param x the source node
 * @param y the destination node
 * 
 * @return The distance between the two vertices.
 */
int distance (pgraphe_t g, int x, int y)
{
  /*
      distance : la longueur du plus court chemin
      entre les sommets x et y
      renvoie la distance entre x et y
  */// On compare si on a parcouru au moins autant d'arc différents que le graphe n'en possède au total
  psommet_t p = chercher_sommet(g,y);
  
  if(p == NULL){
    return INT_MAX;
  }

  algo_dijkstra(g,x);     // L'algo de dijkstra met à jour les plus courts chemin par rapport au sommet x

  return p->dist;
}

/**
 * It returns the maximum distance between a given vertex and all other vertices in the graph
 * 
 * @param g the graph
 * @param n the node we want to find the eccentricity of
 * 
 * @return The maximum distance between the vertex n and all the other vertices in the graph.
 */
int excentricite (pgraphe_t g, int n)
{
  /*
      excentricite : la distance maximale d'un sommet
      avec les autres sommets du graphe
      renvoie l'excentricite du sommet n
  */

  psommet_t p = g;
  int max = 0;

  while (p != NULL)
  {
    int distan = distance(g,n,p->label);

    if(distan > max){
      max = distan;        // On garde la distance max entre n et chaque sommet autre du graphe
    }

    p = p->sommet_suivant; // passer au sommet suivant dans le graphe
  }

  return max; 
}

/**
 * > The diameter of a graph is the maximum distance between any two vertices
 * 
 * @param g the graph
 * 
 * @return The diameter of the graph.
 */
int diametre (pgraphe_t g)
{
  /*
      diametre : l'excentricite maximale 
      entre tous les sommets du graphe
      renvoie le diametre du graphe g
  */

  psommet_t p = g;
  int max = 0;

  while (p != NULL)
  {
    int distan = excentricite(g,p->label);

    if(distan > max){
      max = distan;         // On garde l'excentricité max entre chaque sommet du graphe
    }

    p = p->sommet_suivant; // passer au sommet suivant dans le graphe
  }

  return max; 

}