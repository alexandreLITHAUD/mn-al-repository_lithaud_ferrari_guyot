# Compte rendu TP3 Graphe : Lithaud Guyot Ferrari

- [Compte rendu TP3 Graphe : Lithaud Guyot Ferrari](#compte-rendu-tp3-graphe--lithaud-guyot-ferrari)
  - [Graphes : Algorithmes de base](#graphes--algorithmes-de-base)
  - [Propriétés sur les graphes](#propriétés-sur-les-graphes)
  - [Conclusion](#conclusion)

## Graphes : Algorithmes de base


- **void afficher_graphe_profondeur (pgraphe_t g, int r) ;**
- **void afficher_graphe_largeur (pgraphe_t g, int r) ;**
- **void algo_dijkstra (pgraphe_t g, int r) ;**

Ces algorithmes sont très similaires dans leur fonctionnement, le principal changement réside dans les structures de données utilisées. Le parcours en profondeur va utiliser une pile. Le parcours en largeur lui va utiliser une file. Dijkstra lui va utiliser une file a priorité (fap) afin de pouvoir récupérer la distance minimale en les sommets. 

Dijkstra va utiliser l'argument dist des sommets afin de sauvegarder leur distance.

Enfin, chacun de ces algorithmes va utiliser l'argument isVisited des sommets afin de pouvoir savoir si un sommet a déjà été parcouru.


- **int degre_sortant_sommet(pgraphe_t g, psommet_t s) ;**

Retourne le nombre d’arcs sortant du sommet n dans le graphe g. Fonction réalisée en parcourant la liste chaînée des arcs sortant du sommet s.

- **int degre_entrant_sommet(pgraphe_t g, psommet_t s) ;**
    
Retourne le nombre d’arcs sortant du sommet n dans le graphe g. Fonction réalisée en parcourant la liste des sommets du graphe et en testant l'existence d’un arc vers le sommet s pour chacun d’eux.
    
- **int degre_maximal_graphe(pgraphe_t g) ;**

Retourne le degré maximal du graphe g. Fonction réalisée en additionnant les degrés entrants et sortants pour chaque sommet du graphe.

- **int degre_minimal_graphe(pgraphe_t g) ;**

Retourne le degré maximal du graphe g. Idem.

- **int independant(pgraphe_t g) ;**

Renvoie 1 si le graphe est indépendant. Pour cela, on observe le degré de chaque sommet: s’il est supérieur à 1, alors deux arcs partent du même sommet et le graphe n’est donc pas indépendant.

- **int complet(pgraphe_t g) ;**

Renvoie 1 si le graphe est complet. Pour cela, on observe le degré sortant de chaque sommet: s’il est différent du nombre total de sommets - 1, alors il manque des arcs sortants et le graphe n’est pas complet.

- **int regulier(pgraphe_t g) ;**
  
Renvoie 1 si le graphe est régulier. Fonction réalisée en récupérant le degré du premier sommet du graphe et en le comparant à tous les autres. Si deux sommets comparés n’ont pas le même degré, le graphe n’est pas régulier.


## Propriétés sur les graphes

- **int elementaire (pgraphe_t g, pchemin_t c);**

Renvoie 1 si le chemin est élémentaire. Fonction réalisée en parcourant les arcs et en marquant les sommets du chemin passé en paramètre. Si on parcourt au moins deux fois le même sommet, le chemin n’est pas élémentaire.

- **int simple (pgraphe_t g, pchemin_t c);**

Renvoie 1 si le chemin est simple. Fonction réalisée en parcourant et en marquant les arcs du chemin passé en paramètre. Si on parcourt au moins deux fois le même arc, le chemin n’est pas élémentaire.

- **int eulerien (pgraphe_t g , pchemin_t c);**

Renvoie 1 si le chemin est eulérien. Fonction réalisée en parcourant et en marquant les arcs du chemin passé en paramètre. On compte chaque arc traversé pour la première fois. Si le compteur n’atteint pas le nombre d’arcs du graphe, ce dernier n’est pas eulérien.

- **int hamiltonien (pgraphe_t g , pchemin_t c);**

Renvoie 1 si le chemin est hamiltonien. Fonction réalisée en parcourant et en marquant les sommets du chemin passé en paramètre. On compte chaque sommet traversé pour la première fois. Si le compteur n’atteint pas le nombre de sommets du graphe, ce dernier n’est pas hamiltonien.


- **int graphe_eulerien (pgraphe_t g);**
- **int graphe_hamiltonien (pgraphe_t g);**

Pour faire la fonction graphe eulérien nous avons utilisé une propriété mathématique qui permet de trouver le résultat. La propriété est la suivante : Si le graphe est connexe et que le graphe possède au plus 2 sommets avec des degrés impair alors le graphe est eulérien. Cela nous a donc permis de réaliser cette fonction sans accros.

Pour faire la fonction graphe hamiltonien nous avons utilisé le théorème de Dirac qui nous dit qu’un graphe est hamiltonien si le graphe possède au moins 3 sommets et que le degré de chaque sommet ne soit pas supérieur au nombre de sommet divisé par deux. Cette solution n’est pas parfaite et ne gère pas tous les cas à ma connaissance mais gère une bonne partie des graphes classique.

- **int distance (pgraphe_t g, int x, int y);**
- **int excentricite (pgraphe_t g, int n);**
- **int diametre (pgraphe_t g);**

Pour la fonction distance, nous avons utilisé l'algorithme de Dijkstra implémenté précédemment. Comme celui-ci nous indique la distance minimum entre une racine r et les autres sommet du graphe nous avions juste besoin de modifier la racine et de récupérer la distance du sommet pour avoir le bon résultat.

La fonction excentricite va utiliser la fonction distance afin d’avoir un résultat convenable. Et diamètre va utiliser la fonction excentricite. Le code est loin d'être le plus optimal mais il permet de facilement retourner une valeur correcte.


## Conclusion

Le travail réalisé lors de ce TP nous a permis de mettre en application notre connaissance des graphes, une structure de donnée essentielle dans de nombreux domaines de l’informatique (Réseaux, automate, Machine Learning, etc.). L’orientation des arcs a ajouté un degré de complexité nécessitant l'adaptation des structures de données utilisées. L’analyse de la nature d’un graphe (eulérien ou hamiltonien) nous a également confrontés à des problèmes NP-Complet qui ont été partiellement résolus par l'utilisation de théorèmes mathématiques et de la théorie des graphes.
