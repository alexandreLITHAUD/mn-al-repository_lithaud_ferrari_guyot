#include <stdio.h>
#include <stdlib.h>

#include "poly_creux.h"

// Programme de test des polynomes creux
int main(int argc, char **argv)
{

    p_polyf_creux_t p1;
    p_polyf_creux_t p2;
    p_polyf_creux_t p3;
    p_polyf_creux_t p4;
    p_polyf_creux_t p5;
    p_polyf_creux_t p6;
    //p_polyf_creux_t p7;

    // Si il n'y pas assez de paramètres
    if (argc < 3)
    {
        fprintf(stderr, "Missing parameters\n");
        fprintf(stderr, "Two polynomes creux file needed\n");
        exit(-1);
    }

    // On lit les différents fichiers polynomes
    p1 = lire_polynome_float(argv[1]);
    p2 = lire_polynome_float(argv[2]);

    // On écrit les polynomes lus
    ecrire_polynome_float(p1);
    ecrire_polynome_float(p2);

    /* =============TEST EGALITE============= */
    printf("=====================================\n");
    printf("Egalité entre p1 et p1 : %s\n", egalite_polynome(p1, p1) == 1 ? "true" : "false");
    printf("=====================================\n\n");

    /* =============TEST EGALITE============= */
    printf("=====================================\n");
    printf("Egalité entre p1 et p2 : %s\n", egalite_polynome(p1, p2) == 1 ? "true" : "false");
    printf("=====================================\n\n");

    /* =============TEST ADDITION============= */
    printf("=====================================\n");
    printf("Addition de p1 et de p2 = \n");
    p3 = addition_polynome(p1, p2);
    ecrire_polynome_float(p3);
    printf("=====================================\n\n");

    /* =============TEST EVAL============= */
    printf("=====================================\n");
    printf("eval de p1 avec X = 3\n p1 = %f\n", eval_polynome(p1, 3));
    printf("=====================================\n\n");

    /* =============TEST MULTIPLICATION SCALAIRE============= */
    printf("=====================================\n");
    printf("Multiplication de p1 et le scalaire 3 = \n");
    p4 = multiplication_polynome_scalaire(p1, 3);
    ecrire_polynome_float(p4);
    printf("=====================================\n\n");

    /* =============TEST MULTIPLICATION============= */
    printf("=====================================\n");
    printf("Multiplication de p1 et de p2 = \n");
    p5 = multiplication_polynomes(p1, p2);
    ecrire_polynome_float(p5);
    printf("=====================================\n\n");

    /* =============TEST PUISSANCE============= */
    printf("=====================================\n");
    printf("Puissance de p1 par 2 = \n");
    p6 = puissance_polynome(p1,2);
    ecrire_polynome_float(p6);
    printf("=====================================\n\n");

    /* =============TEST COMPOSITION============= */
    // NON FONCTIONEL
    // printf("=====================================\n");
    // printf("Calcul de p1 O p2 = \n");
    // p7 = composition_polynome(p1,p2);
    // ecrire_polynome_float(p7);
    // printf("=====================================\n\n");

    detruire_polynome(p1);
    detruire_polynome(p2);
    detruire_polynome(p3);
    detruire_polynome(p4);
    detruire_polynome(p5);
    detruire_polynome(p6);
    //detruire_polynome(p7);

    return EXIT_SUCCESS;
}