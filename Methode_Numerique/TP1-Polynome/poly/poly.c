#include <stdio.h>
#include <stdlib.h>

#include "poly.h"

#include <x86intrin.h>

/* ==================================== */
/**
 * @brief Fonction qui créer un polynome
 * 
 * @param nbCoeff le nombre de coefficient du polynome
 * @return le polynome créer
 */
p_polyf_t creer_polynome(int degre)
{
  p_polyf_t p;

  p = (p_polyf_t)malloc(sizeof(polyf_t));
  p->degre = degre;

  p->coeff = (float *)malloc((degre + 1) * sizeof(float));

  return p;
}

/* ==================================== */
/**
 * @brief Fonction qui détruit en mémoire le polynome d'entré
 * 
 * @param p le polynome à détruire
 */
void detruire_polynome(p_polyf_t p)
{
  free(p->coeff);
  free(p);

  return;
}

/* ==================================== */
/**
 * @brief Fonction qui initialise un polynome
 * 
 * @param p la polynome a initialiser
 * @param x la valeur a assigné
 */
void init_polynome(p_polyf_t p, float x)
{
  register unsigned int i;

  for (i = 0; i <= p->degre; ++i)
    p->coeff[i] = x;

  return;
}

/* ==================================== */
/**
 * @brief Fonction qui lit le polynome a partir d'un fichier spécifique
 * 
 * @param nom_fichier le fichier a lire
 * @return le polynome lu
 */
p_polyf_t lire_polynome_float(char *nom_fichier)
{
  FILE *f;
  p_polyf_t p;
  int degre;
  int i;
  int cr;

  f = fopen(nom_fichier, "r");
  if (f == NULL)
  {
    fprintf(stderr, "erreur ouverture %s \n", nom_fichier);
    exit(-1);
  }

  cr = fscanf(f, "%d", &degre);
  if (cr != 1)
  {
    fprintf(stderr, "erreur lecture du degre\n");
    exit(-1);
  }
  p = creer_polynome(degre);

  for (i = 0; i <= degre; i++)
  {
    cr = fscanf(f, "%f", &p->coeff[i]);
    if (cr != 1)
    {
      fprintf(stderr, "erreur lecture coefficient %d\n", i);
      exit(-1);
    }
  }

  fclose(f);

  return p;
}

/* ==================================== */
/**
 * @brief Fonction qui ecrit dans la console le polynome
 * 
 * @param p le polynome a écrire dans la console
 */
void ecrire_polynome_float(p_polyf_t p)
{
  int i;

  printf("%f + %f x ", p->coeff[0], p->coeff[1]);

  for (i = 2; i <= p->degre; i++)
  {
    printf("+ %f X^%d ", p->coeff[i], i);
  }

  printf("\n");

  return;
}

/* ==================================== */
/**
 * @brief Fonction qui vérifie l'égalité de deux polynomes
 * 
 * @param p1 le polynome 1
 * @param p2 le polynome 2
 * @return si le polynome est egal ou non -> 1 oui
 *                                        -> 0 non 
 */
int egalite_polynome(p_polyf_t p1, p_polyf_t p2)
{

  if (p1->degre != p2->degre)
  {
    return 0;
  }

  for (int i = 0; i < p1->degre; i++)
  {
    if (p1->coeff[i] != p2->coeff[i])
    {
      return 0;
    }
  }

  return 1;
}

/* ==================================== */
/**
 * @brief Fonction qui additionne peux polynome ensemble
 * 
 * @param p1 le polynome 1
 * @param p2 le polynome 2
 * @return le polynome résultat
 */
p_polyf_t addition_polynome(p_polyf_t p1, p_polyf_t p2)
{
  p_polyf_t p3;
  register unsigned int i;

  p3 = creer_polynome(max(p1->degre, p2->degre));

  for (i = 0; i <= min(p1->degre, p2->degre); ++i)
  {
    p3->coeff[i] = p1->coeff[i] + p2->coeff[i];
  }

  if (p1->degre > p2->degre)
  {
    for (i = (p2->degre + 1); i <= p1->degre; ++i)
      p3->coeff[i] = p1->coeff[i];
  }
  else if (p2->degre > p1->degre)
  {
    for (i = (p1->degre + 1); i <= p2->degre; ++i)
      p3->coeff[i] = p2->coeff[i];
  }

  return p3;
}

/* ==================================== */
/**
 * @brief Fonction qui multiplie un polynome par un scalaire
 * 
 * @param p le polynome a multiplier
 * @param alpha le scalaire a multiplier
 * @return le polynome résultat
 */
p_polyf_t multiplication_polynome_scalaire(p_polyf_t p, float alpha)
{
  p_polyf_t res = creer_polynome(p->degre);

  for (int i = 0; i < p->degre + 1; i++)
  {
    res->coeff[i] = p->coeff[i] * alpha;
  }

  return res;
}

/* ==================================== */
/**
 * @brief Fonction qui calcul un polynome pour une valeur de X choisie en entré
 * 
 * @param p le polynome a évaluer
 * @param x le valeur de X
 * @return le résulat en floatant
 */
float eval_polynome(p_polyf_t p, float x)
{
  float res = p->coeff[0];
  float puissance = x;

  for (int i = 1; i < p->degre + 1; i++)
  {
    res += (p->coeff[i] * puissance);
    puissance *= x;
  }

  return res;
}

/* ==================================== */
/**
 * @brief Fonction qui multiplie deux polynomes ensemble
 * 
 * @param p1 le polynome 1
 * @param p2 le polynome 2
 * @return le polynome résultat 
 */
p_polyf_t multiplication_polynomes(p_polyf_t p1, p_polyf_t p2)
{

  p_polyf_t p3 = creer_polynome(p1->degre + p2->degre);

  init_polynome(p3,0);

  for (int i = 0; i < p1->degre + p2->degre + 1; i++)
  {
    for (int j = 0; j < i + 1; j++)
    {
      p3->coeff[i] += p1->coeff[j] * p2->coeff[i - j];
    }
  }

  return p3;
}

/* ==================================== */
/**
 * @brief Fonction qui calcul la puissance d'un polynome
 * 
 * @param p le polynome 1
 * @param n la valeur de la puissance
 * @return le polynome résultat 
 */
p_polyf_t puissance_polynome(p_polyf_t p, int n)
{

  p_polyf_t res;

  if (n == 0)
  {
    res = creer_polynome(1);
    init_polynome(res,0.0);
    res->coeff[0] = 1;
    return res;
  }

  res = creer_polynome(p->degre);

  for(int i=0;i<p->degre+1;i++){
    res->coeff[i] = p->coeff[i];
  }

  p_polyf_t temp;
  
  for (int i = 1; i < n; i++)
  {
    temp = multiplication_polynomes(p, res);
    if(i != 1){
      detruire_polynome(res);
    }
    res = temp;
  }

  return res;
}

/* ==================================== */
/**
 * @brief Fonction qui calcul la composition de deux polynomes
 * 
 * @param p le polynome p
 * @param q le polynome q
 * @return le résultat 
 */
p_polyf_t composition_polynome(p_polyf_t p, p_polyf_t q)
{
  p_polyf_t res = creer_polynome(p->degre * q->degre);
  init_polynome(res,0.0);

  p_polyf_t temp;
  p_polyf_t temp2;
  p_polyf_t temp3;

  for(int i=0;i<p->degre+1;i++){

    temp = puissance_polynome(q,i);
    temp2 = multiplication_polynome_scalaire(temp,p->coeff[i]);
    temp3 = addition_polynome(res, temp2);
    detruire_polynome(temp);
    detruire_polynome(temp2);
    //detruire_polynome(res);
    res = temp3;
  }

  return res;
}