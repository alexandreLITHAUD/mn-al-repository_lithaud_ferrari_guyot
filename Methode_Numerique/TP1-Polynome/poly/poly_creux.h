#ifndef POLY_CREUX_H
#define POLY_CREUX_H

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

/* STRUCTURES */

typedef struct n{
  int *degre;
  float *coeff;
  int nbCoeff;
} polyf_creux_t, *p_polyf_creux_t;


/* PROTOTYPES */

p_polyf_creux_t creer_polynome(int nbCoeff);

void init_polynome (p_polyf_creux_t p, float x);

void detruire_polynome (p_polyf_creux_t p);

p_polyf_creux_t lire_polynome_float(char *nom_fichier);

void ecrire_polynome_float (p_polyf_creux_t p);

int egalite_polynome (p_polyf_creux_t p1, p_polyf_creux_t p2);

float eval_polynome (p_polyf_creux_t p, float x);

p_polyf_creux_t addition_polynome (p_polyf_creux_t p1, p_polyf_creux_t p2);

p_polyf_creux_t multiplication_polynome_scalaire (p_polyf_creux_t p, float alpha);

p_polyf_creux_t multiplication_polynomes (p_polyf_creux_t p1, p_polyf_creux_t p2);

p_polyf_creux_t puissance_polynome (p_polyf_creux_t p, int n);

p_polyf_creux_t composition_polynome (p_polyf_creux_t p, p_polyf_creux_t q);

#endif