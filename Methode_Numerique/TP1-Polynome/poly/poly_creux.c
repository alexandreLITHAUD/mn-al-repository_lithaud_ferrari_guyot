#include <stdio.h>
#include <stdlib.h>

#include "poly_creux.h"

#include <x86intrin.h>

/* Prototypes des fonctions privés */
float puissance(float f, int i);
int nbDegreeDifferent(p_polyf_creux_t p1, p_polyf_creux_t p2);
p_polyf_creux_t harmonise_polynome(p_polyf_creux_t poly);

/* ========================================== */
/**
 * @brief Fonction qui créer un polynome creux
 * 
 * @param nbCoeff le nombre de coefficient du polynome
 * @return le polynome créer
 */
p_polyf_creux_t creer_polynome(int nbCoeff)
{
    p_polyf_creux_t p;

    p = (p_polyf_creux_t)malloc(sizeof(polyf_creux_t));

    p->nbCoeff = nbCoeff;
    p->degre = (int *)malloc(nbCoeff * sizeof(int));
    p->coeff = (float *)malloc(nbCoeff * sizeof(float));

    return p;
}

/* ========================================== */
/**
 * @brief Fonction qui initialise un polynome creux
 * 
 * @param p la polynome creux a initialiser
 * @param x la valeur a assigné
 */
void init_polynome(p_polyf_creux_t p, float x)
{
    for (int i = 0; i < p->nbCoeff; i++)
    {
        p->coeff[i] = x;
    }
}

/* ========================================== */
/**
 * @brief Fonction qui détruit en mémoire le polynome creux d'entré
 * 
 * @param p le polynome creux à détruire
 */
void detruire_polynome(p_polyf_creux_t p)
{
    free(p->coeff);
    free(p->degre);
    free(p);
}

/* ========================================== */
/**
 * @brief Fonction qui lit le polynome creux a partir d'un fichier spécifique
 * 
 * @param nom_fichier le fichier a lire
 * @return le polynome creux lu
 */
p_polyf_creux_t lire_polynome_float(char *nom_fichier)
{
    FILE *f;
    p_polyf_creux_t p;
    int nbCoff;
    int i;
    int cr;
    float temp;
    int degre;

    f = fopen(nom_fichier, "r");
    if (f == NULL)
    {
        fprintf(stderr, "erreur ouverture %s \n", nom_fichier);
        exit(-1);
    }

    cr = fscanf(f, "%d", &nbCoff);
    if (cr != 1)
    {
        fprintf(stderr, "erreur lecture du degre\n");
        exit(-1);
    }

    p = creer_polynome(nbCoff);

    for (i = 0; i < nbCoff; i++)
    {
        cr = fscanf(f, "%d", &degre);
        if (cr != 1)
        {
            fprintf(stderr, "erreur lecture coefficient %d\n", i);
            exit(-1);
        }

        cr = fscanf(f, "%f", &temp);

        if (cr != 1)
        {
            fprintf(stderr, "erreur lecture coefficient %d\n", i);
            exit(-1);
        }

        p->degre[i] = degre;
        p->coeff[i] = temp;
    }

    fclose(f);

    return p;
}

/* ========================================== */
/**
 * @brief Fonction qui ecrit dans la console le polynome creux
 * 
 * @param p le polynome creux a écrire dans la console
 */
void ecrire_polynome_float(p_polyf_creux_t p)
{

    for(int i=0;i<p->nbCoeff-1;i++){
        printf("%f X^%d + ",p->coeff[i],p->degre[i]);
    }

    printf("%f X^%d\n",p->coeff[p->nbCoeff-1],p->degre[p->nbCoeff-1]);
}

/* ========================================== */
/**
 * @brief Fonction qui additionne peux polynome creux ensemble
 * 
 * @param p1 le polynome creux 1
 * @param p2 le polynome creux 2
 * @return le polynome creux résultat
 */
p_polyf_creux_t addition_polynome(p_polyf_creux_t p1, p_polyf_creux_t p2)
{
    p_polyf_creux_t p3;
    
    p3 = creer_polynome(nbDegreeDifferent(p1,p2));

    int ittp1 = 0;
    int ittp2 = 0;
    int i = 0;

    while(ittp1 < p1->nbCoeff && ittp2 < p2->nbCoeff){

        if(p1->degre[ittp1] == p2->degre[ittp2]){
            p3->degre[i] = p1->degre[ittp1];
            p3->coeff[i] = p1->coeff[ittp1] + p2->coeff[ittp2];
            ittp1++;
            ittp2++;
        }

        else if(p1->degre[ittp1] > p2->degre[ittp2]){
            p3->degre[i] = p2->degre[ittp2];
            p3->coeff[i] = p2->coeff[ittp2];
            ittp2++; // ittp2++;
        }

        else if(p1->degre[ittp1] < p2->degre[ittp2]){
            p3->degre[i] = p1->degre[ittp1];
            p3->coeff[i] = p1->coeff[ittp1];
            ittp1++; // ittp1++;
        }

        i++;
    }

    while(ittp1 < p1->nbCoeff){
        p3->degre[i] = p1->degre[ittp1];
        p3->coeff[i] = p1->coeff[ittp1];
        ittp1++;
        i++; 
    }

    while(ittp2 < p2->nbCoeff){
        p3->degre[i] = p2->degre[ittp2];
        p3->coeff[i] = p2->coeff[ittp2];
        ittp2++;
        i++; 
    }

    p_polyf_creux_t res = harmonise_polynome(p3);

    return res;
}

/* ========================================== */
/**
 * @brief Fonction qui vérifie l'égalité de deux polynomes
 * 
 * @param p1 le polynome 1
 * @param p2 le polynome 2
 * @return si le polynome est egal ou non -> 1 oui
 *                                        -> 0 non 
 */
int egalite_polynome(p_polyf_creux_t p1, p_polyf_creux_t p2)
{
    if(p1->nbCoeff != p2->nbCoeff){
        return 0;
    }

    for(int i=0;i<p1->nbCoeff;i++){
        if(p1->coeff[i] != p2->coeff[i]){
            return 0;
        }

        if(p1->degre[i] != p2->degre[i]){
            return 0;
        }
    }

    return 1;
}

/* ========================================== */
/**
 * @brief Fonction qui calcul un polynome pour une valeur de X choisie en entré
 * 
 * @param p le polynome a évaluer
 * @param x le valeur de X
 * @return le résulat en floatant
 */
float eval_polynome(p_polyf_creux_t p, float x)
{
    float res = 0.0f;

    for(int i=0;i<p->nbCoeff;i++){
        res += p->coeff[i] * puissance(x,p->degre[i]);
    }

    return res;
}


/* ========================================== */
/**
 * @brief Fonction qui multiplie un polynome creux par un scalaire
 * 
 * @param p le polynome a multiplier
 * @param alpha le scalaire a multiplier
 * @return le polynome creux résultat
 */
p_polyf_creux_t multiplication_polynome_scalaire (p_polyf_creux_t p, float alpha)
{
    p_polyf_creux_t res;
    res = creer_polynome(p->nbCoeff);

    for(int i=0;i<p->nbCoeff;i++){
        res->degre[i] = p->degre[i];
        res->coeff[i] = p->coeff[i] * alpha;
    }

    return res;
}

/* ========================================== */
/**
 * @brief Fonction qui multiplie deux polynomes creux ensemble
 * 
 * @param p1 le polynome creux 1
 * @param p2 le polynome creux 2
 * @return le polynome résultat 
 */
p_polyf_creux_t multiplication_polynomes (p_polyf_creux_t p1, p_polyf_creux_t p2)
{
    p_polyf_creux_t p3 = creer_polynome(p1->nbCoeff * p2->nbCoeff);

    int compteur = 0;

    for(int i=0;i<p1->nbCoeff;i++){
        for(int j=0;j<p2->nbCoeff;j++){
            p3->degre[compteur] = p1->degre[i] + p2->degre[j];
            p3->coeff[compteur] = p1->coeff[i] * p2->coeff[j];
            compteur++;
        }
    }

    p_polyf_creux_t res = creer_polynome(1);
    p_polyf_creux_t temp;

    res->coeff[0] = p3->coeff[0];
    res->degre[0] = p3->degre[0];

    compteur = 0;

    for(int i=1;i<p3->nbCoeff;i++){
        p_polyf_creux_t temp2 = creer_polynome(1);
        temp2->degre[0] = p3->degre[i];
        temp2->coeff[0] = p3->coeff[i];

        if(temp2->degre[0] == 0 && temp2->coeff[0] == 0.0){
            //IGNORE
        }else{

            temp = addition_polynome(res,temp2);
            detruire_polynome(res);
            detruire_polynome(temp2);
            res = temp;

        }
    }

    detruire_polynome(p3);
    return res;
}

/* ========================================== */
/**
 * @brief Fonction qui calcul la puissance d'un polynome creux
 * 
 * @param p le polynome creux 1
 * @param n la valeur de la puissance
 * @return le polynome creux résultat 
 */
p_polyf_creux_t puissance_polynome (p_polyf_creux_t p, int n)
{
    p_polyf_creux_t res;

    if (n == 0)
    {
        res = creer_polynome(1);
        res->degre[0] = 0;
        res->coeff[0] = 1;
        return res;
    }

    res = creer_polynome(p->nbCoeff);

    for(int i=0;i<p->nbCoeff;i++){
        res->degre[i] = p->degre[i];
        res->coeff[i] = p->coeff[i];
    }

    p_polyf_creux_t temp;
    for (int i = 1; i < n; i++)
    {
        temp = multiplication_polynomes(p, res);
        if(i != 1){
            detruire_polynome(res);
        }
        res = temp;
    }

    return res;
}

/* ========================================== */
/**
 * @brief Fonction qui calcul la composition de deux polynomes creux
 * 
 * @param p le polynome creux p
 * @param q le polynome creux q
 * @return le résultat 
 */
// NON FONCTIONEL
p_polyf_creux_t composition_polynome (p_polyf_creux_t p, p_polyf_creux_t q)
{
    p_polyf_creux_t res = creer_polynome(p->nbCoeff * q->nbCoeff);
    //init_polynome(res,0.0);

    p_polyf_creux_t temp;
    p_polyf_creux_t temp2;
    p_polyf_creux_t temp3;

    for(int i=0;i<p->nbCoeff;i++){

        temp = puissance_polynome(q,i);
        temp2 = multiplication_polynome_scalaire(temp,p->coeff[i]);
        temp3 = addition_polynome(res, temp2);
        detruire_polynome(temp);
        detruire_polynome(temp2);
        res = temp3;
    }

    return res;
}

/* ========================================== */
/**
 * @brief Fonction puissance
 * 
 * @param f le nombre a elever a la puissance
 * @param i la puissance
 * @return le résultat 
 */
float puissance(float f, int i)
{
    if(i == 0){
        return 1;
    }
    return puissance(f,i-1) * f;
}

/* ========================================== */
/**
 * @brief Fonction qui calcul le nombre de dégre différent pour deux polynomes creux
 * Utile pour addition de polynome creux
 * @param p1 le polynome creux 1
 * @param p2 le polynome creux 2
 * @return le nombre de degrées différents 
 */
int nbDegreeDifferent(p_polyf_creux_t p1, p_polyf_creux_t p2)
{
    int res = 0;

    int ittp1 = 0;
    int ittp2 = 0;

    while((ittp1 < p1->nbCoeff) || (ittp2 < p2->nbCoeff)){

        if(ittp1 >= p1->nbCoeff){
            res++;
            ittp2++;
        }

        else if(ittp2 >= p2->nbCoeff){
            res++;
            ittp1++;
        }

        else if(p1->degre[ittp1] == p2->degre[ittp2]){
            res++;
            ittp1++;
            ittp2++;
        }

        else if(p1->degre[ittp1] > p2->degre[ittp2]){
            res++;
            ittp2++;
        }

        else if(p1->degre[ittp1] < p2->degre[ittp2]){
            res++;
            ittp1++;
        }
    }

    return res;

}

/* ========================================== */
/**
 * @brief Fonction qui enleve tous les coefficient inutile d'un polynome creux
 * 
 * @param poly le polynome creux a harminoniser
 * @return le polynome d'entré si il n'y a rien a harmoniser
 *         le polynome harminiser sinon
 * 
 *          ATTENTION le polynome d'entrée est supprimer si il retourne une
 *          version harmoniser  
 */
p_polyf_creux_t harmonise_polynome(p_polyf_creux_t poly)
{

    int compteur = 0;

    for(int i=0;i<poly->nbCoeff;i++){
        if(poly->coeff[i] != 0.0){
            compteur++;
        }
    }

    if(poly->nbCoeff == compteur){
        return poly;
    }else{

        if(compteur == 0){
            return poly;
        }

        p_polyf_creux_t res = creer_polynome(compteur);
        int index = 0;

        for(int i=0;i<poly->nbCoeff;i++){
            
            if(poly->coeff[i] != 0.0){
                res->coeff[index] = poly->coeff[i];
                res->degre[index] = poly->degre[i];
                index++; 
            }
        }

        detruire_polynome(poly);
        return res;
    }

}

