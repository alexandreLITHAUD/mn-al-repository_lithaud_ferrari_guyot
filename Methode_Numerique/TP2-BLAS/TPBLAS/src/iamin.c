#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


CBLAS_INDEX mnblas_isamin(const int N, const float  *X, const int incX)
{
    register unsigned int i = 0 ;
    int index = 0;
    float min = X[0];
    
    for (i=1; i< N; i+=incX)
    {
        if(X[i] < min)
        {
            min=X[i];
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_idamin(const int N, const double *X, const int incX)
{
    register unsigned int i = 0 ;
    int index = 0;
    double min = X[0];
    
    for (i=1; i< N; i+=incX)
    {
        if(X[i] > min)
        {
            min=X[i];
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_icamin(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    complexe_float_t *pX = X;
    int index = 0;
    float min = pX[0].real + pX[0].imaginary;
    
    for (i=1; i< N; i+=incX)
    {
        if((pX[i].real + pX[i].imaginary) < min)
        {
            min=pX[i].real + pX[i].imaginary;
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_izamin(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    complexe_double_t *pX = X;
    int index = 0;
    float min = pX[0].real + pX[0].imaginary;
    
    for (i=1; i< N; i+=incX)
    {
        if((pX[i].real + pX[i].imaginary) < min)
        {
            min=pX[i].real + pX[i].imaginary;
            index=i;
        }
    }

    return index;
}