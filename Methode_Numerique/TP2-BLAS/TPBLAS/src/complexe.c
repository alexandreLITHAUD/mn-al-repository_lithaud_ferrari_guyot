#include "complexe.h"

complexe_float_t add_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;
  
  return r ;
}

complexe_double_t add_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;
  
  return r ;
}

complexe_float_t mult_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  r.imaginary = (c1.imaginary * c2.real) + (c1.real * c2.imaginary);
  r.real = (c1.real * c2.real) + -(c1.imaginary * c2.imaginary);
  
  return r ;
}

complexe_double_t mult_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
  {
  complexe_double_t r ;

  r.imaginary = (c1.imaginary * c2.real) + (c1.real * c2.imaginary);
  r.real = (c1.real * c2.real) + -(c1.imaginary * c2.imaginary);
  
  return r ;
}
  

complexe_float_t div_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  complexe_float_t c2conjug = c2;
  c2conjug.imaginary = -c2conjug.imaginary;

  complexe_float_t denominateur = mult_complexe_float(c2,c2conjug);
  complexe_float_t numerateur = mult_complexe_float(c1,c2conjug);

  r.real = numerateur.real / denominateur.real;
  r.imaginary = numerateur.imaginary / denominateur.real;
  
  return r ;
}

complexe_double_t div_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  complexe_double_t c2conjug = c2;
  c2conjug.imaginary = -c2conjug.imaginary;

  complexe_double_t denominateur = mult_complexe_double(c2,c2conjug);
  complexe_double_t numerateur = mult_complexe_double(c1,c2conjug);

  r.real = numerateur.real / denominateur.real;
  r.imaginary = numerateur.imaginary / denominateur.real;
  
  return r ;
}