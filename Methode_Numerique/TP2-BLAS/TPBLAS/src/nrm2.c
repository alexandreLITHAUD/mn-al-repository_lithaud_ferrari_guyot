#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"
#include <math.h>


float module_float(complexe_float_t z)
{
    float module = sqrt(z.real * z.real + z.imaginary * z.imaginary);
    return module;
}


double module_double(complexe_double_t z)
{
    double module = sqrt(z.real * z.real + z.imaginary * z.imaginary);
    return module;
}


float  mnblas_snrm2(const int N, const float *X, const int incX)
{
    register unsigned int i = 0 ;
    float res = 0;

    for(i=0; i<N; i+=incX)
    {
        res += sqrt(sqrt(X[i]));
    }

    return res;
}


double mnblas_dnrm2(const int N, const double *X, const int incX)
{
    register unsigned int i = 0 ;
    double res = 0;

    for(i=0; i<N; i+=incX)
    {
        res += sqrt(sqrt(X[i]));
    }

    return res;
}


float  mnblas_scnrm2(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    float res = 0;
    complexe_float_t *pX = X;

    for(i=0; i<N; i+=incX)
    {
        res += sqrt(sqrt(module_float(pX[i])));
    }

    return res;
}


double mnblas_dznrm2(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    float res = 0;
    complexe_double_t *pX = X;

    for(i=0; i<N; i+=incX)
    {
        res += sqrt(sqrt(module_double(pX[i])));
    }

    return res;
}