#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"

/*
float mncblas_sdot(const int N, const float *X, const int incX, 
                 const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float dot = 0.0 ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
    {
      dot = dot + X [i] * Y [j] ;
    }

  return dot ;
}
*/

complexe_float_t conjugue_float(complexe_float_t z){

  complexe_float_t conjugue_z;
  conjugue_z.real = z.real;
  conjugue_z.imaginary = -z.imaginary;

  return conjugue_z;
}


complexe_double_t conjugue_double(complexe_double_t z){

  complexe_double_t conjugue_z;
  conjugue_z.real = z.real;
  conjugue_z.imaginary = -z.imaginary;

  return conjugue_z;
}

/* float */
float mncblas_sdot(const int N, const float *X, const int incX, const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  float dot = 0.0 ;

  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX)
    {
      dot += X [i] * Y [j] ;
      j+=incY ;
    }

  return dot ;
}


/* double */
double mncblas_ddot(const int N, const double *X, const int incX, const double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  double dot = 0.0;

  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX)
  {
    dot += X [i] * Y [j] ;
    j+=incY ;
  }
  return dot ;
}


/* complexe float */
void mncblas_cdotu_sub(const int N, void *X, const int incX, void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_float_t* pX = X;
  complexe_float_t* pY = Y;
  complexe_float_t* pdotu = dotu;

  pdotu->real = 0;
  pdotu->imaginary = 0;
  
  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX)
  {
    *pdotu = add_complexe_float(*pdotu,mult_complexe_float (pX[i],pY[j])) ;
    j+=incY ;
  }
  //return pdotu;
}


/* complexe float conjugue */
void mncblas_cdotc_sub(const int N, void *X, const int incX, void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_float_t* pX=X;
  complexe_float_t* pY=Y;
  complexe_float_t* pdotc = dotc;
  
  pdotc->real = 0;
  pdotc->imaginary = 0;
  
  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX){
    *pdotc = add_complexe_float(*pdotc,mult_complexe_float(conjugue_float(pX[i]),pY[j])) ;
    j+=incY ;
  }
  //return pdotc;
  
}


/* complexe double */
void mncblas_zdotu_sub(const int N, void *X, const int incX, void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_double_t* pdotu = dotu ;
  complexe_double_t* pX = X;
  complexe_double_t* pY = Y;

  pdotu->real = 0;
  pdotu->imaginary = 0;
  
  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX)
  {
    *pdotu = add_complexe_double(*pdotu,mult_complexe_double(pX[i],pY[j]));
    j+=incY;
  }
  //return pdotu;
}
  

/* complexe double conjugue */
void mncblas_zdotc_sub(const int N, void *X, const int incX, void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  complexe_double_t* pdotc = dotc ;
  complexe_double_t* pX = X;
  complexe_double_t* pY = Y;

  pdotc->real = 0;
  pdotc->imaginary = 0;
  
  #pragma omp parallel for
  for (i = 0 ; i < N ; i += incX)
  {
    *pdotc = add_complexe_double(*pdotc,mult_complexe_double(pX[i],pY[j]));
    j+=incY;
  }
  //return pdotc;
}




