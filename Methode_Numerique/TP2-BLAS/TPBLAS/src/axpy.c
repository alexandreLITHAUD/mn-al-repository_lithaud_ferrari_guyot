#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


/* float */
void mnblas_saxpy(const int N, const float alpha, const float *X, const int incX, float *Y, const int incY)
{
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    #pragma omp parallel for
    for(i=0; i<N; i+=incX)
    {
        Y[j] += X[i] * alpha;
        j += incY;
    }
}


/* double */
void mnblas_daxpy(const int N, const double alpha, const double *X, const int incX, double *Y, const int incY)
{
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    #pragma omp parallel for
    for(i=0; i<N; i+=incX)
    {
        Y[j] += X[i] * alpha;
        j += incY;
    }
}


/* complexe float */
void mnblas_caxpy(const int N, void *alpha, void *X, const int incX, void *Y, const int incY)
{
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    complexe_float_t* pX = X;
    complexe_float_t* pY = Y;
    complexe_float_t* palpha = alpha;


    #pragma omp parallel for
    for(i=0; i<N; i+=incX)
    {
       pY[j] = add_complexe_float(pY[j],mult_complexe_float(pX[i],*palpha));
       j += incY;
    }
}


/* complexe double */
void mnblas_zaxpy(const int N, void *alpha, void *X, const int incX, void *Y, const int incY)
{
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    complexe_double_t* pX = X;
    complexe_double_t* pY = Y;
    complexe_double_t* palpha = alpha;

    #pragma omp parallel for
    for(i=0; i<N; i+=incX)
    {
       pY[j] = add_complexe_double(pY[j],mult_complexe_double(pX[i],*palpha));
       j += incY;
    }
}

