#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


float  mnblas_sasum(const int N, const float *X, const int incX)
{
    register unsigned int i = 0 ;
    float res = 0;

    for(i=0; i<N; i+=incX)
    {
        res += X[i];
    }

    return res;
}


double mnblas_dasum(const int N, const double *X, const int incX)
{
    register unsigned int i = 0 ;
    double res = 0;

    for(i=0; i<N; i+=incX)
    {
        res += X[i];
    }

    return res;
}


float  mnblas_scasum(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    float res = 0;
    complexe_float_t *pX = X;

    for(i=0; i<N; i+=incX)
    {
        res += pX[i].real + pX[i].imaginary;
    }

    return res;
}


double mnblas_dzasum(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    double res = 0;
    complexe_double_t *pX = X;

    for(i=0; i<N; i+=incX)
    {
        res += pX[i].real + pX[i].imaginary;
    }

    return res;
}