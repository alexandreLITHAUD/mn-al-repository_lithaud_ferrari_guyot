#include "mnblas.h"
#include "complexe2.h"

// Y = alpha * A * X + beta * Y;

/*
    X et Y des vecteurs
    alpha et beta des scalaires
    A une matrice
    N et M les dimensions de la matrice 
*/


/* float */
void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY)
{
    register unsigned int i = 0 ;

    float alpha_A[N*M] ;
    for(i=0;i<N*M;i++){
        alpha_A[i] = 0;
    }

    float alpha_A_X[N] ;
    for(i=0;i<N;i++){
        alpha_A_X[i] = 0;
    }

    float beta_Y[N] ;
    for(i=0;i<N;i++){
        beta_Y[i] = 0;
    }

    for (i = 0 ; i < N*M ; i += 1)
    {
        alpha_A[i] = alpha * A[i];      // scalaire * matrice => matrice
    }

    for (i = 0 ; i < N ; i += 1) 
    {
        alpha_A_X[0] += alpha_A[i] * X[0];
        alpha_A_X[1] += alpha_A[i] * X[1];     // matrice * vecteur => vecteur
        alpha_A_X[2] += alpha_A[i] * X[2];
    }

    for (i = 0 ; i < N ; i++)
    {
        beta_Y[i] = beta * Y[i];    // scalaire * vecteur => vecteur
    }

    for (i = 0 ; i < N ; i++)
    {
        Y[i] = alpha_A_X[i] + beta_Y[i];    // vecteur + vecteur => vecteur
    }
    
}


/* double */
void mncblas_dgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY)
{
    register unsigned int i = 0 ;

    double alpha_A[N*M] ;
    for(i=0;i<N*M;i++){
        alpha_A[i] = 0;
    }

    double alpha_A_X[N] ;
    for(i=0;i<N;i++){
        alpha_A_X[i] = 0;
    }

    double beta_Y[N] ;
    for(i=0;i<N;i++){
        beta_Y[i] = 0;
    }

    for (i = 0 ; i < N*M ; i += 1)
    {
        alpha_A[i] = alpha * A[i];      // scalaire * matrice => matrice
    }

    for (i = 0 ; i < N ; i += 1) 
    {
        alpha_A_X[0] += alpha_A[i] * X[0];
        alpha_A_X[1] += alpha_A[i] * X[1];      // matrice * vecteur => vecteur
        alpha_A_X[2] += alpha_A[i] * X[2];
    }

    for (i = 0 ; i < N ; i++)
    {
        beta_Y[i] = beta * Y[i];    // scalaire * vecteur => vecteur
    }

    for (i = 0 ; i < N ; i++)
    {
        Y[i] = alpha_A_X[i] + beta_Y[i];    // vecteur + vecteur => vecteur
    }

}


/* complexe float */
void mncblas_cgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 void *alpha, void *A, const int lda,
                 void *X, const int incX, void *beta,
                 void *Y, const int incY)
{
    register unsigned int i = 0 ;
    complexe_float_t* palpha = alpha;
    complexe_float_t* pbeta = beta;
    complexe_float_t* pA = A;
    complexe_float_t* pX = X;
    complexe_float_t* pY = Y;
    
    complexe_float_t alpha_A[M*N];
    for(i=0;i<N*M;i++){
        alpha_A[i].real = 0;
        alpha_A[i].imaginary = 0;
    }

    complexe_float_t beta_Y[N];
    for(i=0;i<N;i++){
        beta_Y[i].real = 0;
        beta_Y[i].imaginary = 0;
    }

    complexe_float_t alpha_A_X[N];
    for(i=0;i<N*M;i++){
        alpha_A_X[i].real = 0;
        alpha_A_X[i].imaginary = 0;
    }
    
    for (i = 0 ; i < N*M ; i += 1)
    {
        alpha_A[i] = mult_complexe_float(*palpha, pA[i]);      // scalaire * matrice => matrice
    }

    for (i = 0 ; i < N ; i += 1) 
    {
        alpha_A_X[0] = add_complexe_float(alpha_A_X[0],mult_complexe_float(alpha_A[i],pX[0]));
        alpha_A_X[1] = add_complexe_float(alpha_A_X[1],mult_complexe_float(alpha_A[i],pX[1]));     // matrice * vecteur => vecteur
        alpha_A_X[2] = add_complexe_float(alpha_A_X[2],mult_complexe_float(alpha_A[i],pX[2]));
    }

    for (i = 0 ; i < N ; i++)
    {
        beta_Y[i] = mult_complexe_float(*pbeta,pY[i]);    // scalaire * vecteur => vecteur
    }

    for (i = 0 ; i < N ; i++)
    {
        pY[i] = add_complexe_float(alpha_A_X[i], beta_Y[i]);    // vecteur + vecteur => vecteur
    }
    
}


/* complexe double */
void mncblas_zgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 void *alpha, void *A, const int lda,
                 void *X, const int incX, void *beta,
                 void *Y, const int incY)
{
    register unsigned int i = 0 ;
    complexe_double_t* palpha = alpha;
    complexe_double_t* pbeta = beta;
    complexe_double_t* pA = A;
    complexe_double_t* pX = X;
    complexe_double_t* pY = Y;
    
    complexe_double_t alpha_A[M*N];
    for(i=0;i<N*M;i++){
        alpha_A[i].real = 0;
        alpha_A[i].imaginary = 0;
    }

    complexe_double_t beta_Y[N];
    for(i=0;i<N;i++){
        beta_Y[i].real = 0;
        beta_Y[i].imaginary = 0;
    }

    complexe_double_t alpha_A_X[N];
    for(i=0;i<N*M;i++){
        alpha_A_X[i].real = 0;
        alpha_A_X[i].imaginary = 0;
    }

    for (i = 0 ; i < N*M ; i += 1)
    {
        alpha_A[i] = mult_complexe_double(*palpha, pA[i]);      // scalaire * matrice => matrice
    }

    for (i = 0 ; i < N ; i += 1) 
    {
        alpha_A_X[0] = add_complexe_double(alpha_A_X[0],mult_complexe_double(alpha_A[i],pX[0]));
        alpha_A_X[1] = add_complexe_double(alpha_A_X[1],mult_complexe_double(alpha_A[i],pX[1]));     // matrice * vecteur => vecteur
        alpha_A_X[2] = add_complexe_double(alpha_A_X[2],mult_complexe_double(alpha_A[i],pX[2]));
    }

    for (i = 0 ; i < N ; i++)
    {
        beta_Y[i] = mult_complexe_double(*pbeta,pY[i]);    // scalaire * vecteur => vecteur
    }

    for (i = 0 ; i < N ; i++)
    {
        pY[i] = add_complexe_double(alpha_A_X[i], beta_Y[i]);    // vecteur + vecteur => vecteur
    }
    
}

