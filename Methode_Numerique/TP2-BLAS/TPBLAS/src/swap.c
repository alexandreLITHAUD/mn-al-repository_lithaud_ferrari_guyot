#include "mnblas.h"
#include "complexe2.h"

void mncblas_sswap(const int N, float *X, const int incX, 
                 float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float save ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
    {
      save = Y [j] ;
      Y [j] = X [i] ;
      X [i] = save ;
    }

  return ;
}

void mncblas_dswap(const int N, double *X, const int incX, 
                 double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register double save ;
  
  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
    {
      save = Y [j] ;
      Y [j] = X [i] ;
      X [i] = save ;
    }

  return ;
}

void mncblas_cswap(const int N, void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t save ;


  complexe_float_t* Xc = (complexe_float_t*) X;
  complexe_float_t* Yc = (complexe_float_t*) X;


  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
    {
      //save = Y [j] ;
      save.real = Yc[j].real;
      save.imaginary = Yc[j].imaginary;

      //Y [j] = X [i] ;
      Yc[j].real = Xc[j].real;
      Yc[j].imaginary = Yc[j].imaginary;

      //X [i] = save ;
      Xc[i].real = save.real;
      Xc[i].imaginary = save.imaginary;
    }

  return ;
}

void mncblas_zswap(const int N, void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t save ;


  complexe_double_t* Xc = (complexe_double_t*) X;
  complexe_double_t* Yc = (complexe_double_t*) X;


  for (; ((i < N) && (j < N)) ; i += incX, j+=incY)
    {
      //save = Y [j] ;
      save.real = Yc[j].real;
      save.imaginary = Yc[j].imaginary;

      //Y [j] = X [i] ;
      Yc[j].real = Xc[j].real;
      Yc[j].imaginary = Yc[j].imaginary;

      //X [i] = save ;
      Xc[i].real = save.real;
      Xc[i].imaginary = save.imaginary;
    }

  return ;
}
