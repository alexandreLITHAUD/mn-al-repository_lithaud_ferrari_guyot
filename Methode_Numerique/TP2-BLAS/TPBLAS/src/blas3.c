#include "../include/mnblas.h"
#include "../include/complexe2.h"

void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const float alpha, const float *A,
                   const int lda, const float *B, const int ldb,
                   const float beta, float *C, const int ldc)
{
    //  C = alpha*op(A)*op(B) + beta*C
    // Or MNCBLAS_TRANSPOSE toujours à NO donc op(a) = A et op(b) = B

    // A de taille m x k
    // B de taille k x n
    // C de taille m x n

    //  A*B    ==============================
    // --------------------------------------
    float temp[M * K];
    
    // --------------------------------------
    // Initialisation pour éviter les résidus de mémoire
    for (int i = 0; i < M * K; i++)
    {
        temp[i] = 0;
    }
    // --------------------------------------

        // M * K * K itérations
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < K; j++)
            {

                // int x = 0;
                // int y = 0;
                // // M ou K itérations
                // while ((x < M) & (y < K))
                // {
                //     // 2 FLOP (add + mult)
                //     temp[i * K + j] += A[i * K + y] * B[x * K + j];

                //     x++;
                //     y++;
                // }

                for (int x = 0; x < K; x++){
                    // 2 FLOP (add + mult)
                    temp[i * K + j] += A[i * K + x] * B[x * K + j];
                }
            }
        }

        // M * K * K * 2 FLOP
    //=====================================

    // alpha * A * B
    for (int i = 0; i < K * M; i += 1)
    {
        temp[i] = alpha * temp[i]; // scalaire * matrice => matrice
    }
    // K * M FLOP

    // beta * C
    for (int i = 0; i < N * M; i += 1)
    {
        C[i] = beta * C[i]; // scalaire * matrice => matrice
    }
    // N* M FLOP
    
    // alpha * A * B    +   beta * C
    for (int i = 0; i < N * K; i += 1)
    {
        C[i] = temp[i] + C[i]; // matrice + matrice => matrice
    }
    //N * K FLOP
}

void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const double alpha, const double *A,
                   const int lda, const double *B, const int ldb,
                   const double beta, double *C, const int ldc)
{
    //  A*B    ==============================
    // --------------------------------------
    double temp[M * K];

    // Initialisation pour éviter les résidus de mémoire
    for (int i = 0; i < M * K; i++)
    {
        temp[i] = 0;
    }
    // --------------------------------------

        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < K; j++)
            {

                // int x = 0;
                // int y = 0;
                // while ((x < M) & (y < K))
                // {
                //     temp[i * K + j] += A[i * K + y] * B[x * K + j];

                //     x++;
                //     y++;
                // }

                for (int x = 0; x < K; x++){
                    // 2 FLOP (add + mult)
                    temp[i * K + j] += A[i * K + x] * B[x * K + j];
                }
            }
        }
    //=======================================

    // alpha * A * B
    for (int i = 0; i < K * M; i += 1)
    {
        temp[i] = alpha * temp[i]; // scalaire * matrice => matrice
    }

    // beta * C
    for (int i = 0; i < N * M; i += 1)
    {
        C[i] = beta * C[i]; // scalaire * matrice => matrice
    }

    // alpha * A * B    +   beta * C
    for (int i = 0; i < N * K; i += 1)
    {
        C[i] = temp[i] + C[i]; // matrice + matrice => matrice
    }
};

void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const void *alpha, const void *A,
                   const int lda, const void *B, const int ldb,
                   const void *beta, void *C, const int ldc)
{
    complexe_float_t *Ac = (complexe_float_t *)A;
    complexe_float_t *Bc = (complexe_float_t *)B;
    complexe_float_t *Cc = (complexe_float_t *)C;

    complexe_float_t *alphac = (complexe_float_t *)alpha;
    complexe_float_t *betac = (complexe_float_t *)beta;

    //  A*B    ==============================
    // --------------------------------------
    complexe_float_t temp[M * K];

    // Initialisation pour éviter les résidus de mémoire
    for (int i = 0; i < M * K; i++)
    {
        temp[i].real = 0;
        temp[i].imaginary = 0;
    }
    // --------------------------------------


    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < K; j++)
        {

            // int x = 0;
            // int y = 0;
            // while ((x < M) & (y < K))
            // {
            //     temp[i * K + j] = add_complexe_float(temp[i * K + j], mult_complexe_float(Ac[i * K + y], Bc[x * K + j]));

            //     x++;
            //     y++;
            // }

            for (int x = 0; x < K; x++){
                temp[i * K + j] = add_complexe_float(temp[i * K + j], mult_complexe_float(Ac[i * K + x], Bc[x * K + j]));
            }
            //Appel à add_complexe_float -> 2 FLOP
            //Appel à mult_complexe_float -> 6 ~ 7 FLOP
        }
    }
    //=======================================

    // alpha * A * B
    for (int i = 0; i < K * M; i += 1)
    {
        temp[i] = mult_complexe_float(*alphac, temp[i]);
    }
    //Appel à mult_complexe_float -> 6 ~ 7 FLOP

    // beta * C
    for (int i = 0; i < N * M; i += 1)
    {
        Cc[i] = mult_complexe_float(*betac, Cc[i]);
    }
    //Appel à mult_complexe_float -> 6 ~ 7 FLOP


    // alpha * A * B    +   beta * C
    for (int i = 0; i < N * K; i += 1)
    {
        Cc[i] = add_complexe_float(temp[i], Cc[i]);
    }
    //Appel à add_complexe_float -> 2 FLOP
};

void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                   MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                   const int K, const void *alpha, const void *A,
                   const int lda, const void *B, const int ldb,
                   const void *beta, void *C, const int ldc)
{
    complexe_double_t *Ac = (complexe_double_t *)A;
    complexe_double_t *Bc = (complexe_double_t *)B;
    complexe_double_t *Cc = (complexe_double_t *)C;

    complexe_double_t *alphac = (complexe_double_t *)alpha;
    complexe_double_t *betac = (complexe_double_t *)beta;

    //  A*B    ==============================
    // --------------------------------------
    complexe_double_t temp[M * K];

    // Initialisation pour éviter les résidus de mémoire
    for (int i = 0; i < M * K; i++)
    {
        temp[i].real = 0;
        temp[i].imaginary = 0;
    }
    // --------------------------------------

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < K; j++)
        {

            // int x = 0;
            // int y = 0;
            // while ((x < M) & (y < K))
            // {
            //     temp[i * K + j] = add_complexe_double(temp[i * K + j], mult_complexe_double(Ac[i * K + y], Bc[x * K + j]));

            //     x++;
            //     y++;
            // }

            for (int x = 0; x < K; x++){
                temp[i * K + j] = add_complexe_double(temp[i * K + j], mult_complexe_double(Ac[i * K + x], Bc[x * K + j]));
            }
        }
    }
    //=====================================

    // alpha * A * B
    for (int i = 0; i < K * M; i += 1)
    {
        temp[i] = mult_complexe_double(*alphac, temp[i]);
    }

    // beta * C
    for (int i = 0; i < N * M; i += 1)
    {
        Cc[i] = mult_complexe_double(*betac, Cc[i]);
    }

    // alpha * A * B    +   beta * C
    for (int i = 0; i < N * K; i += 1)
    {
        Cc[i] = add_complexe_double(temp[i], Cc[i]);
    }
};