#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


CBLAS_INDEX mnblas_isamax(const int N, const float  *X, const int incX)
{
    register unsigned int i = 0 ;
    int index = 0;
    float max = X[0];
    
    for (i=1; i< N; i+=incX)
    {
        if(X[i] > max)
        {
            max=X[i];
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_idamax(const int N, const double *X, const int incX)
{
    register unsigned int i = 0 ;
    int index = 0;
    double max = X[0];
    
    for (i=1; i< N; i+=incX)
    {
        if(X[i] > max)
        {
            max=X[i];
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_icamax(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    complexe_float_t *pX = X;
    int index = 0;
    float max = pX[0].real + pX[0].imaginary;
    
    for (i=1; i< N; i+=incX)
    {
        if((pX[i].real + pX[i].imaginary) > max)
        {
            max=pX[i].real + pX[i].imaginary;
            index=i;
        }
    }

    return index;
}


CBLAS_INDEX mnblas_izamax(const int N, void *X, const int incX)
{
    register unsigned int i = 0 ;
    complexe_double_t *pX = X;
    int index = 0;
    float max = pX[0].real + pX[0].imaginary;
    
    for (i=1; i< N; i+=incX)
    {
        if((pX[i].real + pX[i].imaginary) > max)
        {
            max=pX[i].real + pX[i].imaginary;
            index=i;
        }
    }

    return index;
}