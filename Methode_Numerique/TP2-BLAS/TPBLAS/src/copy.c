#include "mnblas.h"
#include "complexe2.h"

void mncblas_scopy(const int N, const float *X, const int incX, 
                 float *Y, const int incY)
{
  #pragma omp parallel for
  for (register unsigned int i = 0;i < N; i += incX)
    {
      Y [i] = X [i] ;
    }

  return ;
}

void mncblas_dcopy(const int N, const double *X, const int incX, 
                 double *Y, const int incY)
{

  #pragma omp parallel for
  for (register unsigned int i = 0;i < N; i += incX)
    {
      Y [i] = X [i] ;
    }

  return ;

}

void mncblas_ccopy(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  complexe_float_t* yc = (complexe_float_t*)Y;
  complexe_float_t* xc = (complexe_float_t*)X;

  #pragma omp parallel for
  for (register unsigned int i = 0;i < N; i += incX)
    {
      yc[i].real = xc[i].real;
      yc[i].imaginary = xc[i].imaginary;
    }

  return ;

}

void mncblas_zcopy(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  complexe_double_t* yc = (complexe_double_t*)Y;
  complexe_double_t* xc = (complexe_double_t*)X;

  #pragma omp parallel for
  for (register unsigned int i = 0;i < N; i += incX)
    {
      yc[i].real = xc[i].real;
      yc[i].imaginary = xc[i].imaginary;
    }

  return ;
}
