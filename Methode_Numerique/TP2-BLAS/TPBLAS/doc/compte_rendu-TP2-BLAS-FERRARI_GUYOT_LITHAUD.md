# Compte Rendu TP2 MN - BLAS - FERRARI GUYOT LITHAUD

- [Compte Rendu TP2 MN - BLAS - FERRARI GUYOT LITHAUD](#compte-rendu-tp2-mn---blas---ferrari-guyot-lithaud)
- [Fonctions](#fonctions)
  - [Fonctions BLAS 1](#fonctions-blas-1)
  - [Fonctions BLAS 2](#fonctions-blas-2)
  - [Fonctions BLAS 3](#fonctions-blas-3)
- [Test de performance](#test-de-performance)
  - [Performance BLAS 1](#performance-blas-1)
  - [Performance BLAS 2](#performance-blas-2)
  - [Performance BLAS 3](#performance-blas-3)
- [Conclusion](#conclusion)
  - [Problème retenu lors de l’utilisation des 4 threads](#problème-retenu-lors-de-lutilisation-des-4-threads)
  - [Conclusion générale](#conclusion-générale)
- [Annexe](#annexe)

> Tous les fichiers de code sont dans le package [src](../src/)

> Tous les fichiers de tests sont dans le package [exemples](../examples/)

# Fonctions

## Fonctions BLAS 1

- copy : [copy.c](../src/copy.c) | [test_copy.c](../examples/test_copy.c)
- swap : [swap.c](../src/swap.c) | [test_swap.c](../examples/test_swap.c)
- dot : [dot.c](../src/dot.c) | [test_dot.c](../examples/test_dot.c)
- asum : [asum.c](../src/asum.c) | [test_asum.c](../examples/test_asum.c)
- iamax : [iamax.c](../src/iamax.c) | [test_iamax.c](../examples/test_iamax.c)
- iamin : [iamin.c](../src/iamin.c) | [test_iamin.c](../examples/test_iamin.c)
- nrm 2 : [nrm2.c](../src/nrm2.c) | [test_nrm2.c](../examples/test_nrm2.c)
- axpy : [axpy.c](../src/axpy.c) | [test_axpy.c](../examples/test_axpy.c)

## Fonctions BLAS 2

- gemv2 : [gemv2.c](../src/gemv2.c) | [test_gemv2.c](../examples/test_gemv2.c)

## Fonctions BLAS 3

- gemm : [gemm.c](../src/gemm.c) | [test_gemm.c](../examples/test_gemm.c)


# Test de performance

## Performance BLAS 1

Test Performance axpy: [test_axpy.c](../examples/test_axpy.c)
Test Performance copy: [test_copy.c](../examples/test_copy.c)
Test Performance dot: [test_dot.c](../examples/test_dot.c)


> Voir images en [annexe](#annexe) : 


## Performance BLAS 2

Test Performance BLAS2: [test_gemv2.c](../examples/test_gemv2.c)

## Performance BLAS 3

Test Performance BLAS3: [test_blas3.c](../examples/test_blas3.c)


# Conclusion

## Problème retenu lors de l’utilisation des 4 threads

Comme on peut le voir dans les tableaux et graphiques ci-dessus et contrairement ce à quoi l’on pourrait s’attendre, l'utilisation d'OpenMP en utilisant 4 threads n'augmente pas les performances. L’observation constatée devrait se situer entre celles des expériences à 2 et 8 threads.

Cette mesure a été effectuée sur un PC Lenovo équipé du processeur **Intel(R) Core(TM) i5-10300H CPU @2.50GHz**.

Lien de la page Intel : https://www.intel.fr/content/www/fr/fr/products/sku/201839/intel-core-i510300h-processor-8m-cache-up-to-4-50-ghz/specifications.html.

Les conditions d’utilisation intensives dans lesquelles ont été réalisées ces mesures expliquent en partie les résultats observés.

**En lançant les tests directement après le démarrage de la machine (sans processus superflu en arrière-plan), on constate des performances conformes aux prévisions.**

## Conclusion générale

BLAS1 et BLAS2 ont des performances similaires, comme le montre l’intensité arithmétique en O(1) de leurs fonctions.

Cependant, BLAS3 et plus particulièrement la multiplication de matrices sont bien plus coûteuses pour le processeur. En effet, elles possèdent une intensité arithmétique proche de O(n).

Dans tous les cas, l'optimisation des boucles for dans ces différentes fonctions permettent une grande augmentation des performances.
De plus, l’augmentation des performances va également de pair avec le nombre de threads (cœurs) utilisés.

Ainsi, OpenMP permet une augmentation significative des performances de certaines fonctions.

# Annexe

![test de performance scopy ](./images/scopy.png)

![test de performance axpy ](./images/saxpy.png)

![test de performance dot ](./images/sdot.png)