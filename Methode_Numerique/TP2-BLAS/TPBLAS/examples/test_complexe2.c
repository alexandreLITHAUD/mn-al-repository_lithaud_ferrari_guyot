#include <stdio.h>
#include <stdlib.h>

#include "mnblas.h"
#include "complexe2.h"

#include "flop.h"


#define    NB_FOIS        512

int main (int argc, char **argv)
{
 complexe_float_t c1= {1.0, 2.0} ;
 complexe_float_t c2= {3.0, 6.0} ;
 complexe_float_t c3= {1.0, 4.0};
 complexe_float_t c4= {5.0, 1.0};
 complexe_float_t c5= {20.0, -4.0};
 complexe_float_t c6= {3.0, 2.0};

 complexe_double_t cd1 ;
 complexe_double_t cd2 ;
 complexe_double_t cd3 ;
 complexe_double_t cd4 ;
 complexe_double_t cd5 ;
 complexe_double_t cd6 ;

 cd1 = (complexe_double_t) {1.0, 2.0} ;
 cd2 = (complexe_double_t) {3.0, 6.0} ;
 cd3 = (complexe_double_t) {1.0, 4.0} ;
 cd4 = (complexe_double_t) {5.0, 1.0} ;
 cd5 = (complexe_double_t) {20.0, -4.0} ;
 cd6 = (complexe_double_t) {3.0, 2.0} ;

 printf ("c1.r %f c1.i %f\n", c1.real, c1.imaginary) ;
 printf ("c2.r %f c2.i %f\n", c2.real, c2.imaginary) ;
 printf ("c3.r %f c3.i %f\n", c3.real, c3.imaginary) ;
 printf ("c4.r %f c4.i %f\n", c4.real, c4.imaginary) ;
 printf ("c5.r %f c5.i %f\n", c5.real, c5.imaginary) ;
 printf ("c6.r %f c6.i %f\n", c6.real, c6.imaginary) ;

 printf ("cd1.r %f cd1.i %f\n", cd1.real, cd1.imaginary) ;
 printf ("cd2.r %f cd2.i %f\n", cd2.real, cd2.imaginary) ;
 printf ("cd3.r %f cd3.i %f\n", cd3.real, cd3.imaginary) ;
 printf ("cd4.r %f cd4.i %f\n", cd4.real, cd4.imaginary) ;
 printf ("cd5.r %f cd5.i %f\n", cd5.real, cd5.imaginary) ;
 printf ("cd6.r %f cd6.i %f\n", cd6.real, cd6.imaginary) ;

/*-------Tests de la justesse des résultats-------*/

 c1 = add_complexe_float (c1, c2) ;
 c3 = mult_complexe_float (c3, c4) ;
 c5 = div_complexe_float (c5, c6) ;

 cd1 = add_complexe_double (cd1, cd2) ;
 cd3 = mult_complexe_double (cd3, cd4) ;
 cd5 = div_complexe_double (cd5, cd6);
 
 printf ("Addition float : c1 + c2 = %f %f\n", c1.real, c1.imaginary) ;
 printf ("Multiplication float : c3 * c4 = %f %f\n", c3.real, c3.imaginary) ;
 printf ("Division float : c5 / c6 = %f %f\n", c5.real, c5.imaginary) ;
 
 printf ("Addition double : cd1 + cd2 = %f %f\n", cd1.real, cd1.imaginary) ;
 printf ("Multiplication double : cd3 * cd4 = %f %f\n", cd3.real, cd3.imaginary) ;
 printf ("Division double : cd5 / cd6 = %f %f\n", cd5.real, cd5.imaginary) ;


/*--------Tests des performances---------------*/

 struct timeval start1, end1, start2, end2, start3, end3 ;
 int i ;

 init_flop_micro () ;
 
 c1 = add_complexe_float (c1, c2) ;

 printf ("c1.r %f c&.i %f\n", c1.real, c1.imaginary) ;

 cd1 = (complexe_double_t) {10.0, 7.0} ;
 cd2 = (complexe_double_t) {25.0, 32.0} ;

//Addition
 TOP_MICRO(start1) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd1 = add_complexe_double (cd1, cd2) ;
   }

 TOP_MICRO(end1) ;

 printf ("Apres boucle cd1.real %f cd1.imaginary %f duree %f \n", cd1.real, cd1.imaginary, tdiff_micro (&start1, &end1)) ;

 calcul_flop_micro ("Addition complexe ", NB_FOIS*2, tdiff_micro(&start1, &end1)) ;

//Multiplication
 TOP_MICRO(start2) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd3 = mult_complexe_double (cd3, cd4) ;
   }

 TOP_MICRO(end2) ;

 printf ("Apres boucle cd3.real %f cd3.imaginary %f duree %f \n", cd1.real, cd1.imaginary, tdiff_micro (&start2, &end2)) ;

 calcul_flop_micro ("Multiplication complexe ", NB_FOIS*2, tdiff_micro(&start2, &end2)) ;

//Division 
 TOP_MICRO(start3) ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     cd5 = div_complexe_double (cd5, cd6) ;
   }

 TOP_MICRO(end3) ;

 printf ("Apres boucle cd5.real %f cd5.imaginary %f duree %f \n", cd1.real, cd1.imaginary, tdiff_micro (&start3, &end3)) ;

 calcul_flop_micro ("Division complexe ", NB_FOIS*2, tdiff_micro(&start3, &end3)) ;

 exit (0) ;
}


