#include <stdio.h>
#include <stdlib.h>
#include "../src/gemv2.c"

#include "flop.h"

void testFloat(int alpha, int beta){
    
    struct timeval start, end;
    
    init_flop_micro();

    float X[2] = {0,1};
    float Y[2] = {1,0};

    int M = 2;
    int N = 2;

    float A[4] = {0,1,2,3};
    
    TOP_MICRO(start);
    mncblas_sgemv(MNCblasRowMajor, MNCblasNoTrans, M, N, alpha, A, 1, X, 1, beta, Y, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->   N*M + 5*N   FLOP
    calcul_flop_micro("blas2 mncblas_sgemv micro", N*M + 5*N, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 2; t++){
        printf("%f \t", Y[t]);
    }
    printf("\n");
}

void testDouble(int alpha, int beta){
        
    struct timeval start, end;
    
    init_flop_micro();

    double X[2] = {0,1};
    double Y[2] = {1,0};

    int M = 2;
    int N = 2;

    double A[4] = {0,1,2,3};
    
    TOP_MICRO(start);
    mncblas_dgemv(MNCblasRowMajor, MNCblasNoTrans, M, N, alpha, A, 1, X, 1, beta, Y, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->  N*M + 5*N  FLOP
    calcul_flop_micro("blas2 mncblas_dgemv micro", N*M + 5*N, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 2; t++){
        printf("%f \t", Y[t]);
    }
    printf("\n");
}

void testComplexFloat(){

   struct timeval start, end;
    
    init_flop_micro();

    complexe_float_t X[2] = {{1,0},{0,1}};
    complexe_float_t Y[2] = {{2,0},{0,2}};

    int M = 2;
    int N = 2;

    complexe_float_t alpha = {(float)1,(float)1};
    complexe_float_t beta = {(float)2,(float)2};

    complexe_float_t A[4] = {{0,0},{1,1},{2,2},{3,3}};
    
    TOP_MICRO(start);
    mncblas_cgemv(MNCblasRowMajor, MNCblasNoTrans, M, N, &alpha, A, 1, X, 1, &beta, Y, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->  2*N*M + 16*N  FLOP
    calcul_flop_micro("blas2 mncblas_cgemv micro", 2*N*M + 16*N, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 2; t++){
       printf("%f + %fi \t", Y[t].real, Y[t].imaginary);
    }
    printf("\n");
}

void testComplexDouble(){

    struct timeval start, end;
    
    init_flop_micro();

    complexe_double_t X[2] = {{1,0},{0,1}};
    complexe_double_t Y[2] = {{2,0},{0,2}};

    int M = 2;
    int N = 2;

    complexe_double_t alpha = {(double)1,(double)1};
    complexe_double_t beta = {(double)2,(double)2};

    complexe_double_t A[4] = {{0,0},{1,1},{2,2},{3,3}};
    
    TOP_MICRO(start);
    mncblas_zgemv(MNCblasRowMajor, MNCblasNoTrans, M, N, &alpha, A, 1, X, 1, &beta, Y, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->  2*N*M + 16*N  FLOP
    calcul_flop_micro("blas2 mncblas_zgemv micro", 2*N*M + 16*N, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 2; t++){
       printf("%f + %fi \t", Y[t].real, Y[t].imaginary);
    }
    printf("\n");
}


int main (int argc, char **argv)
{
    int alpha = 1;
    int beta = 2;
    printf("==========================================================\n");
    printf("===================== BLAS2 - Float ======================\n");
    printf("Résultat attendu : 2  1\n");
    printf("--------------------------\n");

    testFloat(alpha,beta);

    printf("==========================================================\n");
    printf("===================== BLAS2 - Double =====================\n");
    printf("Résultat attendu : 2  1\n");
    printf("--------------------------\n");

    testDouble(alpha,beta);

    printf("==========================================================\n");
    printf("================= BLAS2 - Complex FLoat ==================\n");
    printf("Résultat attendu : 4 + 6i   -6 + 4i\n");
    printf("------------------------------------\n");

    testComplexFloat();

    printf("==========================================================\n");
    printf("================ BLAS2 - Complex Double ==================\n");
    printf("Résultat attendu : 4 + 6i   -6 + 4i\n");
    printf("------------------------------------\n");

    testComplexDouble();

    exit(0);
}