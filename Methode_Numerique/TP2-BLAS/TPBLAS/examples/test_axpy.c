#include <stdio.h>

#include "mnblas.h"
#include "complexe2.h"

#include "flop.h"

#define VECSIZE 65536

#define VECSIZE_PETIT 5

#define NB_FOIS 10

typedef float vfloat [VECSIZE] ;
typedef float vfloat_petit [VECSIZE_PETIT] ;

typedef double vdouble [VECSIZE] ;
typedef double vdouble_petit [VECSIZE_PETIT] ;


vfloat vec1, vec2 ;
vfloat_petit vec3, vec4 ;

vdouble vecd1, vecd2 ;
vdouble_petit vecd3, vecd4 ;

complexe_double_t comp_d1 [VECSIZE];
complexe_double_t comp_d2 [VECSIZE];
complexe_double_t comp_d3 [VECSIZE];
complexe_double_t comp_d4 [VECSIZE_PETIT];
complexe_double_t comp_d5 [VECSIZE_PETIT];
complexe_double_t comp_d6 [VECSIZE_PETIT];

complexe_float_t comp_f1 [VECSIZE];
complexe_float_t comp_f2 [VECSIZE];
complexe_float_t comp_f3 [VECSIZE];
complexe_float_t comp_f4 [VECSIZE_PETIT];
complexe_float_t comp_f5 [VECSIZE_PETIT];
complexe_float_t comp_f6 [VECSIZE_PETIT];

void complexe_init (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexe_init_petit (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init_petit (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_petit (vfloat_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vectord_init (vdouble V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}
void vectord_init_petit (vdouble_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_print_petit (vfloat_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print (vdouble V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print_petit (vdouble_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void complexe_print_petit (complexe_float_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%f ", cf[i].real) ;
    printf ("%fi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

void complexed_print_petit (complexe_double_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%lf ", cf[i].real) ;
    printf ("%lfi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //struct timespec startnano, endnano;
 //unsigned long long int start_tsc, end_tsc ;
 
 float res ;
 int i ;

 printf ("====================TEST MATHS - mncblas_saxpy==========================\n") ;

 vector_init_petit (vec3, 1.0) ;
 vector_init_petit (vec4, 2.0) ;

 mnblas_saxpy (VECSIZE_PETIT, 2.0 , vec3, 1, vec4, 1) ;

 vector_print_petit (vec3);
 vector_print_petit (vec4);

 printf ("========================================================================\n") ;

 printf ("=====================TEST GFLOP - mnblas_saxpy==========================\n") ;
 
 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 2.0) ;
     res = 0.0 ;
     
     TOP_MICRO(start) ;
        mnblas_saxpy (VECSIZE, 2.0 , vec1, 1, vec2, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("axpy mnblas_saxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
 printf ("========================================================================\n") ;

 printf ("====================TEST MATHS - mncblas_daxpy==========================\n") ;

 vectord_init_petit (vecd3, 1.0) ;
 vectord_init_petit (vecd4, 2.0) ;

 mnblas_daxpy (VECSIZE_PETIT, 2.0 , vecd3, 1, vecd4, 1) ;

 vectord_print_petit (vecd3);
 vectord_print_petit (vecd4);

 printf ("========================================================================\n") ;

 printf ("=======================TEST GFLOP - mnblas_daxpy========================\n") ;
 
 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vectord_init (vecd1, 1.0) ;
     vectord_init (vecd2, 2.0) ;

     TOP_MICRO(start) ;
        mnblas_daxpy (VECSIZE, 2.0, vecd1, 1, vecd2, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("axpy mnblas_daxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
 printf ("========================================================================\n") ;

 printf ("====================TEST MATHS - mncblas_caxpy==========================\n") ;

 complexe_init_petit (comp_f4, 1.0,1.0) ;
 complexe_init_petit (comp_f5, 2.0,2.0) ;
 complexe_init_petit (comp_f6, 3.0,3.0) ;

 mnblas_caxpy (VECSIZE_PETIT, comp_f6 ,comp_f4, 1, comp_f5, 1) ;

 complexe_print_petit (comp_f4);
 complexe_print_petit (comp_f5);
 complexe_print_petit (comp_f6);

 printf ("========================================================================\n") ;

 printf ("======================TEST GFLOP - mnblas_caxpy=========================\n") ;
 
 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     complexe_init(comp_f1,1.0,1.0);
     complexe_init(comp_f2,2.0,2.0);
     complexe_init(comp_f3,3.0,3.0);
     
     TOP_MICRO(start) ;
        mnblas_caxpy (VECSIZE, comp_f3 ,comp_f1, 1, comp_f2, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("axpy mnblas_caxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
 printf ("========================================================================\n") ;

 printf ("====================TEST MATHS - mncblas_zaxpy==========================\n") ;

 complexed_init_petit (comp_d4, 1.0,1.0) ;
 complexed_init_petit (comp_d5, 2.0,2.0) ;
 complexed_init_petit (comp_d6, 3.0,3.0) ;

 mnblas_zaxpy (VECSIZE_PETIT, comp_d6 ,comp_d4, 1, comp_d5, 1) ;

 complexed_print_petit (comp_d4);
 complexed_print_petit (comp_d5);
 complexed_print_petit (comp_d6);

 printf ("========================================================================\n") ;

 printf ("=======================TEST GFLOP - mnblas_zaxpy========================\n") ;
 
 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     complexed_init(comp_d1,1.0,1.0);
     complexed_init(comp_d2,2.0,2.0);
     complexed_init(comp_d3,3.0,3.0);
     
     TOP_MICRO(start) ;
        mnblas_zaxpy (VECSIZE, comp_d3, comp_d1, 1, comp_d2, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("axpy mnblas_zaxpy micro", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
 printf ("========================================================================\n") ;
}