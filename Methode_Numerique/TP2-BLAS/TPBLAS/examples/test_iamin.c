#include <stdio.h>

#include "mnblas.h"
#include "complexe2.h"

#include "flop.h"

#define VECSIZE 65536

#define VECSIZE_PETIT 5

#define NB_FOIS 10


typedef float vfloat[VECSIZE];
typedef float vfloat_petit [VECSIZE_PETIT] ;

typedef double vdouble[VECSIZE];
typedef double vdouble_petit [VECSIZE_PETIT] ;

vfloat vec1;
vfloat_petit vec2;

vdouble vecd1;
vdouble_petit vecd2;

complexe_double_t comp_d1[VECSIZE];
complexe_double_t comp_d2 [VECSIZE_PETIT];

complexe_float_t comp_f1[VECSIZE];
complexe_float_t comp_f2 [VECSIZE_PETIT];

void complexe_init (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexe_init_petit (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init_petit (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_petit (vfloat_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vectord_init (vdouble V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}
void vectord_init_petit (vdouble_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_print_petit (vfloat_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print (vdouble V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print_petit (vdouble_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void complexe_print_petit (complexe_float_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%f ", cf[i].real) ;
    printf ("%fi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

void complexed_print_petit (complexe_double_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%lf ", cf[i].real) ;
    printf ("%lfi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}
int main(int argc, char **argv)
{
  struct timeval start, end;
  //struct timespec startnano, endnano;
  //unsigned long long int start_tsc, end_tsc;

  float res;
  //double resd;
  int i;
  
  printf ("====================TEST MATHS - mncblas_isamin==========================\n") ;

  vector_init_petit (vec2, 1.0) ;

  int min_float = mnblas_isamin (VECSIZE_PETIT, vec2, 1) ;

  vector_print_petit (vec2);
  printf("Index du max = %i\n", min_float);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_isamin==========================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    vector_init(vec1, 1.0);
    res = 0.0;

    TOP_MICRO(start);
    mnblas_isamin(VECSIZE, vec1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("iamin mnblas_isamin micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mncblas_idamin==========================\n") ;

  vectord_init_petit (vecd2, 1.0) ;

  int min_double = mnblas_idamin (VECSIZE_PETIT, vecd2, 1);

  vectord_print_petit (vecd2);
  printf("Index du min = %i\n", min_double);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_idamin=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    vectord_init(vecd1, 1.0);

    TOP_MICRO(start);
    mnblas_idamin(VECSIZE, vecd1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("iamin mnblas_idamin micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mncblas_icamin==========================\n") ;

  complexe_init_petit (comp_f2, 1.0,1.0) ;

  int min_comp_float = mnblas_icamin (VECSIZE_PETIT, comp_f2 ,1) ;

  complexe_print_petit (comp_f2);
  printf("Index du min = %i\n", min_comp_float);
  
  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_icamin=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    complexe_init(comp_f1, 1.0, 1.0);

    TOP_MICRO(start);
    mnblas_icamin(VECSIZE, comp_f1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("iamin mnblas_icamin micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mncblas_izamin==========================\n") ;

  complexed_init_petit (comp_d2, 1.0,1.0) ;

  int min_comp_double = mnblas_izamin (VECSIZE_PETIT, comp_d2, 1) ;

  complexed_print_petit (comp_d2);
  printf("Index du min = %i\n", min_comp_double);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_izamin=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    complexed_init(comp_d1, 1.0, 1.0);

    TOP_MICRO(start);
    mnblas_izamin(VECSIZE, comp_d1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("iamin mnblas_izamin micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");
}