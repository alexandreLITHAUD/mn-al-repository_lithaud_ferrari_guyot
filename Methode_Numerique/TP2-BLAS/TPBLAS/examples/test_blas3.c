#include <stdio.h>
#include <stdlib.h>
#include "../src/blas3.c"

#include "flop.h"

void testFloat(int alpha, int beta){
    
    struct timeval start, end;
    
    init_flop_micro();

    float A[4] = {1,2,3,4};
    float B[4] = {1,2,0,3};
    float C[4] = {2,0,1,0};

    int M = 2;
    int N = 2;
    int K = 2;
    
    TOP_MICRO(start);
    mncblas_sgemm(MNCblasRowMajor, MNCblasNoTrans, MNCblasNoTrans, M, N, K,alpha, A, 1, B, 1, beta, C, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->   M*K*K?*2 + K*M + N*M + N*K    FLOP
    calcul_flop_micro("blas3 mncblas_sgemm micro", M*K*K*2 + K*M + N*M + N*K, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 4; t+=2){
        printf("%f \t", C[t]);
        printf("%f \n", C[t+1]);

    }

    printf("\n");
}

void testDouble(int alpha, int beta){
        
    struct timeval start, end;
    
    init_flop_micro();

    double E[4] = {1,2,3,4};
    double F[4] = {1,2,0,3};
    double G[4] = {2,0,1,0};

    int M = 2;
    int N = 2;
    int K = 2;
    
    TOP_MICRO(start);
    mncblas_dgemm(MNCblasRowMajor, MNCblasNoTrans, MNCblasNoTrans, M, N, K, alpha, E, 1, F, 1, beta, G, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->   M*K*K?*2 + K*M + N*M + N*K    FLOP
    calcul_flop_micro("blas3 mncblas_sgemm micro", M*K*K*2 + K*M + N*M + N*K, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 4; t+=2){
        printf("%f \t", G[t]);
        printf("%f \n", G[t+1]);

    }
    printf("\n");
}

void testComplexFloat(){

    struct timeval start, end;
    
    init_flop_micro();

    complexe_float_t E[4] = {{1,4},{2,3},{3,2},{4,1}};
    complexe_float_t F[4] = {{1,3},{2,0},{0,2},{3,1}};
    complexe_float_t G[4] = {{2,0},{0,1},{1,0},{0,2}};

    complexe_float_t alphac = {(float)3, (float)1};
    complexe_float_t betac = {(float)0, (float)2};

    int M = 2;
    int N = 2;
    int K = 2;
    
    TOP_MICRO(start);
    mncblas_cgemm(MNCblasRowMajor, MNCblasNoTrans, MNCblasNoTrans, M, N, K,&alphac, E, 1, F, 1, &betac, G, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->   M*K*K*9 + K*M*7 + N*M*7 + N*K*2    FLOP
    calcul_flop_micro("blas3 mncblas_sgemm micro", M*K*K*9 + K*M*7 + N*M*7 + N*K*2, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 4; t+=2){
        printf("%f + %fi \t", G[t].real, G[t].imaginary);
        printf("%f + %fi \n", G[t+1].real, G[t+1].imaginary);

    }
    printf("\n");
}

void testComplexDouble(){

    struct timeval start, end;
    
    init_flop_micro();

    complexe_double_t E[4] = {{1,4},{2,3},{3,2},{4,1}};
    complexe_double_t F[4] = {{1,3},{2,0},{0,2},{3,1}};
    complexe_double_t G[4] = {{2,0},{0,1},{1,0},{0,2}};

    complexe_double_t alphac = {(double)3, (double)1};
    complexe_double_t betac = {(double)0, (double)2};

    int M = 2;
    int N = 2;
    int K = 2;
    
    TOP_MICRO(start);
    mncblas_zgemm(MNCblasRowMajor, MNCblasNoTrans, MNCblasNoTrans, M, N, K,&alphac, E, 1, F, 1, &betac, G, 1);
    TOP_MICRO(end);

    // nb_operations_flottantes |->   M*K*K*9 + K*M*7 + N*M*7 + N*K*2    FLOP
    calcul_flop_micro("blas3 mncblas_sgemm micro", M*K*K*9 + K*M*7 + N*M*7 + N*K*2, tdiff_micro(&start, &end));
    printf("--------------------------\n");

    printf("Resultat observé :\n");
    //========================= PRINT
    for (int t = 0; t < 4; t+=2){
        printf("%f + %fi \t", G[t].real, G[t].imaginary);
        printf("%f + %fi \n", G[t+1].real, G[t+1].imaginary);

    }
    printf("\n");
}


int main (int argc, char **argv)
{
    int alpha = 3;
    int beta = 2;


    printf("==========================================================\n");
    printf("===================== BLAS3 - Float ======================\n");
    printf("Résultat attendu : 7    24\n");
    printf("                   11   54\n");
    printf("--------------------------\n");

    testFloat(alpha, beta);

    printf("==========================================================\n");
    printf("===================== BLAS3 - Double =====================\n");
    printf("Résultat attendu : 7    24\n");
    printf("                   11   54\n");
    printf("--------------------------\n");

    testDouble(alpha, beta);

    printf("==========================================================\n");
    printf("================= BLAS3 - Complex FLoat ==================\n");
    printf("Résultat attendu : -62+20i    -6+62i\n");
    printf("                   -34+54i    36+50i\n");
    printf("------------------------------------\n");

    testComplexFloat();

    printf("==========================================================\n");
    printf("================ BLAS3 - Complex Double ==================\n");
    printf("Résultat attendu : -62+20i    -6+62i\n");
    printf("                   -34+54i    36+50i\n");
    printf("------------------------------------\n");

    testComplexDouble();

    exit(0);
}