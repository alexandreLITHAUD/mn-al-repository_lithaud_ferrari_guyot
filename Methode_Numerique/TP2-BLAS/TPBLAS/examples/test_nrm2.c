#include <stdio.h>

#include "mnblas.h"
#include "complexe2.h"

#include "flop.h"

#define VECSIZE 65536

#define VECSIZE_PETIT 5

#define NB_FOIS 10

typedef float vfloat[VECSIZE];
typedef float vfloat_petit [VECSIZE_PETIT] ;

typedef double vdouble[VECSIZE];
typedef double vdouble_petit [VECSIZE_PETIT] ;

vfloat vec1;
vfloat_petit vec2;

vdouble vecd1;
vdouble_petit vecd2;

complexe_double_t comp_d1[VECSIZE];
complexe_double_t comp_d2 [VECSIZE_PETIT];

complexe_float_t comp_f1[VECSIZE];
complexe_float_t comp_f2 [VECSIZE_PETIT];

void complexe_init (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexe_init_petit (complexe_float_t *cf, float v1, float v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void complexed_init_petit (complexe_double_t *cf, double v1, double v2){
  register unsigned int i;

  for(i = 0; i<VECSIZE_PETIT;i++){
    cf[i].real = v1;
    cf[i].imaginary = v2;
  }
  return;
}

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_petit (vfloat_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vectord_init (vdouble V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}
void vectord_init_petit (vdouble_petit V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    V [i] = x ;

  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_print_petit (vfloat_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print (vdouble V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vectord_print_petit (vdouble_petit V)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE_PETIT; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void complexe_print_petit (complexe_float_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%f ", cf[i].real) ;
    printf ("%fi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

void complexed_print_petit (complexe_double_t *cf){
  register unsigned int i ;
  
  for (i = 0; i < VECSIZE_PETIT; i++){
    printf ("%lf ", cf[i].real) ;
    printf ("%lfi ", cf[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

int main(int argc, char **argv)
{
  struct timeval start, end;
  //struct timespec startnano, endnano;
  //unsigned long long int start_tsc, end_tsc;

  float res;
  //double resd;
  int i;

  printf ("====================TEST MATHS - mncblas_snrm2==========================\n") ;

  vector_init_petit (vec2, 1.0) ;

  float result_float = mnblas_snrm2 (VECSIZE_PETIT, vec2, 1) ;

  vector_print_petit (vec2);
  printf("Result = %f\n", result_float);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_snrm2==========================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    vector_init(vec1, 1.0);
    res = 0.0;

    TOP_MICRO(start);
    mnblas_snrm2(VECSIZE, vec1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("nrm2 mnblas_snrm2 micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mnblas_dnrm2==========================\n") ;

  vectord_init_petit (vecd2, 1.0) ;

  double result_double = mnblas_dnrm2 (VECSIZE_PETIT, vecd2, 1);

  vectord_print_petit (vecd2);
  printf("Result = %lf\n", result_double);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_dnrm2=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    vectord_init(vecd1, 1.0);

    TOP_MICRO(start);
    mnblas_dnrm2(VECSIZE, vecd1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("nrm2 mnblas_dnrm2 micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mnblas_scnrm2==========================\n") ;

  complexe_init_petit (comp_f2, 1.0,1.0) ;

  float result_comp_float = mnblas_scnrm2 (VECSIZE_PETIT, comp_f2 ,1) ;

  complexe_print_petit (comp_f2);
  printf("Result = %f\n", result_comp_float);
  

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_scnrm2=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    complexe_init(comp_f1, 1.0, 1.0);

    TOP_MICRO(start);
    mnblas_scnrm2(VECSIZE, comp_f1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("nrm2 mnblas_scnrm2 micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");

  printf ("====================TEST MATHS - mnblas_dznrm2==========================\n") ;

  complexed_init_petit (comp_d2, 1.0,1.0) ;

  double result_comp_double = mnblas_dznrm2 (VECSIZE_PETIT, comp_d2, 1) ;

  complexed_print_petit (comp_d2);
  printf("Result = %lf\n", result_comp_double);

  printf ("========================================================================\n") ;

  printf("====================TEST GFLOP - mnblas_dznrm2=====================\n");

  init_flop_micro();

  for (i = 0; i < NB_FOIS; i++)
  {
    complexed_init(comp_d1, 1.0, 1.0);

    TOP_MICRO(start);
    mnblas_dznrm2(VECSIZE, comp_d1, 1);
    TOP_MICRO(end);

    calcul_flop_micro("nrm2 mnblas_dznrm2 micro", 2 * VECSIZE, tdiff_micro(&start, &end));
  }

  printf("res = %f\n", res);
  printf("==========================================================\n");
}